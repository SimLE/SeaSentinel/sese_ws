### Directory setup
```bash
mkdir Sese
cd Sese
git clone https://gitlab.com/SimLE/SeaSentinel/sese_ws.git --recursive 
git clone https://github.com/PyroX2/PX4-Autopilot.git --recursive
cd sese_ws
git clone https://github.com/PX4/px4_msgs.git ./src/px4_msgs
```

### Check Session Type
```bash
echo $XDG_SESSION_TYPE
```
Make sure this returns `x11`

### Running docker
```bash
make build # to build an image
make shell-sitl # to run a container
```

### Inside docker container
```bash
cd /home/user/PX4-Autopilot
make px4_sitl gz_x500 # Replace with "gz_sese_omni_boat" for sese boat
```

### Simulation problems
Right now when starting simulation in docker I get the following error: `ERROR [gz_bridge] Service call timed out. Check GZ_SIM_RESOURCE_PATH is set correctly`. Not sure why, however simple Ctrl+C and starting simulation again helps. 

### Connect PX4 with ROS2
To connect PX4 with ROS2 Micro-XRCE-DDS is used. To run it, after `make shell-sitl` open a second terminal and run `./open_second_terminal.sh`. If the file is not executable change it with `chmod +x open_second_terminal.sh`. You should now be able to control the same container from second terminal

Run `MicroXRCEAgent udp4 -p 8888`. If you now open docker container in third terminal and run `ros2 topic list` you should see topics starting with /fmu/... 

Topics starting with /fmu/in are topics that PX4 is subscribed to and /fmu/out are topics published by PX4