import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose

class MockCameraNode(Node):
    def __init__(self):
        super().__init__('mock_camera_node')
        self.publisher = self.create_publisher(ObjectHypothesisWithPose, 'camera/detections', 10)
        self.timer = self.create_timer(0.3, self.publish_detections)
        

    def publish_detections(self):
        msg = ObjectHypothesisWithPose()
        # print(msg)

        msg.hypothesis.class_id = '0'
        msg.pose.pose.position.x = 111.0
        msg.pose.pose.position.z = 23.0
        msg.pose.pose.position.y = 765.4
        msg.hypothesis.score = 0.5

        self.publisher.publish(msg)

def main(args=None):
    rclpy.init(args=args)

    node = MockCameraNode()
    rclpy.spin(node)

    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()