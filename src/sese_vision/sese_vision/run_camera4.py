#!/usr/bin/env python
import cv2
import depthai as dai
import numpy as np
import time
from pathlib import Path
import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose
from vision_msgs.msg import BoundingBox2D
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import os

from ament_index_python.packages import get_package_share_directory

class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        self.detections_publisher = self.create_publisher(ObjectHypothesisWithPose, '/Detections', 10)
        self.detections_publisher_T5 = self.create_publisher(ObjectHypothesisWithPose, '/Detections_T5', 10)
        self.bbox_publisher = self.create_publisher(BoundingBox2D, '/bboxes', 10)
        self.image_publisher = self.create_publisher(Image, '/raw_image', 10)


def create_pipeline(blob_name, syncNN=True):

    pipeline = dai.Pipeline()

    # chunk size for splitting device-sent XLink packets, in bytes
    pipeline.setXLinkChunkSize(64 * 1024)

    # pipeline.setCameraTuningBlobPath('/home/jakub/Downloads/tuning_color_ov9782_wide_fov.bin')
    # Define sources and outputs
    camRgb = pipeline.create(dai.node.ColorCamera)
    camRgb.initialControl.setManualExposure(700, 100) #(800,100)
    spatialDetectionNetwork = pipeline.create(dai.node.YoloSpatialDetectionNetwork)
    monoLeft = pipeline.create(dai.node.MonoCamera)
    monoRight = pipeline.create(dai.node.MonoCamera)
    stereo = pipeline.create(dai.node.StereoDepth)
    nnNetworkOut = pipeline.create(dai.node.XLinkOut)

    xoutRgb = pipeline.create(dai.node.XLinkOut)
    xoutNN = pipeline.create(dai.node.XLinkOut)
    xoutDepth = pipeline.create(dai.node.XLinkOut)

    xoutRgb.setStreamName("rgb")
    xoutNN.setStreamName("detections")
    xoutDepth.setStreamName("depth")
    nnNetworkOut.setStreamName("nnNetwork")

    # Properties
    camRgb.setPreviewSize(
        640,640
    )  # Size of images (640, 640) is required by DepthAI model conversion pipeline
    camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    camRgb.setInterleaved(False)
    camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P) #THE_720_P
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P) #THE_400_P
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

    # setting node configs
    stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
    # Align depth map to the perspective of RGB camera, on which inference is done
    stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
    stereo.setOutputSize(monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight())

    spatialDetectionNetwork.setBlobPath(blob_name)
    spatialDetectionNetwork.setConfidenceThreshold(0.6)
    spatialDetectionNetwork.input.setBlocking(False)
    spatialDetectionNetwork.setBoundingBoxScaleFactor(0.5)
    spatialDetectionNetwork.setDepthLowerThreshold(100)
    spatialDetectionNetwork.setDepthUpperThreshold(200_000)

    # Yolo specific parameters
    spatialDetectionNetwork.setNumClasses(80) #before change was 5
    spatialDetectionNetwork.setCoordinateSize(4)
    spatialDetectionNetwork.setAnchors([10, 14, 23, 27, 37, 58, 81, 82, 135, 169, 344, 319])
    spatialDetectionNetwork.setAnchorMasks(
        {"side80": [1, 2, 3], "side40": [3, 4, 5], "side20": [3, 4, 5]}
    )
    spatialDetectionNetwork.setIouThreshold(0.5)

    # Linking
    monoLeft.out.link(stereo.left)
    monoRight.out.link(stereo.right)

    camRgb.preview.link(spatialDetectionNetwork.input)
    if syncNN:
        spatialDetectionNetwork.passthrough.link(xoutRgb.input)
    else:
        camRgb.preview.link(xoutRgb.input)

    spatialDetectionNetwork.out.link(xoutNN.input)

    stereo.depth.link(spatialDetectionNetwork.inputDepth)
    spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)
    spatialDetectionNetwork.outNetwork.link(nnNetworkOut.input)

    return pipeline


def main():
    rclpy.init()
    minimal_publisher = MinimalPublisher()

    # Create msgs
    msg = ObjectHypothesisWithPose()
    msg2 = ObjectHypothesisWithPose()
    bbox_msg = BoundingBox2D()
    raw_image_msg = Image()
    bridge = CvBridge()


    #nnBlobPath = f'/home/{os.environ["USER"]}/sese_git/src/sese/nn_models/only_boys_6/best_openvino_2022.1_6shave.blob'
    nnBlobPath = os.path.join(get_package_share_directory('sese_vision'),'models','yolo_8n', 'yoloo_openvino_2022.1_6shave.blob')
    
    if not Path(nnBlobPath).exists():
        import sys

        raise FileNotFoundError(
            f'Blob file not found"'
        )

    print(nnBlobPath)

    for device in dai.Device.getAllAvailableDevices():
        print(f"{device.getMxId()} {device.state}")
        camera_id = device.getMxId()

    # Create pipeline
    pipeline = create_pipeline(nnBlobPath, syncNN=True)

    with dai.Device(pipeline) as device:

        
        detectionNNQueue = device.getOutputQueue(
            name="detections", maxSize=4, blocking=False
        )
       
        networkQueue = device.getOutputQueue(name="nnNetwork", maxSize=4, blocking=False)

        # It uses USB3
        # print(f'USB speed = {device.getUsbSpeed()}')
        startTime = time.monotonic()
        counter = 0
        fps = 0
        color = (255, 255, 255)
        color_text=(0, 255, 255)
        printOutputLayersOnce = True
        abc = 0
        while True:
            abc += 1
           
            inDet = detectionNNQueue.get()
            
            inNN = networkQueue.get()

            
            detections = inDet.detections
            print(f"{abc}:{detections=}")
           

          
            for detection in detections:
                print("detekcja :)")
                
                msg.id = str(detection.label)
                msg.pose.pose.position.x = detection.spatialCoordinates.x
                msg.pose.pose.position.z = z_calculate_by_size
                msg.pose.pose.position.y = detection.spatialCoordinates.y
                msg.score = detection.confidence

                # msg.id = detection.label
                minimal_publisher.detections_publisher.publish(msg)
               
        rclpy.spin(minimal_publisher)

          
        minimal_publisher.destroy_node()
        rclpy.shutdown()


        # rospy.spin()

if __name__ == '__main__':
    main()
