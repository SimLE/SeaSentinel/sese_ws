import cv2
import depthai as dai
import numpy as np
import time
from pathlib import Path
import os
import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from sese_vision.utilities.utils import create_pipeline, create_camera_pipeline_long_range, annotateFrame
from sese_vision.utilities.config import YOLOV8N_MODEL

class CameraPublisher(Node):
    def __init__(self):
        super().__init__('minimal_publisher')
        self.detections_publisher = self.create_publisher(ObjectHypothesisWithPose, 'Camera/detections', 10)
        self.image_publisher = self.create_publisher(Image, 'Camera/raw_image', 10)

    def publish_detections(self, detections):
        for det in detections:
            detection_msg= self.create_detection_msg(det)
    
            self.detections_publisher.publish(detection_msg)

    def create_detection_msg(self, det):
        detection_msg = ObjectHypothesisWithPose()
                            
        # ID obiektu (np. klasa rozpoznanego obiektu)
        detection_msg.hypothesis.class_id = str(det.label)
        detection_msg.hypothesis.score = det.confidence  # Pewność detekcji
        
        # Pozycja obiektu w przestrzeni 
        pose = Pose()
        pose.position.x = det.spatialCoordinates.x
        pose.position.y = det.spatialCoordinates.y
        pose.position.z = det.spatialCoordinates.z
        
        # Ustawienie orientacji
        pose.orientation.x = 0.0
        pose.orientation.y = 0.0
        pose.orientation.z = 0.0
        pose.orientation.w = 1.0
        
        detection_msg.pose.pose = pose
        return detection_msg


def main():
    try:
        rclpy.init()
        cameraPublisher = CameraPublisher()

        devices = dai.Device.getAllAvailableDevices()
        if not devices:
            raise RuntimeError("No DepthAI devices found")

        pipeline = create_camera_pipeline_long_range(YOLOV8N_MODEL)

        with dai.Device(pipeline) as device:
            previewQueue = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
            detectionNNQueue = device.getOutputQueue(name="detections", maxSize=4, blocking=False)

            bridge = CvBridge()

            while True:
                inPreview = previewQueue.get()
                inDet = detectionNNQueue.get()
            
                frame = inPreview.getCvFrame()

                detections = inDet.detections
                if detections:
                    cameraPublisher.publish_detections(detections)
                    
                annotated_frame = annotateFrame(frame, detections)

                image_msg = bridge.cv2_to_imgmsg(annotated_frame, encoding="bgr8")
                cameraPublisher.image_publisher.publish(image_msg)

    except KeyboardInterrupt:
        cameraPublisher.destroy_node()
        rclpy.shutdown()

if __name__ == '__main__':
    main()