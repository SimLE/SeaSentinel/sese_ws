import sese_vision.utilities.config as cfg
import json
import numpy as np
import cv2
from pathlib import Path
import depthai as dai

def create_pipeline(model_name):
    print(f'{model_name=}')
    if model_name.lower() == "yolov8n":
        model_path=cfg.YOLOV8N_MODEL     
    else:
        model_path=cfg.YOLOV8N_MODEL
    
    print(f"{model_path=}")
    return create_camera_pipeline(model_path)


def create_camera_pipeline(model_path):
    pipeline = dai.Pipeline()
    
    print("[INFO] configuring source and outputs...")
    camRgb = pipeline.create(dai.node.ColorCamera)
    # create a Yolo detection node
    detectionNetwork = pipeline.create(dai.node.YoloSpatialDetectionNetwork)

    xoutRgb = pipeline.create(dai.node.XLinkOut)
    nnOut = pipeline.create(dai.node.XLinkOut)
    # set stream names used in queue to fetch data when the pipeline is started
    xoutRgb.setStreamName("rgb")
    nnOut.setStreamName("nn_detections")

    print("[INFO] setting camera properties...")
    camRgb.setPreviewSize(cfg.CAMERA_PREV_DIM)
    camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    camRgb.setInterleaved(False)
    camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
    camRgb.setFps(3)

    monoLeft = pipeline.create(dai.node.MonoCamera)
    monoRight = pipeline.create(dai.node.MonoCamera)
    stereo = pipeline.create(dai.node.StereoDepth)
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    # monoLeft.setCamera("left")
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    # monoRight.setCamera("right")

    # setting node configs
    stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
    # Align depth map to the perspective of RGB camera, on which inference is done
    stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
    stereo.setOutputSize(monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight())

    print("[INFO] setting YOLO network properties...")
    detectionNetwork.setConfidenceThreshold(cfg.YoloConfig.confidenceThreshold)
    detectionNetwork.setNumClasses(cfg.YoloConfig.classes)
    detectionNetwork.setCoordinateSize(cfg.YoloConfig.coordinates)
    detectionNetwork.setAnchors(cfg.YoloConfig.anchors)
    detectionNetwork.setAnchorMasks(cfg.YoloConfig.anchorMasks)
    detectionNetwork.setIouThreshold(cfg.YoloConfig.iouThreshold)
    detectionNetwork.setBlobPath(model_path)
    detectionNetwork.setNumInferenceThreads(2)
    detectionNetwork.input.setBlocking(False)
    detectionNetwork.setDepthLowerThreshold(cfg.YoloConfig.depth_lower_treshold)
    detectionNetwork.setDepthUpperThreshold(cfg.YoloConfig.depth_upper_treshold)

    monoLeft.out.link(stereo.left)
    monoRight.out.link(stereo.right)
    stereo.depth.link(detectionNetwork.inputDepth)
    
    camRgb.preview.link(detectionNetwork.input)
    

    detectionNetwork.passthrough.link(xoutRgb.input)
    detectionNetwork.out.link(nnOut.input)
   
    return pipeline
    

def create_camera_pipeline_long_range(model_path):
    syncNN = True

    pipeline = dai.Pipeline()

    detectionNetwork = pipeline.create(dai.node.YoloSpatialDetectionNetwork)
    detectionNetwork.setBlobPath(model_path)
    detectionNetwork.setConfidenceThreshold(cfg.YoloConfig.confidenceThreshold)
    detectionNetwork.setIouThreshold(cfg.YoloConfig.iouThreshold)
    detectionNetwork.setNumClasses(cfg.YoloConfig.classes)
    detectionNetwork.setCoordinateSize(cfg.YoloConfig.coordinates)
    detectionNetwork.setAnchors(cfg.YoloConfig.anchors)
    detectionNetwork.setAnchorMasks(cfg.YoloConfig.anchorMasks)
    detectionNetwork.setNumInferenceThreads(2)
    detectionNetwork.setNumNCEPerInferenceThread(1)
    detectionNetwork.input.setBlocking(False)
    detectionNetwork.setDepthLowerThreshold(cfg.YoloConfig.depth_lower_treshold)
    detectionNetwork.setDepthUpperThreshold(cfg.YoloConfig.depth_upper_treshold)

    camRgb = pipeline.create(dai.node.ColorCamera)
    leftRgb = pipeline.create(dai.node.ColorCamera)
    rightRgb = pipeline.create(dai.node.ColorCamera)
    stereo = pipeline.create(dai.node.StereoDepth)

    nnNetworkOut = pipeline.create(dai.node.XLinkOut)
    xoutRgb = pipeline.create(dai.node.XLinkOut)
    xoutNN = pipeline.create(dai.node.XLinkOut)
    xoutDepth = pipeline.create(dai.node.XLinkOut)

    # set stream names used in queue to fetch data when the pipeline is started
    xoutRgb.setStreamName("rgb")
    xoutNN.setStreamName("detections")
    xoutDepth.setStreamName("depth")
    nnNetworkOut.setStreamName("nnNetwork")

    camRgb.setPreviewSize(cfg.CAMERA_PREV_DIM)
    camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1200_P)
    camRgb.setInterleaved(False)
    camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
    camRgb.setFps(5)

    leftRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1200_P)
    leftRgb.setCamera("left")
    leftRgb.setIspScale(2, 3)

    rightRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1200_P)
    rightRgb.setCamera("right")
    rightRgb.setIspScale(2, 3)

    stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
    stereo.setDepthAlign(dai.CameraBoardSocket.CAM_A)
    stereo.setSubpixel(True)

    # Linking
    leftRgb.isp.link(stereo.left)
    rightRgb.isp.link(stereo.right)

    camRgb.preview.link(detectionNetwork.input)
    if syncNN:
        detectionNetwork.passthrough.link(xoutRgb.input)
    else:
        camRgb.preview.link(xoutRgb.input)
        
    detectionNetwork.out.link(xoutNN.input)

    stereo.depth.link(detectionNetwork.inputDepth)
    detectionNetwork.passthroughDepth.link(xoutDepth.input)
    detectionNetwork.outNetwork.link(nnNetworkOut.input)

    # camRgb.preview.link(detectionNetwork.input)

    return pipeline


def annotateFrame(frame, detections):
    color = (0, 0, 255)
    for detection in detections:
        bbox = frameNorm(frame, (detection.xmin, detection.ymin, detection.xmax, detection.ymax))
        cv2.putText(frame, cfg.LABELS[detection.label], (bbox[0] + 10, bbox[1] + 25), cv2.FONT_HERSHEY_TRIPLEX, 1,
                    color)
        cv2.putText(frame, f"{int(detection.confidence * 100)}%", (bbox[0] + 10, bbox[1] + 60),
                    cv2.FONT_HERSHEY_TRIPLEX, 1, color)
        cv2.putText(frame, 
                    f"x:{int(detection.spatialCoordinates.x)} [mm]", 
                    (bbox[0] + 10, bbox[1] + 50),
                    cv2.FONT_HERSHEY_TRIPLEX, 1, 
                    (255,0,0))
        # cv2.putText(frame, 
        #             f"y:{int(detection.spatialCoordinates.y)} [mm]", 
        #             (bbox[0] + 10, bbox[1] + 65),
        #             cv2.FONT_HERSHEY_TRIPLEX, 1, 
        #             (255,0,0))
        cv2.putText(frame, 
                    f"z:{int(detection.spatialCoordinates.z)} [mm]", 
                    (bbox[0] + 10, bbox[1] + 100),
                    cv2.FONT_HERSHEY_TRIPLEX, 1, 
                    (255,0,0))
        cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[2], bbox[3]), color, 2)
    return frame

def frameNorm(frame, bbox):
    # nn data, being the bounding box locations, are in <0..1> range
    # normalized them with frame width/height
    normVals = np.full(len(bbox), frame.shape[0])
    normVals[::2] = frame.shape[1]
    return (np.clip(np.array(bbox), 0, 1) * normVals).astype(int)

def create_video_writer():
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')

    video_writer = cv2.VideoWriter(
    cfg.OUTPUT_VIDEO,
    fourcc,
    3.0,
    cfg.CAMERA_PREV_DIM
    )
    print("writer_output")
    return video_writer
