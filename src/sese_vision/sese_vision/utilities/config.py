import os
from dataclasses import dataclass
from ament_index_python.packages import get_package_share_directory


#dataset 4k, roboboat 2024
LABELS4k = ['red_gate_buoy',
            'green_gate_buoy',
            'red_buoy',
            'green_buoy',
            'yellow_buoy',
            'black_buoy',
            'blue_circle',
            'green_circle',
            'red_circle',
            'blue_triangle',
            'green_triangle',
            'red_triangle',
            'blue_square',
            'green_square',
            'red_square',
            'blue_plus',
            'green_plus',
            'red_plus',
            'duck',
            'blue_buoy']


#dataset 40k
LABELS40k = ['red_gate_buoy', 
	'green_gate_buoy', 
	'red_buoy', 
	'green_buoy', 
	'yellow_buoy', 
	'black_buoy', 
	'blue_circle', 
	'green_circle', 
	'red_circle', 
	'blue_triangle', 
	'green_triangle', 
	'red_triangle', 
	'blue_square', 
	'green_square', 
	'red_square', 
	'blue_plus', 
	'green_plus', 
	'red_plus', 
	'duck_image', 
	'other', 
	'banner', 
	'blue_buoy', 
	'black_circle', 
	'black_plus', 
	'black_triangle', 
	'black_plus', 
	'blue_racquet_ball', 
	'dock', 
	'rubber_duck', 
	'misc_buoy', 
	'red_racquet_ball', 
	'yellow_racquet_ball']

LABELS=LABELS40k

@dataclass
class YoloConfig:
    classes: int = len(LABELS)
    coordinates: int = 4
    anchors = [10, 14, 23, 27, 37, 58, 81, 82, 135, 169, 344, 319]
    anchorMasks = {"side26": [1, 2, 3], "side13": [3, 4, 5]}
    iouThreshold: float = 0.5
    confidenceThreshold: float = 0.5
    depth_lower_treshold: int = 500
    depth_upper_treshold: int = 10000



# define path to the model, test data directory and results
YOLOV8N_MODEL = os.path.join(get_package_share_directory('sese_vision'),'models','yolov_8n', 'yoloo.json')

OUTPUT_VIDEO = os.path.join("output_records", "abc4.avi")

# define camera preview dimensions same as YOLOv8 model input size [square shape]
CAMERA_PREV_DIM = (640,640)
