from depthai_sdk import OakCamera
import depthai as dai
from depthai_sdk.classes import DetectionPacket, Detections

import rclpy
from rclpy.node import Node

from vision_msgs.msg import ObjectHypothesisWithPose
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

from sese_vision.utilities.config import YOLOV8N_MODEL

class CameraPublisher(Node):
    def __init__(self):
        super().__init__('minimal_publisher')
        self.detections_publisher = self.create_publisher(ObjectHypothesisWithPose, 'Camera/detections', 10)
        self.image_publisher = self.create_publisher(Image, 'Camera/raw_image', 10)

    def publish_detections(self, detections):
        for det in detections:
            detection_msg= self.create_detection_msg(det)
    
            self.detections_publisher.publish(detection_msg)

    def create_detection_msg(self, det):
        detection_msg = ObjectHypothesisWithPose()
                            
        # ID obiektu (np. klasa rozpoznanego obiektu)
        detection_msg.hypothesis.class_id = str(det.label)
        detection_msg.hypothesis.score = det.confidence  # Pewność detekcji
        
        # Pozycja obiektu w przestrzeni 
        pose = Pose()
        pose.position.x = det.spatialCoordinates.x
        pose.position.y = det.spatialCoordinates.y
        pose.position.z = det.spatialCoordinates.z
        
        # Ustawienie orientacji
        pose.orientation.x = 0.0
        pose.orientation.y = 0.0
        pose.orientation.z = 0.0
        pose.orientation.w = 1.0
        
        detection_msg.pose.pose = pose
        return detection_msg


def publish_detections_callback(camera_publisher: CameraPublisher, spatialImgDetectionsPacket: DetectionPacket):
    detections: Detections = spatialImgDetectionsPacket.img_detections.detections
    if detections:
        camera_publisher.publish_detections(detections)
        

def run_camera(camera_publisher: CameraPublisher):
    with OakCamera() as oak:
            color = oak.create_camera('color')

            # https://docs.luxonis.com/projects/sdk/en/latest/features/ai_models/#sdk-supported-models
            nn = oak.create_nn('/home/jetson/sese_git/src/sese_vision/models/yolo_8n/yoloo.json', color, spatial=True)

            nn.config_spatial(
                bb_scale_factor=0.5, # Scaling bounding box before averaging the depth in that ROI
                lower_threshold=900, # Discard depth points below 90cm
                upper_threshold=5000, # Discard depth pints above 5m
                # Average depth points before calculating X and Y spatial coordinates:
                calc_algo=dai.SpatialLocationCalculatorAlgorithm.AVERAGE
            )

            # oak.callback(nn, callback=lambda pkt: publish_detections_callback(camera_publisher, pkt))
            oak.visualize(nn.out.main, fps=True)
            oak.visualize([nn.out.passthrough, nn.out.spatials])
            oak.start(blocking=True)


def main():
    try:
        rclpy.init()
        camera_publisher = CameraPublisher()

        run_camera(camera_publisher)

    except KeyboardInterrupt:
        camera_publisher.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()
    
   