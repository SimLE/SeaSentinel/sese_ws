from sese_vision.utilities.utils import create_pipeline, create_camera_pipeline_long_range
from sese_vision.utilities.config import YOLOV8N_MODEL
import depthai as dai

class BaseCamera:
    def __init__(self, model_name: str):
        self.pipeline = create_camera_pipeline_long_range(YOLOV8N_MODEL)
        self.device = dai.Device(self.pipeline)
        self.q_detections = self.device.getOutputQueue(name="nn_detections", maxSize=4, blocking=False)

    def get_detections(self):
        detections = []
        in_detections = self.q_detections.tryGet()
        if in_detections:
            for detection in in_detections.detections:
                detections.append({
                    "label": detection.label,
                    "confidence": detection.confidence,
                    "spatial_coords": {
                        "x": detection.spatialCoordinates.x,
                        "y": detection.spatialCoordinates.y,
                        "z": detection.spatialCoordinates.z
                    }
                })
        return detections


class OAK_D(BaseCamera):
    def __init__(self, model_name: str):
        super().__init__(model_name)
        self.pipeline = create_pipeline(model_name)
        
        
class OAK_D_Long_Range(BaseCamera):
    def __init__(self, model_name: str):
        super().__init__(model_name)
        
