import rclpy
from rclpy.node import Node
from std_msgs.msg import String
from sese_vision.cameras import OAK_D, OAK_D_Long_Range
import json
from sese_vision.utilities.config import YOLOV8N_MODEL

class CameraNode(Node):
    def __init__(self):
        super().__init__('camera_node')
        self.publisher = self.create_publisher(String, 'camera/detections', 10)
        self.timer = self.create_timer(0.3, self.publish_detections)
        self.camera = OAK_D_Long_Range('camera')

    def publish_detections(self):
        detections = self.camera.get_detections()
        if detections:
            msg = String()
            msg.data = json.dumps(detections)
            self.publisher.publish(msg)
            self.get_logger().info(f"Published detections: {msg.data}")

def main(args=None):
    rclpy.init(args=args)

    node = CameraNode()
    rclpy.spin(node)

    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()