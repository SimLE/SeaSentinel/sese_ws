import cv2
import depthai as dai
import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from vision_msgs.msg import Detection2D, Detection2DArray
from sese_interfaces.msg import SeSeObjectHypothesisWithPose
from geometry_msgs.msg import Pose
from cv_bridge import CvBridge
import json
from pathlib import Path
import os
from rclpy.qos import QoSProfile, ReliabilityPolicy, DurabilityPolicy, HistoryPolicy
from sese_vision.utilities.utils import annotateFrame

from ament_index_python.packages import get_package_share_directory

class JsonManager:
    def __init__(self, config_path, model_path):

        self.config_path = Path(config_path)
        if not self.config_path.exists():
            raise ValueError(f"Path {self.config_path} does not exist!")
        
        self.model_path = Path(model_path)
        if not self.model_path.exists():
            raise ValueError(f"No blob found at: {self.model_path}")
        
        self.config = self._load_config()

    def _load_config(self):
        with self.config_path.open() as f:
            return json.load(f)

    def get_nn_config(self):
        return self.config.get("nn_config", {})

    def get_metadata(self):
        nn_config = self.get_nn_config()
        return nn_config.get("NN_specific_metadata", {})

    def get_labels(self):
        mappings = self.config.get("mappings", {})
        return mappings.get("labels", {})

    def get_model_path(self):
        return self.model_path
    
class PipelineManager:
    def __init__(self, json_manager):
        self.json_manager = json_manager
        self.pipeline = dai.Pipeline()
        
    def _configure_stereo_default(self, stereo):
        # Konfiguracja dla trybu Default
        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
        stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_7x7)
        stereo.initialConfig.setSubpixel(True)  # 3-bit subpixel precision
        stereo.initialConfig.setExtendedDisparity(False)
        
        config = stereo.initialConfig.get()
        config.postProcessing.temporalFilter.enable = True
        config.postProcessing.thresholdFilter.minRange = 400
        config.postProcessing.thresholdFilter.maxRange = 15000
        stereo.initialConfig.set(config)

    def _configure_stereo_face(self, stereo):
        # Konfiguracja dla trybu Face
        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
        stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_7x7)
        stereo.initialConfig.setSubpixel(True)  # 5-bit subpixel precision
        stereo.initialConfig.setExtendedDisparity(True)
        
        config = stereo.initialConfig.get()
        config.postProcessing.temporalFilter.enable = True
        config.postProcessing.thresholdFilter.minRange = 400
        config.postProcessing.thresholdFilter.maxRange = 15000
        stereo.initialConfig.set(config)

    def _configure_stereo_high_detail(self, stereo):
        # Konfiguracja dla trybu High Detail
        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_ACCURACY)
        stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_7x7)
        stereo.initialConfig.setSubpixel(True)  # 5-bit subpixel precision
        stereo.initialConfig.setExtendedDisparity(True)

        config = stereo.initialConfig.get()
        config.postProcessing.temporalFilter.enable = True
        config.postProcessing.thresholdFilter.minRange = 400
        config.postProcessing.thresholdFilter.maxRange = 15000
        stereo.initialConfig.set(config)

    def _configure_stereo_robotics(self, stereo):
        # Konfiguracja dla trybu Robotics
        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
        stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_7x7)
        stereo.initialConfig.setSubpixel(False)  # 3-bit subpixel precision
        stereo.initialConfig.setExtendedDisparity(False)
        
        config = stereo.initialConfig.get()
        config.postProcessing.temporalFilter.enable = False
        config.postProcessing.thresholdFilter.minRange = 400
        config.postProcessing.thresholdFilter.maxRange = 15000
        stereo.initialConfig.set(config)

    def create_OAK_D_pipeline(self):
        nn_config = self.json_manager.get_nn_config()
        metadata = self.json_manager.get_metadata()

        W, H = tuple(map(int, nn_config.get("input_size").split('x')))
        confidence_threshold = metadata.get("confidence_threshold", 0.5)
        classes = metadata.get("classes", 80)
        coordinates = metadata.get("coordinates", 4)
        anchors = metadata.get("anchors", [])
        anchor_masks = metadata.get("anchor_masks", {})
        iou_threshold = metadata.get("iou_threshold", 0.5)

        self.pipeline = dai.Pipeline()

        # Define sources and outputs
        cam_rgb = self.pipeline.create(dai.node.ColorCamera)
        detection_network = self.pipeline.create(dai.node.YoloDetectionNetwork)
        xout_rgb = self.pipeline.create(dai.node.XLinkOut)
        nn_out = self.pipeline.create(dai.node.XLinkOut)

        xout_rgb.setStreamName("rgb")
        nn_out.setStreamName("nn")

        # Properties
        cam_rgb.setPreviewSize(W, H)
        cam_rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
        cam_rgb.setInterleaved(False)
        cam_rgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
        cam_rgb.setFps(40)

        # Network specific settings
        detection_network.setConfidenceThreshold(confidence_threshold)
        detection_network.setNumClasses(classes)
        detection_network.setCoordinateSize(coordinates)
        detection_network.setAnchors(anchors)
        detection_network.setAnchorMasks(anchor_masks)
        detection_network.setIouThreshold(iou_threshold)
        detection_network.setBlobPath(str(self.json_manager.get_model_path()))
        detection_network.setNumInferenceThreads(2)
        detection_network.input.setBlocking(False)

        # Linking
        cam_rgb.preview.link(detection_network.input)
        detection_network.passthrough.link(xout_rgb.input)
        detection_network.out.link(nn_out.input)

    def create_OAK_D_spatial_pipeline(self, stereo_mode='robotics'):
        nn_config = self.json_manager.get_nn_config()
        metadata = self.json_manager.get_metadata()

        W, H = tuple(map(int, nn_config.get("input_size").split('x')))
        confidence_threshold = metadata.get("confidence_threshold", 0.5)
        classes = metadata.get("classes", 80)
        coordinates = metadata.get("coordinates", 4)
        anchors = metadata.get("anchors", [])
        anchor_masks = metadata.get("anchor_masks", {})
        iou_threshold = metadata.get("iou_threshold", 0.5)

        self.pipeline = dai.Pipeline()
        self.pipeline.setXLinkChunkSize(64 * 1024)

        # Define sources and outputs
        cam_rgb = self.pipeline.create(dai.node.ColorCamera)
        monoLeft = self.pipeline.create(dai.node.MonoCamera)
        monoRight = self.pipeline.create(dai.node.MonoCamera)
        spatial_detection_network = self.pipeline.create(dai.node.YoloSpatialDetectionNetwork)
        stereo = self.pipeline.create(dai.node.StereoDepth)
        xout_rgb = self.pipeline.create(dai.node.XLinkOut)
        nn_out = self.pipeline.create(dai.node.XLinkOut)

        xout_rgb.setStreamName("rgb")
        nn_out.setStreamName("nn")

        # Properties
        cam_rgb.setPreviewSize(W, H)
        cam_rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
        cam_rgb.setInterleaved(False)
        cam_rgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
        cam_rgb.setFps(40)

        # StereoDepth properties

        # StereoDepth configuration based on mode
        # https://docs.luxonis.com/software/depthai-components/nodes/stereo_depth#StereoDepth-Depth%20Presets-Depth%20Presets
        if stereo_mode == "default":
            self._configure_stereo_default(stereo)
        elif stereo_mode == "face":
            self._configure_stereo_face(stereo)
        elif stereo_mode == "high_detail":
            self._configure_stereo_high_detail(stereo)
        elif stereo_mode == "robotics":
            self._configure_stereo_robotics(stereo)
        else:
            raise ValueError(f"Unknown stereo mode: {stereo_mode}")
        
        stereo.setDepthAlign(dai.CameraBoardSocket.CAM_A)
        stereo.setOutputSize(monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight())

        # YoloSpatialDetectionNetwork properties
        spatial_detection_network.setConfidenceThreshold(confidence_threshold)
        spatial_detection_network.setNumClasses(classes)
        spatial_detection_network.setCoordinateSize(coordinates)
        spatial_detection_network.setAnchors(anchors)
        spatial_detection_network.setAnchorMasks(anchor_masks)
        spatial_detection_network.setIouThreshold(iou_threshold)
        spatial_detection_network.setBlobPath(str(self.json_manager.get_model_path()))
        spatial_detection_network.setNumInferenceThreads(2)
        spatial_detection_network.input.setBlocking(False)

        # Linking
        cam_rgb.preview.link(spatial_detection_network.input)
        spatial_detection_network.passthrough.link(xout_rgb.input)
        spatial_detection_network.out.link(nn_out.input)
        stereo.depth.link(spatial_detection_network.inputDepth)

        # StereoDepth linking
       
        monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
        monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
        monoLeft.out.link(stereo.left)
        monoRight.out.link(stereo.right)

    def create_OAK_D_LR_spatial_pipeline(self, stereo_mode='robotics'):
        nn_config = self.json_manager.get_nn_config()
        metadata = self.json_manager.get_metadata()
        W, H = tuple(map(int, nn_config.get("input_size").split('x')))
        confidence_threshold = metadata.get("confidence_threshold", 0.5)
        classes = metadata.get("classes", 80)
        coordinates = metadata.get("coordinates", 4)
        anchors = metadata.get("anchors", [])
        anchor_masks = metadata.get("anchor_masks", {})
        iou_threshold = metadata.get("iou_threshold", 0.5)

        self.pipeline = dai.Pipeline()
        self.pipeline.setXLinkChunkSize(64 * 1024)


        camRgb = self.pipeline.create(dai.node.ColorCamera)
        leftRgb = self.pipeline.create(dai.node.ColorCamera)
        rightRgb = self.pipeline.create(dai.node.ColorCamera)
        stereo = self.pipeline.create(dai.node.StereoDepth)
        spatial_detection_network = self.pipeline.create(dai.node.YoloSpatialDetectionNetwork)
        xout_rgb = self.pipeline.create(dai.node.XLinkOut)
        nn_out = self.pipeline.create(dai.node.XLinkOut)

        xout_rgb.setStreamName("rgb")
        nn_out.setStreamName("nn")

        camRgb.setPreviewSize(W, H)
        camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1200_P)
        camRgb.setInterleaved(False)
        camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)
        camRgb.setFps(5)

        # StereoDepth properties

        # StereoDepth configuration based on mode
        # https://docs.luxonis.com/software/depthai-components/nodes/stereo_depth#StereoDepth-Depth%20Presets-Depth%20Presets
        if stereo_mode == "default":
            self._configure_stereo_default(stereo)
        elif stereo_mode == "face":
            self._configure_stereo_face(stereo)
        elif stereo_mode == "high_detail":
            self._configure_stereo_high_detail(stereo)
        elif stereo_mode == "robotics":
            self._configure_stereo_robotics(stereo)
        else:
            raise ValueError(f"Unknown stereo mode: {stereo_mode}")
        
        stereo.setDepthAlign(dai.CameraBoardSocket.CAM_A)
        # stereo.setOutputSize(monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight())

        spatial_detection_network.setBlobPath(str(self.json_manager.get_model_path()))
        spatial_detection_network.setConfidenceThreshold(confidence_threshold)
        spatial_detection_network.setIouThreshold(iou_threshold)
        spatial_detection_network.setNumClasses(classes)
        spatial_detection_network.setCoordinateSize(coordinates)
        spatial_detection_network.setAnchors(anchors)
        spatial_detection_network.setAnchorMasks(anchor_masks)
        spatial_detection_network.setNumInferenceThreads(2)
        spatial_detection_network.setNumNCEPerInferenceThread(1)
        spatial_detection_network.setBoundingBoxScaleFactor(0.5)
        spatial_detection_network.input.setBlocking(False)

        leftRgb.isp.link(stereo.left)
        rightRgb.isp.link(stereo.right)
        camRgb.preview.link(spatial_detection_network.input)
        spatial_detection_network.passthrough.link(xout_rgb.input) 
        spatial_detection_network.out.link(nn_out.input)
        stereo.depth.link(spatial_detection_network.inputDepth)


        # stereo.depth.link(xout_rgb.input) 

        leftRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1200_P)
        leftRgb.setCamera("left")
        leftRgb.setIspScale(2, 3)

        rightRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1200_P)
        rightRgb.setCamera("right")
        rightRgb.setIspScale(2, 3)

        stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
        stereo.setDepthAlign(dai.CameraBoardSocket.CAM_A)
        stereo.setSubpixel(True)

    def get_pipeline(self):
        return self.pipeline


class CameraPublisher(Node):
    EXCLUDED_LABELS = ["dock", "other"]

    def __init__(self, labels):
        super().__init__('minimal_publisher')

        self.labels = labels

        self.qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )

        self.detections_publisher = self.create_publisher(SeSeObjectHypothesisWithPose, 'Camera/detections', self.qos_profile)
        self.image_pub = self.create_publisher(Image, 'rgb_image', self.qos_profile)

    def publish_img(self, img):
        self.image_pub.publish(img)

    def publish_detections(self, detections):
        for det in detections:
            detection_msg= self.create_detection_msg(det)
            if detection_msg.id not in CameraPublisher.EXCLUDED_LABELS:
                self.detections_publisher.publish(detection_msg)

    def create_detection_msg(self, det):
        detection_msg = SeSeObjectHypothesisWithPose()
                            
        # ID obiektu (np. klasa rozpoznanego obiektu)
        detection_msg.id = self.labels[det.label]
        detection_msg.score = det.confidence  # Pewność detekcji
        
        # Pozycja obiektu w przestrzeni 
        pose = Pose()
        pose.position.x = det.spatialCoordinates.x
        pose.position.y = det.spatialCoordinates.y
        pose.position.z = det.spatialCoordinates.z
        
        # Ustawienie orientacji
        pose.orientation.x = 0.0
        pose.orientation.y = 0.0
        pose.orientation.z = 0.0
        pose.orientation.w = 1.0
        
        detection_msg.pose.pose = pose
        return detection_msg


def main(args=None):
    try:
        rclpy.init(args=args)

        configPath = os.path.join(get_package_share_directory('sese_vision'),'models','yolo_8n', 'yoloo.json')
        nnPath = os.path.join(get_package_share_directory('sese_vision'),'models','yolo_8n', 'yoloo_openvino_2022.1_6shave.blob')
        print(f'{configPath=}\n{nnPath=}\n')


        json_manager = JsonManager(configPath, nnPath)
        pipeline_manager = PipelineManager(json_manager)
        pipeline_manager.create_OAK_D_LR_spatial_pipeline()
        pipeline = pipeline_manager.get_pipeline()

        labels = json_manager.get_labels()

        cameraPublisher = CameraPublisher(labels)
        # rclpy.spin(cameraPublisher)
        pipeline = pipeline_manager.get_pipeline()

        labels = json_manager.get_labels()
        print(f'{labels=}')

        with dai.Device(pipeline) as device:
            print("Starting pipeline...")

            # Output queues
            q_rgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
            q_nn = device.getOutputQueue(name="nn", maxSize=4, blocking=False)


            while True:
                in_nn = q_nn.get()
                in_rgb = q_rgb.get()

                if in_rgb is not None:
                    frame = in_rgb.getCvFrame()
                    if in_nn is not None:
                        frame = annotateFrame(frame, in_nn.detections)
                    bridge = CvBridge()
                    ros_image = bridge.cv2_to_imgmsg(frame, encoding="bgr8")
                    cameraPublisher.publish_img(ros_image)

                if in_nn is not None:
                    
                    detections = in_nn.detections
                    cameraPublisher.publish_detections(detections)


    except KeyboardInterrupt:
        cameraPublisher.destroy_node()
        rclpy.shutdown()
            


if __name__ == '__main__':
    main()