from setuptools import find_packages, setup
from glob import glob
import os

package_name = 'sese_vision'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'models'), glob(os.path.join('models' ,'*.blob'))),
        # (os.path.join('share', package_name, 'yolo_8n'), glob(os.path.join('yolo_8n', '*'))),
        (os.path.join('share', package_name, 'models' ,'yolo_8n'), glob(os.path.join('models' ,'yolo_8n', '*.json'))),
        (os.path.join('share', package_name, 'models' ,'yolo_8n'), glob(os.path.join('models' ,'yolo_8n', '*.blob'))),
        # (os.path.join('share', package_name, 'models' ,'yolo_8n'), glob(os.path.join('models' ,'yolo_8n', '*.xml'))),
    ],
    install_requires=['setuptools'],
    maintainer='root',
    maintainer_email='abc@abc.pl',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "camera = sese_vision.camera_node:main",
            "mockCamera = sese_vision.mock_camera_node:main",
            "run_camera = sese_vision.run_camera3:main",
        ],
    },
)
