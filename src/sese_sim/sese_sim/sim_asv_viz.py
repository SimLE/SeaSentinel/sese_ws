import rclpy
import csv
from rclpy.node import Node
from geometry_msgs.msg import Pose
from px4_msgs.msg import VehicleLocalPosition
from sese_interfaces.msg import SeSeObjectHypothesisWithPose
from sese_control.utils import BaseNode
from rclpy.qos import QoSProfile, ReliabilityPolicy, DurabilityPolicy, HistoryPolicy
import math
import random
from ament_index_python.packages import get_package_share_directory

class BuoyVisibilityChecker(BaseNode):
    def __init__(self):
        super().__init__('buoy_visibility_checker')
        self.buoys = []
        self.heading = 0.0
        self.asv_N = 0.0
        self.asv_E = 0.0
        self.asv_visibility_deg = 80    # 80 degrees visibility range
        self.asv_camera_range = 30      # 15 meters camera range
        # self.get_logger().info('BuoyVisibilityChecker node has been initialized.')

        self.subscriber = self.create_qos_subscription(
            VehicleLocalPosition, 
            '/fmu/out/vehicle_local_position', 
            self.local_position_callback)
        
        # Use ament_index_python to find the path to the CSV file in the share directory

        package_share_directory = get_package_share_directory('sese_sim')
        # csv_path = self.declare_parameter('csv_path', f'{package_share_directory}/config/buoys.csv').get_parameter_value().string_value
        # self.get_logger().info(f'CSV path: {csv_path}')

        self.declare_parameter('csv_path', f'{package_share_directory}/config/buoys_v3.csv')
        csv_path = self.get_parameter('csv_path').get_parameter_value().string_value

        self.buoys = self.load_waypoints(csv_path)
        # self.get_logger().info(f'Our buoys: {self.buoys}')

        # self.get_logger().info(f'Loaded {len(self.buoys)} buoys.')

    def load_waypoints(self, csv_path):
        buoys = []
        with open(csv_path, mode='r') as file:
            reader = csv.reader(file)
            next(reader)  # Skip header
            for row in reader:
                try:
                    N, E, buoy_class = float(row[0]), float(row[1]), str(row[2])
                    buoys.append([E, N, buoy_class])
                    # print(buoys)
                except ValueError:
                    pass
                    # self.get_logger().warning(f"Invalid row in CSV file: {row}")
        self.get_logger().info(f'Our buoys: {buoys}')
        return buoys
            
    def local_position_callback(self, msg):
        # Gazebo have switched axis so N = X and E = Y
        self.asv_N = msg.x
        self.asv_E = msg.y
        self.heading = msg.heading

    def calculate_distance(self, buoy_N, buoy_E):
        distance = math.sqrt((buoy_N - self.asv_N) ** 2 + (buoy_E - self.asv_E) ** 2)
        return distance
    
    # Calculate angle between ASV and buoy
    # Remember to give Y value as first argument and X value as second argument in the function !!!!
    def calculate_buoy_alfa(self, buoy_N, buoy_E):
        dE = buoy_E - self.asv_E
        dN = buoy_N - self.asv_N
        angle = math.degrees((math.pi/2 - math.atan2(dN, dE)))
        return angle if angle >= 0 else angle + 360

    # Convert angle to NED
    def angle_to_NED(self, angle):
        if angle >= 0 and angle <= 180:
            return math.radians(angle)
        elif angle > 180 and angle <= 360:
            return math.radians(angle) - 2 * math.pi

    # Normalize angle (rad) to NED
    def normalize_to_NED(self, rad):
        while rad > math.pi:
            rad -= 2 * math.pi
        while rad < -math.pi:
            rad += 2 * math.pi
        return rad
    
    def asv_visibility_range(self, deg_range):
        rad_range = math.radians(deg_range) / 2
        left_limit = self.heading - rad_range
        right_limit = self.heading + rad_range
        left_limit = self.normalize_to_NED(left_limit)
        right_limit = self.normalize_to_NED(right_limit)
        return [left_limit, right_limit]
    
    # Check if buoy is visible
    def is_visible(self, buoy):
        buoy_alfa_deg = self.calculate_buoy_alfa(buoy[1], buoy[0])
        buoy_NED = self.angle_to_NED(buoy_alfa_deg)
        # self.get_logger().info(f'Buoy: {buoy} angle in NED: {buoy_NED}')
        left_limit, right_limit = self.asv_visibility_range(self.asv_visibility_deg)
        if left_limit <= right_limit:
            return left_limit <= buoy_NED <= right_limit and self.calculate_distance(buoy[1], buoy[0]) <= self.asv_camera_range
        else:
            return (buoy_NED >= left_limit or buoy_NED <= right_limit) and self.calculate_distance(buoy[1], buoy[0]) <= self.asv_camera_range

    def get_visible_buoys(self):
        visible_buoys = [buoy for buoy in self.buoys if self.is_visible(buoy)]
        # self.get_logger().info(f'Number of visible buoys: {len(visible_buoys)}')
        return visible_buoys

    def calculate_cam_Z_X(self, buoy):
        # Vector from the boat to the buoy
        def calculate_vector_p(buoy):
            # buoy[E, N, class]. Gazebo have switched axis so N = X and E = Y
            p_x = buoy[0] - self.asv_E
            p_y = buoy[1] - self.asv_N
            return p_x, p_y
        
        # Computes the scalar projection of p onto the heading vector
        def calculate_projection_length():
            p_x, p_y = calculate_vector_p(buoy)
            v_heading_x = math.sin(self.heading)
            v_heading_y = math.cos(self.heading)
            projection_length = p_x * v_heading_x + p_y * v_heading_y
            return projection_length
        
        # Computes the perpendicular distance from the buoy to the end of heading line (projection vector)
        def calculate_perpendicular_distance():
            p_x, p_y = calculate_vector_p(buoy)
            projection_length = calculate_projection_length()
            v_heading_x = math.sin(self.heading)
            v_heading_y = math.cos(self.heading)
            # Find the perpendicular vector
            perp_vector_x = p_x - projection_length * v_heading_x
            perp_vector_y = p_y - projection_length * v_heading_y
            
            # Determine if the buoy is to the right or left of the heading line (projection vector)
            cross_product = perp_vector_x * v_heading_y - perp_vector_y * v_heading_x
            # Perpendicular distance is the magnitude of the perpendicular vector
            # Adjust the sign based on the cross product (positive if to the right, negative if to the left)
            return math.sqrt(perp_vector_x**2 + perp_vector_y**2) * (1 if cross_product >= 0 else -1)

        # Calculate Z (projection length) and X (perpendicular distance)
        Z = calculate_projection_length()
        X = calculate_perpendicular_distance()

        return Z, X


class PublishVisibleBuoys(BaseNode):
    def __init__(self, visibility_checker):
        super().__init__('sim_visible_buoys')
        # self.get_logger().info('PublishVisibleBuoys node has been initialized.')

        # self.labels = [
        #     'red_gate_buoy', 'green_gate_buoy', 'red_buoy', 'green_buoy', 
        #     'yellow_buoy', 'black_buoy', 'blue_circle', 'green_circle', 
        #     'red_circle', 'blue_triangle', 'green_triangle', 'red_triangle', 
        #     'blue_square', 'green_square', 'red_square', 'blue_plus', 
        #     'green_plus', 'red_plus', 'duck_image', 'other', 'banner', 
        #     'blue_buoy', 'black_circle', 'black_plus', 'black_triangle', 
        #     'black_plus', 'blue_racquet_ball', 'dock', 'rubber_duck', 
        #     'misc_buoy', 'red_racquet_ball', 'yellow_racquet_ball'
        # ]

        self.detections_publisher = self.create_qos_publisher(SeSeObjectHypothesisWithPose, 'Camera/detections')

        self.visibility_checker = visibility_checker
        self.create_timer(0.2, self.publish_visible_buoys)

    def publish_visible_buoys(self):
        self.visible_buoys = self.visibility_checker.get_visible_buoys()
        # self.get_logger().info(f'Publishing {len(self.visible_buoys)} visible buoys.')
        for buoy in self.visible_buoys:
            detection_msg = self.create_detection_msg(buoy)
            self.detections_publisher.publish(detection_msg)

    def create_detection_msg(self, buoy):
        _, _, buoy_class = buoy
        Z, X = self.visibility_checker.calculate_cam_Z_X(buoy)

        # Add noise to the Z and X values
        Z += Z * random.uniform(-0.005, 0.005)
        X += X * random.uniform(-0.005, 0.005)

        detection_msg = SeSeObjectHypothesisWithPose()
                                  
        detection_msg.id = buoy_class
        detection_msg.score = random.uniform(0.85, 0.99)
        
        pose = Pose()
        pose.position.x = float(X * 1000) # Convert to meters to millimeters 
        pose.position.y = 0.0
        pose.position.z = float(Z * 1000) # Convert to meters to millimeters 
        
        # self.get_logger().info(f'Buoy: [N: {buoy[1]}, E: {buoy[0]}] -> Z: {pose.position.z}, X: {pose.position.x}, Class: {detection_msg.hypothesis.class_id}, Accuracy: {detection_msg.hypothesis.score}')

        # Doesnt matter. Given values are constant
        pose.orientation.x = 0.0
        pose.orientation.y = 0.0
        pose.orientation.z = 0.0
        pose.orientation.w = 1.0
        
        detection_msg.pose.pose = pose
        return detection_msg


def main(args=None):
    rclpy.init(args=args)
    # Initialize the BuoyVisibilityChecker node
    buoy_visibility_checker = BuoyVisibilityChecker()
    
    # Initialize the PublishVisibleBuoys node with the existing BuoyVisibilityChecker instance
    publish_visible_buoys = PublishVisibleBuoys(buoy_visibility_checker)

    # Spin both nodes
    executor = rclpy.executors.MultiThreadedExecutor()
    executor.add_node(buoy_visibility_checker)
    executor.add_node(publish_visible_buoys)
    executor.spin()

    # Destroy both nodes
    buoy_visibility_checker.destroy_node()
    publish_visible_buoys.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
