import rclpy
from rclpy.node import Node
from px4_msgs.msg import ActuatorMotors
from std_msgs.msg import Float64
from sese_control.utils import BaseNode
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
import math
import subprocess
import threading 

class ActuatorMotorBridge(BaseNode):
    def __init__(self):
        super().__init__('actuator_motor_bridge')

        qos_profile_gz = QoSProfile(
            reliability=ReliabilityPolicy.RELIABLE,
            history=HistoryPolicy.KEEP_LAST,
            depth=10
        )

        self.subscriber_ = self.create_qos_subscription(
            ActuatorMotors,
            '/fmu/out/actuator_motors',
            self.actuators_callback)

        self.publisher_1 = self.create_publisher(Float64, "/model/sese_omni_boat/joint/thruster1_joint/cmd_thrust", qos_profile_gz)
        self.publisher_2 = self.create_publisher(Float64, "/model/sese_omni_boat/joint/thruster2_joint/cmd_thrust", qos_profile_gz)
        self.publisher_3 = self.create_publisher(Float64, "/model/sese_omni_boat/joint/thruster3_joint/cmd_thrust", qos_profile_gz)
        self.publisher_4 = self.create_publisher(Float64, "/model/sese_omni_boat/joint/thruster4_joint/cmd_thrust", qos_profile_gz)

        self.run_subprocess_command()

    def actuators_callback(self, msg):
        thruster1_msg = Float64()
        thruster2_msg = Float64()
        thruster3_msg = Float64()
        thruster4_msg = Float64()

        thrust_scaler = 30
        thruster_data = [float(c) for c in msg.control if not math.isnan(c)]
        thruster1_msg.data = thruster_data[1] * -thrust_scaler
        thruster2_msg.data = thruster_data[0] * -thrust_scaler
        thruster3_msg.data = thruster_data[3] * thrust_scaler
        thruster4_msg.data = thruster_data[2] * thrust_scaler

        self.publisher_1.publish(thruster1_msg)
        self.publisher_2.publish(thruster2_msg)
        self.publisher_3.publish(thruster3_msg)
        self.publisher_4.publish(thruster4_msg)

        # DEBUG
        # self.get_logger().info(f"Published Actuator Data: {thruster_data}")

    def run_subprocess_command(self):
        """Run subprocess command for each thruster in parallel using threading."""
        thruster_topics = [
            "/model/sese_omni_boat/joint/thruster1_joint/cmd_thrust",
            "/model/sese_omni_boat/joint/thruster2_joint/cmd_thrust",
            "/model/sese_omni_boat/joint/thruster3_joint/cmd_thrust",
            "/model/sese_omni_boat/joint/thruster4_joint/cmd_thrust"
        ]
        
        threads = []
        for thruster_topic in thruster_topics:
            command = f"ros2 run ros_gz_bridge parameter_bridge {thruster_topic}@std_msgs/msg/Float64]gz.msgs.Double"
            thread = threading.Thread(target=self.run_command_in_thread, args=(command,))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

    def run_command_in_thread(self, command):
        try:
            subprocess.Popen(command, shell=True)
            self.get_logger().info(f"Successfully started command in background: {command}")
        except Exception as e:
            self.get_logger().error(f"Error running command in background: {e}")

def main(args=None):
    rclpy.init(args=args)
    node = ActuatorMotorBridge()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
