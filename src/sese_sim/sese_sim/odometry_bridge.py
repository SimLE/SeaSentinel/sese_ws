import rclpy
from rclpy.node import Node
from px4_msgs.msg import VehicleLocalPosition
from nav_msgs.msg import Odometry
from sese_control.utils import BaseNode
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
import math
import subprocess
import threading 
import time

class OdometryBridge(BaseNode):
    def __init__(self):
        super().__init__('odometry_bridge')

        qos_profile_px4 = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT, # for px4 mock
            history=HistoryPolicy.KEEP_LAST,
            depth=10
        )

        self.subscriber_ = self.create_subscription(
            Odometry,
            '/sese_omni_boat/local_position',
            self.callback,
            10
        )

        self.publisher_ = self.create_qos_subscription(
            VehicleLocalPosition, 
            '/fmu/in/gazebo_vehicle_local_position')
        
        self.run_subprocess_command()

        self.last_time = None
        self.last_vx = None
        self.last_vy = None
    
    def callback(self, msg):
        vehicle_local_position = VehicleLocalPosition() 

        vehicle_local_position.timestamp = msg.header.stamp.sec
        vehicle_local_position.timestamp_sample = msg.header.stamp.sec
        vehicle_local_position.x = msg.pose.pose.position.y
        vehicle_local_position.y = msg.pose.pose.position.x
        vehicle_local_position.vx = msg.twist.twist.linear.y
        vehicle_local_position.vy = msg.twist.twist.linear.x

        # Calculate heading
        orientation_q = msg.pose.pose.orientation
        siny_cosp = 2 * (orientation_q.w * orientation_q.z + orientation_q.x * orientation_q.y)
        cosy_cosp = 1 - 2 * (orientation_q.y * orientation_q.y + orientation_q.z * orientation_q.z)
        heading = math.atan2(siny_cosp, cosy_cosp)
        vehicle_local_position.heading = -heading

        # Calculate acceleration
        current_time = time.time()
        current_vx = msg.twist.twist.linear.y
        current_vy = msg.twist.twist.linear.x

        if self.last_time is not None:
            dt = current_time - self.last_time
            vehicle_local_position.ax = (current_vx - self.last_vx) / dt
            vehicle_local_position.ay = (current_vy - self.last_vy) / dt
        else:
            vehicle_local_position.ax = 0.0
            vehicle_local_position.ay = 0.0

        self.last_time = current_time
        self.last_vx = current_vx
        self.last_vy = current_vy

        # Publish the message
        self.publisher_.publish(vehicle_local_position)
        self.get_logger().info(
            f"Published Vehicle Local Position:\n"
            f"  Position: (x: {vehicle_local_position.x:.5f}, y: {vehicle_local_position.y:.5f})\n"
            f"  Velocity: (vx: {vehicle_local_position.vx:.5f}, vy: {vehicle_local_position.vy:.5f})\n"
            f"  Acceleration: (ax: {vehicle_local_position.ax:.5f}, ay: {vehicle_local_position.ay:.5f})\n"
            f"  Heading: {vehicle_local_position.heading:.5f}"
        )

    def run_subprocess_command(self):
        """Run the command in separate thread for parameter_bridge. GZ -> ROS2"""
        odometry_topic = "/sese_omni_boat/local_position"
        command = f"ros2 run ros_gz_bridge parameter_bridge {odometry_topic}@nav_msgs/msg/Odometry[gz.msgs.Odometry"
        thread = threading.Thread(target=self.run_command_in_thread, args=(command,))
        thread.start()
        thread.join()

    def run_command_in_thread(self, command):
        try:
            subprocess.Popen(command, shell=True)
            self.get_logger().info(f"Successfully started command in background: {command}")
        except Exception as e:
            self.get_logger().error(f"Error running command in background: {e}")

def main(args=None):
    rclpy.init(args=args)
    node = OdometryBridge()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()