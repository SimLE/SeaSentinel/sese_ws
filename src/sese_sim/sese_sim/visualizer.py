from sese_control.utils import BaseNode
from px4_msgs.msg import VehicleLocalPosition
from sese_control.utils import HNEDPosition, NEDPosition, Obstacle
from sese_interfaces.msg import DetectionArray, DetectionMSG

import rclpy
from geometry_msgs.msg import Point, PointStamped
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import argparse

class TrajectoryVisualizer(BaseNode):
    def __init__(self, refresh_rate=100, shutdown_time=30):
        super().__init__('trajectory_visualizer')

        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback)
        self.asv_position=None

        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self.get_detections_callback)
        self.obstacles: List[Obstacle] = []

       
        self.positions = []  # Lista przechowująca współrzędne trajektorii
        self.buoys_positions = []  # Lista przechowująca pozycje boi
        self.refresh_rate = refresh_rate  # Częstotliwość odświeżania animacji

        self.timer = self.create_timer(self.refresh_rate//1000, self.add_objects)
        self.shutdown_timer = self.create_timer(shutdown_time, self.shutdown_node) 


        # Ustawienia animacji Matplotlib
        self.fig, self.ax = plt.subplots()
        self.sc = self.ax.scatter([], [])  # Wykres scatter dla trajektorii
        self.buoys_sc = self.ax.scatter([], [], c='red', marker='o', label='Buoys')  # Dynamiczne boje
        self.asv = self.ax.plot([], [], 'g-')[0]  # Trójkąt jako ASV
        self.ax.set_xlim(-10, 10)  # Zakres osi X
        self.ax.set_ylim(-10, 10)  # Zakres osi Y
        self.ax.set_xlabel('E')
        self.ax.set_ylabel('N')
        self.ax.legend()
        self.ani = animation.FuncAnimation(
            self.fig, self.update_plot, interval=self.refresh_rate
        )

    def shutdown_node(self):
        self.get_logger().info('\n\n\n\n\n5 minutes passed, shutting down.\n\n\n\n\n\n')
        self.save_animation()
        rclpy.shutdown()


    def get_detections_callback(self, msg: DetectionArray):
        self.obstacles = []
        for detection in msg.detections:
            obstacle = Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=detection.ned_d),
                    id=detection.object_class
                )
            self.obstacles.append(obstacle)

    def vehicle_local_position_callback(self, vehicle_local_position):
        self.asv_position = HNEDPosition(heading=vehicle_local_position.heading, ned=NEDPosition(n=vehicle_local_position.x,e=vehicle_local_position.y,d=0.0))

    def add_objects(self):
        if self.asv_position is None:
            return
        self.positions.append(self.asv_position)
        self.buoys_positions.append(self.obstacles)

    def update_plot(self, frame):
        if frame > len(self.positions):
            print("ups")
            return

        x_data = self.positions[frame].ned.e
        y_data = self.positions[frame].ned.n
        self.sc.set_offsets(np.array([[x_data, y_data]]))
        self.draw_asv(x_data, y_data, self.positions[frame].heading)

        buoys = self.buoys_positions[frame]
        buoy_x = [buoy.ned.e for buoy in buoys]
        buoy_y = [buoy.ned.n for buoy in buoys]
        self.buoys_sc.set_offsets(np.c_[buoy_x, buoy_y])
        # self.get_logger().info(f"{x_data=}")
        # self.get_logger().info(f"{y_data=}")
        # self.get_logger().info(f"{self.positions[frame].heading=}")
        
        # buoys = self.buoys_positions[frame]
        # for buoy in buoys:
        #     buoy_x = buoy.ned.e
        #     buoy_y = buoy.ned.n
        #     self.buoys_sc.set_offsets(np.c_[buoy_x, buoy_y])
        # return self.sc, self.buoys_sc, self.asv

    def draw_asv(self, x, y, heading):
        """Rysowanie trójkąta reprezentującego ASV z aktualnym headingiem."""
        length = 2.5  # Długość trójkąta
        angle_rad = np.deg2rad(heading)
        triangle = np.array([
            [x, y],
            [x - length * np.cos(angle_rad + np.pi / 6), y - length * np.sin(angle_rad + np.pi / 6)],
            [x - length * np.cos(angle_rad - np.pi / 6), y - length * np.sin(angle_rad - np.pi / 6)],
            [x, y]  # Zamknięcie trójkąta
        ])
        self.asv.set_data(triangle[:, 0], triangle[:, 1])

    def save_animation(self):
        """Zapis animacji jako GIF."""
        ani = animation.FuncAnimation(
            self.fig, self.update_plot, frames=len(self.positions), interval=self.refresh_rate, save_count=len(self.positions)
        )
        self.get_logger().info('\n\n\n\n\nSave gif.\n\n\n\n\n\n')
        ani.save('./test.mp4')
        self.get_logger().info('Animation saved as trajectory.gif')


# def main(args=None):
#     parser = argparse.ArgumentParser(description='Trajectory Visualizer with ROS2')
#     parser.add_argument('--refresh_rate', type=int, default=100, help='Refresh rate of animation in milliseconds')
#     parsed_args = parser.parse_args()

#     rclpy.init(args=args)
#     visualizer = TrajectoryVisualizer(refresh_rate=parsed_args.refresh_rate)

#     # Spin ROS2 node
#     rclpy.spin(visualizer)

#     # Po zakończeniu działania zapisanie animacji
#     visualizer.save_animation()
#     visualizer.destroy_node()
#     rclpy.shutdown()

# if __name__ == '__main__':
#     main()

def main(args=None):
    rclpy.init(args=args)
    visualizer = TrajectoryVisualizer(refresh_rate=100)

    # Spin ROS2 node
    rclpy.spin(visualizer)

    # Po zakończeniu działania zapisanie animacji
    visualizer.save_animation()
    visualizer.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()