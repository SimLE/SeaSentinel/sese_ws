import os
from setuptools import setup
from glob import glob

package_name = 'sese_sim'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # Ensure the launch directory is included
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),
        (os.path.join('share', package_name, 'config'), glob(os.path.join('config', 'buoys_v3.csv'))),
        (os.path.join('share', package_name, 'config'), glob(os.path.join('config', 'buoys_v2.csv'))),
        (os.path.join('share', package_name, 'config'), glob(os.path.join('config', 'buoys.csv')))

    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='dux',
    maintainer_email='dux@example.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'odometry_bridge = sese_sim.odometry_bridge:main',
            'actuator_bridge = sese_sim.actuator_bridge:main',
            'sim_asv_viz = sese_sim.sim_asv_viz:main',
            'visualizer = sese_sim.visualizer:main',
        ],
    },
)
