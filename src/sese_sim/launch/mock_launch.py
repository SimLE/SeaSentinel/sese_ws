from launch import LaunchDescription
from launch_ros.actions import Node

from ament_index_python.packages import get_package_share_directory
import os


def generate_launch_description():

    buoys_path = os.path.join(
        get_package_share_directory('sese_sim'),
        'config',
        'buoys_v3.csv'
    )

    return LaunchDescription([

        Node(
            package='sese_control',
            executable='offboardControl',
            name='offboardControl_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task0',
            name='task0_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task1',
            name='task1_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task2',
            name='task2_node',
            output='screen'
        ),
        # Node(                             Kiedyś bedzie na main :)  (mam nadzieje)
        #     package='sese_control',
        #     executable='task3',
        #     name='task3_node',
        #     output='screen'
        # ),
        Node(
            package='sese_control',
            executable='task4',
            name='task4_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task5',
            name='task5_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task6',
            name='task6_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='taskr',
            name='taskr_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='taskManager',
            name='taskManager_node',
            output='screen',
            parameters=[{'task_list': ["Task_0", "Task_1", "Task_2", "Task_6"]}]
        ),
        Node(
            package='sese_control',
            executable='object_list',
            name='object_list_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='px4Mock',
            name='px4Mock_node',
            output='screen',
            parameters=[{'asv_start_position': [-11.5, -20.0, 0.0, 0.0]}]
        ),
        Node(
            package='sese_sim',
            executable='sim_asv_viz',
            name='sim_asv_viz_node',
            output='screen',
            parameters=[{'csv_path': buoys_path}]
        )
    ])