from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='sese_sim',
            executable='odometry_bridge',
            name='odometry_bridge',
            output='screen'
        ),
        Node(
            package='sese_sim',
            executable='actuator_bridge',
            name='actuator_bridge',
            output='screen'
        )
    ])
