import os
from glob import glob
from setuptools import find_packages, setup

package_name = 'sese_control'

setup(
    name=package_name,
    version='0.0.1',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),
        (os.path.join('share', package_name, 'config'), glob(os.path.join('config', 'waypoints.csv'))),
        (os.path.join('share', package_name, 'config'), glob(os.path.join('config', 'instructions.csv')))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='root',
    maintainer_email='abc@abc.pl',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
   entry_points={
    'console_scripts': [
        "waypoint_reader = sese_control.waypoint_reader_node:main",
        "offboardControl = sese_control.offboard_control.CombinedOffboardControl:main",
        "taskManager = sese_control.taskManager.taskManager:main",
        "task0 = sese_control.taskManager.tasks.task_0:main",
        "task1 = sese_control.taskManager.tasks.task_1:main",
        "task2 = sese_control.taskManager.tasks.task_2:main",
        "task3 = sese_control.taskManager.tasks.task_3:main",
        "task4 = sese_control.taskManager.tasks.task_4:main",
        "task5 = sese_control.taskManager.tasks.task_5:main",
        "task6 = sese_control.taskManager.tasks.task_6:main",
        "taskr = sese_control.taskManager.tasks.task_r:main",
        "object_list = sese_control.object_list:main",
        "marker_test = sese_control.marker_test:main",
        "px4Mock = sese_control.px4Mock.px4Mock:main",
    ],
    },
)
