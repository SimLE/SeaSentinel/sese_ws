import unittest
import matplotlib.pyplot as plt
from utils import ArtificialPotentialField, NEDPosition  # Import z utils.py

class TestArtificialPotentialField(unittest.TestCase):

    def setUp(self):
        # Inicjalizacja APF przed każdym testem
        self.apf = ArtificialPotentialField(attractive_power=2.0, repulsive_distance=2.0)

    def plot_scenario(self, test_name, vehicle_pos, destination, detections, temporary_target):
        plt.figure(figsize=(8, 6))
        plt.title(f"{test_name}", fontsize=14)

        # Pozycja pojazdu (Vehicle)
        plt.scatter(vehicle_pos.n, vehicle_pos.e, c='blue', label='Vehicle', s=100)

        # Cel (Destination)
        plt.scatter(destination.n, destination.e, c='green', marker='^', label='Destination', s=150)

        # Przeszkody (Obstacles)
        if detections:
            for obs in detections:
                plt.scatter(obs.n, obs.e, c='red', marker='o', label='Obstacle', s=100)

        # Następny cel (Temporary Target)
        plt.scatter(temporary_target.n, temporary_target.e, c='purple', marker='*', label='Temporary Target', s=200)

        # Rysowanie strzałek (wektory sił)
        plt.quiver(vehicle_pos.n, vehicle_pos.e,
                   temporary_target.n - vehicle_pos.n,
                   temporary_target.e - vehicle_pos.e,
                   angles='xy', scale_units='xy', scale=1, color='gray', width=0.005)

        plt.xlabel('North (m)')
        plt.ylabel('East (m)')
        plt.grid(True)
        plt.legend(loc='best')

        # Zakresy osi dopasowane do scenariusza
        all_points = [vehicle_pos, destination, temporary_target] + detections
        min_n = min(p.n for p in all_points) - 2
        max_n = max(p.n for p in all_points) + 2
        min_e = min(p.e for p in all_points) - 2
        max_e = max(p.e for p in all_points) + 2

        plt.xlim(min_n, max_n)
        plt.ylim(min_e, max_e)

        plt.show()

    def run_scenario(self, test_name, vehicle_pos, destination, detections, proximity_threshold):
        # Użycie metody klasy APF
        target = self.apf.compute_temporary_target(vehicle_pos, destination, detections, proximity_threshold)
        self.plot_scenario(test_name, vehicle_pos, destination, detections, target)
        return target

    def test_reach_destination_without_obstacles(self):
        vehicle_pos = NEDPosition(10, 10, 0)
        destination = NEDPosition(15, 15, 0)
        proximity_threshold = 1.0
        detections = []

        target = self.run_scenario("No Obstacles", vehicle_pos, destination, detections, proximity_threshold)

        self.assertGreater(target.n, vehicle_pos.n)
        self.assertGreater(target.e, vehicle_pos.e)

    def test_near_destination(self):
        vehicle_pos = NEDPosition(4.9, 5.0, 0)
        destination = NEDPosition(5.0, 5.0, 0)
        proximity_threshold = 1.0
        detections = []

        target = self.run_scenario("Near Destination", vehicle_pos, destination, detections, proximity_threshold)

        self.assertEqual(target, destination)

    def test_obstacle_on_path(self):
        vehicle_pos = NEDPosition(4, 4, 0)
        destination = NEDPosition(10, 10, 0)
        proximity_threshold = 1.0
        detections = [NEDPosition(7, 6, 0)]

        target = self.run_scenario("Obstacle On Path", vehicle_pos, destination, detections, proximity_threshold)

        self.assertNotEqual(target.e, 0)
        self.assertGreater(target.e, 4)

    def test_obstacle_off_path(self):
        vehicle_pos = NEDPosition(0, 0, 0)
        destination = NEDPosition(10, 0, 0)
        proximity_threshold = 1.0
        detections = [NEDPosition(5, 5, 0)]

        target = self.run_scenario("Obstacle Off Path", vehicle_pos, destination, detections, proximity_threshold)

        self.assertGreater(target.n, vehicle_pos.n)
        self.assertAlmostEqual(target.e, 0, delta=0.5)

    def test_multiple_obstacles(self):
        vehicle_pos = NEDPosition(0, 0, 0)
        destination = NEDPosition(10, 0, 0)
        proximity_threshold = 1.0
        detections = [
            NEDPosition(4, 1, 0),
            NEDPosition(6, -1, 0),
        ]

        target = self.run_scenario("Multiple Obstacles", vehicle_pos, destination, detections, proximity_threshold)

        self.assertGreater(target.n, vehicle_pos.n)
        self.assertAlmostEqual(target.e, 0, delta=0.5)

    def test_starting_from_nonzero_position(self):
        vehicle_pos = NEDPosition(20, -5, 0)
        destination = NEDPosition(25, 5, 0)
        proximity_threshold = 1.0
        detections = [NEDPosition(22, 0, 0)]

        target = self.run_scenario("Starting from Non-zero Position", vehicle_pos, destination, detections, proximity_threshold)

        self.assertGreater(target.n, vehicle_pos.n)
        self.assertGreater(target.e, vehicle_pos.e)

if __name__ == '__main__':
    unittest.main()