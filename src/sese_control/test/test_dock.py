import numpy as np
from matplotlib import pyplot as plt
from dock import DockTransform


def draw_dock(theta, cn, ce, color):
    p = np.array([[-190, -150],
                  [190, -150],
                  [190, 150],
                  [-190, 150],
                  [-190, -150]])

    translation = np.array([cn, ce])
    rotation_matrix = np.array([
        [np.cos(theta), np.sin(theta)],
        [-np.sin(theta), np.cos(theta)]
    ])
    t_p = (p @ rotation_matrix) + translation
    plt.plot(t_p[:, 1], t_p[:, 0], "-", color=color)


dock = DockTransform()
translations = [
    [0, 0, 0],
    [np.pi/4, 400, 400],
    [0, -600, 0],
    [np.pi/3, 0, 0],
    [np.pi/1.3, 500, -500],
    [-np.pi/6, -500, -500],
]

plt.figure(figsize=(8, 8))
colors = []
for t in translations:
    dock.apply_transform(*t)
    points = dock.get()
    n = [p["ned"]["n"] for p in points]
    e = [p["ned"]["e"] for p in points]
    p = plt.plot(e, n, ".")
    colors.append(p[0].get_color())

plt.legend([f"θ={int(np.round(np.degrees(t[0])))}, dN={t[1]}, dE={t[2]}" for t in translations],
           fontsize='small', loc='lower right')
for i in range(len(translations)):
    draw_dock(*translations[i], colors[i])

plt.xlabel("E")
plt.ylabel("N")
plt.xlim(-900, 700)
plt.ylim(-850, 750)
plt.savefig("./docks.png", format="PNG")
plt.show()

dock.apply_transform()
points = dock.get()

points = sorted(points, key=lambda p: p["ned"]["n"] * 10000 + p["ned"]["e"])
prev = points[0]["ned"]["n"]
for p in points:
    if p["ned"]["n"] > prev:
        print()
    print(p["name"], end=" ")
    prev = p["ned"]["n"]
