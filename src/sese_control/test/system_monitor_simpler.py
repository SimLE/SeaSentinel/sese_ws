import rclpy
from rclpy.node import Node
from px4_msgs.msg import VehicleLocalPosition
from vision_msgs.msg import ObjectHypothesisWithPose
from sese_control.utils import BaseNode
import time

class SystemMonitor(BaseNode):

    def __init__(self):
        super().__init__('system_monitor')

        # Subskrypcja topiców
        self.sub_px4 = self.create_qos_subscription(
            VehicleLocalPosition,
            '/fmu/out/vehicle_local_position',
            self.px4_callback)

        self.sub_oakd = self.create_subscription(
            ObjectHypothesisWithPose,
            '/camera/detections',
            self.oakd_callback,
            10)

        # Inicjalizacja zmiennych do monitorowania stanu systemu
        self.last_px4_msg_time = None
        self.last_px4_msg = None
        self.px4_freeze_start = None

        self.last_oakd_msg_time = None
        self.last_oakd_msg = None
        self.oakd_freeze_start = None

        # Timer do sprawdzania stanu systemu
        self.create_timer(1.0, self.check_system_state)

    def px4_callback(self, msg):
        current_time = time.time()
        self.last_px4_msg_time = current_time

        if self.last_px4_msg is not None and self.last_px4_msg == msg:
            if self.px4_freeze_start is None:
                self.px4_freeze_start = current_time
        else:
            self.px4_freeze_start = None

        self.last_px4_msg = msg

    def oakd_callback(self, msg):
        current_time = time.time()
        self.last_oakd_msg_time = current_time

        if self.last_oakd_msg is not None and self.last_oakd_msg == msg:
            if self.oakd_freeze_start is None:
                self.oakd_freeze_start = current_time
        else:
            self.oakd_freeze_start = None

        self.last_oakd_msg = msg

    def check_system_state(self):
        current_time = time.time()

        # Sekwencje ANSI dla kolorów
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        RED = '\033[91m'
        BLUE = '\033[94m'
        RESET = '\033[0m'

        # Sprawdzenie stanu PX4
        if self.last_px4_msg_time is None:
            px4_status = f'PX4 Topic: {BLUE}System Initialisation{RESET}'
        elif current_time - self.last_px4_msg_time > 10:
            px4_status = f'PX4 Topic: {RED}System Error{RESET}'
        elif self.px4_freeze_start is not None and current_time - self.px4_freeze_start > 30:
            px4_status = f'PX4 Topic: {YELLOW}System Freeze{RESET}'
        else:
            px4_status = f'PX4 Topic: {GREEN}System Active{RESET}'

        # Sprawdzenie stanu OakD
        if self.last_oakd_msg_time is None:
            oakd_status = f'OakD Topic: {BLUE}System Initialisation{RESET}'
        elif current_time - self.last_oakd_msg_time > 10:
            oakd_status = f'OakD Topic: {RED}System Error{RESET}'
        elif self.oakd_freeze_start is not None and current_time - self.oakd_freeze_start > 30:
            oakd_status = f'OakD Topic: {YELLOW}System Freeze{RESET}'
        else:
            oakd_status = f'OakD Topic: {GREEN}System Active{RESET}'

        # Nadpisywanie komunikatów w konsoli
        print("\033[2J\033[H", end='')  # Czyści konsolę i ustawia kursor na górze
        print(px4_status)
        print(oakd_status)

def main(args=None):
    rclpy.init(args=args)
    system_monitor = SystemMonitor()
    rclpy.spin(system_monitor)
    system_monitor.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()