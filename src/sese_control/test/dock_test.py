from sese_control.utils import Obstacle, NEDPosition, distance_between_points
from sese_control.pathPlanner.convexPlanner import ConvexPlanner
from sese_control.pathPlanner.deafultPlanner import DeafultPlanner
from sese_control.pathPlanner.corridorPlanner import CorridorPlanner
from sese_control.areaPredictor.predictor import AreaPredictor 
from sese_control.taskManager.tasks import DockTransform
import numpy as np
import matplotlib.pyplot as plt
import math

def establish_dock(banners):

    feet_to_meter = 0.3048

    centers = [
        [(banners[i].ned.n + banners[j].ned.n)/2,
        (banners[i].ned.e + banners[j].ned.e)/2]
        for i, j in [[1,2],[0,2],[0,1]]]

    # print(f'{centers[0]=}')
    # print(f'{banners[0]=}')
    # distance_between_points((centers[0][0], centers[0][1]), (banners[0].ned.n + banners[0].ned.e))
    dists = [distance_between_points((centers[i][0], centers[i][1]), (banners[i].ned.n, banners[i].ned.e)) for i in range(3)]

    M_idx = dists.index(min(dists))
    L_idx = 0 if M_idx != 0 else 1
    R_idx = 1 if M_idx == 2 else 2

    NM, EM = (banners[M_idx].ned.n, banners[M_idx].ned.e)
    NL, EL = (banners[L_idx].ned.n, banners[L_idx].ned.e)
    NR, ER = (banners[R_idx].ned.n, banners[R_idx].ned.e)

    # not so quick maths
    aLR = (NL - NR)/(EL - ER)
    aMX = -1./aLR
    bMX = NM - aMX*EM
    a = 1. + aMX**2
    b = 2*aMX*bMX - 2*EM - 2*aMX*NM
    MX_dist = 10. * feet_to_meter
    c = NM**2 + EM**2 - MX_dist**2 + bMX**2 - 2*NM*bMX
    d_sqrt = b**2 - 4*a*c
    NX1 = (-b - d_sqrt)/(2*a)
    NX2 = (-b + d_sqrt)/(2*a)
    EX1 = (NX1 - bMX)/aMX
    EX2 = (NX2 - bMX)/aMX

    asv_pos = (0.5, -0.5)
    if distance_between_points([NX1, EX1], asv_pos) > distance_between_points([NX2, EX2], asv_pos):
        dock_center = [NX1, EX1]
    else:
        dock_center = [NX2, EX2]

    return math.atan(aMX), *dock_center, feet_to_meter
        # self.dock_transform.apply_transform(math.atan(aMX), *dock_center, feet_to_meter)

BANNER_LABELS = [
        'blue_circle',
        'green_circle',
        'red_circle',
        'blue_triangle',
        'green_triangle',
        'red_triangle',
        'blue_square',
        'green_square',
        'red_square'
    ]



if __name__ == '__main__':

    dock = DockTransform()

    obstaclesv2 = [
    #     # Task 0
    #     Obstacle(ned=NEDPosition(n=24.97, e=-10.0, d=0.0), id='red_gate_buoy'), 
    #     Obstacle(ned=NEDPosition(n=20.02, e=-5.05, d=0.0), id='red_gate_buoy'), 
    #     Obstacle(ned=NEDPosition(n=22.85, e=-12.12, d=0.0),id='green_gate_buoy'), 
    #     Obstacle(ned=NEDPosition(n=17.90, e=-7.17, d=0.0), id='green_gate_buoy'),

    #     # Task 1
    
    # Obstacle(NEDPosition(15.5, -7.5, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(13.5, -8.0, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(12.0, -9.5, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(10.5, -11.0, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(8.5, -11.0, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(6.5, -10.5, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(4.5, -8.0, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(4.5, -5.5, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(4.0, -3.0, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(3.0, -0.5, 0.0), "green_buoy"),
    
    # Obstacle(NEDPosition(16.0, -4.5, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(14.0, -4.5, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(12.0, -6.0, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(11.0, -8.0, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(8.5, -8.8, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(6.5, -7.5, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(6.5, -5.25, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(7.5, -3.0, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(5.5, 1.0, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(6.0, -1.0, 0.0), "red_buoy"),


    # Obstacle(NEDPosition(6.5, -2.7, 0.0), "yellow_buoy"),
    # Obstacle(NEDPosition(13.0, -5.5, 0.0), "yellow_buoy"),
    # Obstacle(NEDPosition(14.3, -7.2, 0.0), "yellow_buoy"),
    # Obstacle(NEDPosition(7.8, -9.0, 0.0), "yellow_buoy"),

    # #Task 4
    # Obstacle(NEDPosition(23.5, 13.0, 0.0), "blue_buoy"),

    # Obstacle(NEDPosition(20.0, 10.0, 0.0), "black_buoy"),
    # Obstacle(NEDPosition(24.0, 12.0, 0.0), "black_buoy"),
    # Obstacle(NEDPosition(21.3, 6.5, 0.0), "black_buoy"),
    # Obstacle(NEDPosition(26.0, 5.9, 0.0), "black_buoy"),
    # Obstacle(NEDPosition(21.2, 12.2, 0.0), "black_buoy"),

    # Obstacle(NEDPosition(25.0, 0.0, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(22.2, 0.0, 0.0), "red_buoy"),
    # Obstacle(NEDPosition(25.0, 0.9, 0.0), "green_buoy"),
    # Obstacle(NEDPosition(22.2, 0.9, 0.0), "red_buoy"),

    # #Task 5a
    # Obstacle(NEDPosition(13.0, -9.0, 0.0), "black_plus"),
    # Obstacle(NEDPosition(23.3, 8.0, 0.0), "black_triangle"),
    # Obstacle(NEDPosition(18.1, 4.0, 0.0), "black_triangle"),

    # #Task 5b (dock)
    # Obstacle(ned=NEDPosition(n=12.20, e=5.90, d=0.0), id='black_plus'), 
    # Obstacle(ned=NEDPosition(n=12.22, e=1.63, d=0.0), id='black_plus'), 
    # Obstacle(ned=NEDPosition(n=10.05, e=8.04, d=0.0), id='black_triangle'), 

    #Task 3
    Obstacle(ned=NEDPosition(n=13.63, e=3.05, d=0.0), id='blue_circle'), 
    Obstacle(ned=NEDPosition(n=11.49, e=5.19, d=0.0), id='green_triangle'), 
    Obstacle(ned=NEDPosition(n=9.35, e=7.33, d=0.0), id='red_square'), 
    Obstacle(ned=NEDPosition(n=13.28, e=2.69, d=0.0), id='green_triangle'), 
    Obstacle(ned=NEDPosition(n=11.14, e=4.83, d=0.0), id='red_square'), 
    Obstacle(ned=NEDPosition(n=8.99, e=6.98, d=0.0), id='blue_square'),

    #  #Task 6
    # Obstacle(NEDPosition(29.0, -10.0, 0.0), "black_buoy"),
    # Obstacle(NEDPosition(29.0, -7.0, 0.0), "black_buoy"),

    ]

    # Przygotowanie danych do wykresu
    obstacle_y = [obstacle.ned.n for obstacle in obstaclesv2]
    obstacle_x = [obstacle.ned.e for obstacle in obstaclesv2]
    obstacle_colors = []
    for obstacle in obstaclesv2:
        if obstacle.id == "red_gate_buoy" or obstacle.id == "red_buoy":
            obstacle_colors.append('red')
        elif obstacle.id == "green_gate_buoy" or obstacle.id == "green_buoy":
            obstacle_colors.append('green')
        elif obstacle.id == "yellow_buoy":
            obstacle_colors.append("yellow")
        elif obstacle.id == "black_buoy":
            obstacle_colors.append("black")
        elif obstacle.id == "blue_buoy":
            obstacle_colors.append("blue")
        elif obstacle.id == "black_triangle":
            obstacle_colors.append("orange")
        elif obstacle.id == "black_plus":
            obstacle_colors.append("gray")
        elif obstacle.id in ["blue_circle", "green_triangle", "red_square", "blue_square"]:
            obstacle_colors.append("purple")

    banners = [obs for obs in obstaclesv2 if obs.id in BANNER_LABELS]
    a,b,c, d = establish_dock(banners)
    dock.apply_transform(a, b, c, d)

    points = dock.get()
    n = [p["ned"]["n"] for p in points]
    e = [p["ned"]["e"] for p in points]
    print(f'{n=}')

    plt.figure(figsize=(10, 8))
    plt.scatter(obstacle_x, obstacle_y, color=obstacle_colors, marker='o', s=25, label='Obstacles')
    plt.scatter(e, n, color='orange', marker='*', s=25, label='dock_transform')

    plt.ylabel('N (North)')
    plt.xlabel('E (East)')
    plt.title('buoys_v2.csv')
    plt.legend()
    plt.grid(True)
    plt.xlim(-15.0, 50.0)
    plt.ylim(0.0, 65.0)
        


    plt.savefig("./Dock_test.png", format="PNG")