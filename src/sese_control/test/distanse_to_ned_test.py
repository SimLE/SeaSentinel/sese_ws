import unittest
import math

def distance_to_ned_sway_surge(sway, surge, ned_n, ned_e, heading):
    new_n = surge * math.cos(heading) - sway * math.sin(heading) + ned_n
    new_e = surge * math.sin(heading) + sway * math.cos(heading) + ned_e
    return [new_n, new_e]

def distance_to_ned_displacement_distance(displacement, distance, ned_n, ned_e, yaw):
    beta = math.asin(displacement / distance)
    surge = distance * math.cos(beta)

    new_n = surge * math.cos(yaw) - displacement * math.sin(yaw) + ned_n
    new_e = surge * math.sin(yaw) + displacement * math.cos(yaw) + ned_e
    return [new_n, new_e]

class TestDistanceConversion(unittest.TestCase):
    
    def check_error(self, sway, surge, displacement, distance, ned_n, ned_e, heading, expected_n, expected_e, test_name):
        print(f'\nRunning test: {test_name}')
        ned_sway_surge = distance_to_ned_sway_surge(sway, surge, ned_n, ned_e, heading)
        ned_displacement_distance = distance_to_ned_displacement_distance(displacement, distance, ned_n, ned_e, heading)
        
        print(f'Expected NED: [{expected_n}, {expected_e}]')
        print(f'distance_to_ned_sway_surge: {ned_sway_surge}')
        print(f'distance_to_ned_displacement_distance: {ned_displacement_distance}')
        
        error_n = abs(ned_sway_surge[0] - ned_displacement_distance[0])
        error_e = abs(ned_sway_surge[1] - ned_displacement_distance[1])

        return error_n, error_e
    
    def test_forward_motion(self):
        error_n, error_e = self.check_error(
            sway=0, surge=5, displacement=0, distance=5,
            ned_n=10, ned_e=10, heading=0,
            expected_n=15, expected_e=10,
            test_name='Forward Motion'
        )
        self.assertAlmostEqual(error_n, 0, places=6)
        self.assertAlmostEqual(error_e, 0, places=6)

    def test_side_offset(self):
        error_n, error_e = self.check_error(
            sway=2, surge=5, displacement=2, distance=math.sqrt(5**2 + 2**2),
            ned_n=0, ned_e=0, heading=math.radians(45),
            expected_n=5 * math.cos(math.radians(45)) - 2 * math.sin(math.radians(45)),
            expected_e=5 * math.sin(math.radians(45)) + 2 * math.cos(math.radians(45)),
            test_name='Side Offset'
        )
        self.assertAlmostEqual(error_n, 0, places=6)
        self.assertAlmostEqual(error_e, 0, places=6)

    def test_heading_90_deg(self):
        error_n, error_e = self.check_error(
            sway=1, surge=3, displacement=1, distance=math.sqrt(3**2 + 1**2),
            ned_n=5, ned_e=5, heading=math.radians(90),
            expected_n=5 - 1,
            expected_e=5 + 3,
            test_name='Heading 90 Degrees'
        )
        self.assertAlmostEqual(error_n, 0, places=6)
        self.assertAlmostEqual(error_e, 0, places=6)

    def test_large_displacement(self):
        error_n, error_e = self.check_error(
            sway=5, surge=10, displacement=5, distance=math.sqrt(10**2 + 5**2),
            ned_n=3, ned_e=7, heading=math.radians(30),
            expected_n=3 + 10 * math.cos(math.radians(30)) - 5 * math.sin(math.radians(30)),
            expected_e=7 + 10 * math.sin(math.radians(30)) + 5 * math.cos(math.radians(30)),
            test_name='Large Displacement'
        )
        self.assertAlmostEqual(error_n, 0, places=6)
        self.assertAlmostEqual(error_e, 0, places=6)

    def test_negative_sway(self):
        error_n, error_e = self.check_error(
            sway=-3, surge=4, displacement=-3, distance=math.sqrt(4**2 + (-3)**2),
            ned_n=0, ned_e=0, heading=math.radians(0),
            expected_n=4,
            expected_e=-3,
            test_name='Negative Sway'
        )
        self.assertAlmostEqual(error_n, 0, places=6)
        self.assertAlmostEqual(error_e, 0, places=6)
    
    def test_incorrect_surge_as_distance(self):
        error_n, error_e = self.check_error(
            sway=2, surge=5, displacement=2, distance=5,  # distance powinno być sqrt(5^2+2^2)
            ned_n=0, ned_e=0, heading=math.radians(45),
            expected_n=None, expected_e=None,
            test_name='Incorrect: Surge used as Distance'
        )
        self.assertGreater(error_n, 0.01)
        self.assertGreater(error_e, 0.01)
    
    def test_incorrect_distance_as_surge(self):
        error_n, error_e = self.check_error(
            sway=2, surge=math.sqrt(5**2 + 2**2), displacement=2, distance=math.sqrt(5**2 + 2**2),  # surge powinno być 5
            ned_n=0, ned_e=0, heading=math.radians(45),
            expected_n=None, expected_e=None,
            test_name='Incorrect: Distance used as Surge'
        )
        self.assertGreater(error_n, 0.01)
        self.assertGreater(error_e, 0.01)

if __name__ == '__main__':
    unittest.main()