import rclpy
from std_msgs.msg import String

# import sys
# sys.path.append("~/Sese/sese_tomka/sese_ws/src/sese_control/sese_control")

from sese_control.utils import BaseNode

class MyNode(BaseNode):
    def __init__(self):
        super().__init__('my_node')
        
        # Example publisher
        self.publisher = self.create_qos_publisher(String, 'example_topic')
        
        # Example subscription
        self.subscription = self.create_qos_subscription(String, 'example_topic', self.callback)

        # Timer
        self.create_timer(1.0, self.publish_message)

    def callback(self, msg):
        self.get_logger().info(f"Received message: {msg.data}")

    def publish_message(self):
        msg = String()
        msg.data = "Hello from MyNode!"
        self.publisher.publish(msg)
        self.get_logger().info("Published message.")

def main(args=None):
    rclpy.init(args=args)
    node = MyNode()

    # ROS2 stuff
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()