from sese_control.utils import Obstacle, NEDPosition
from sese_control.pathPlanner.convexPlanner import ConvexPlanner
from sese_control.pathPlanner.deafultPlanner import DeafultPlanner
from sese_control.pathPlanner.corridorPlanner import CorridorPlanner
from sese_control.areaPredictor.predictor import AreaPredictor 
import numpy as np

def rotate(obstacle, theta, dn, de):
    obs = np.array([obstacle.ned.n, obstacle.ned.e])
    translation = np.array([dn, de])
    rotation_matrix = np.array([
            [np.cos(theta), np.sin(theta)],
            [-np.sin(theta), np.cos(theta)]
        ])
    out = ((obs) @ rotation_matrix) + translation
    return Obstacle(NEDPosition(out[0], out[1], 0.0), obstacle.id)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    # from home to buyo
    start_point = (16, 16) # gate position
    goal_point = (15.9, 16.2) # home

    obstacles = [
    # Task 0
    Obstacle(NEDPosition(-12.0, -12.0, 0.0), "red_gate_buoy"),
    Obstacle(NEDPosition(12.0, -12.0, 0.0), "red_gate_buoy"),
    Obstacle(NEDPosition(-12.0, -10.0, 0.0), "green_gate_buoy"),
    Obstacle(NEDPosition(12.0, -10.0, 0.0), "green_gate_buoy"),

    # Task 1
    Obstacle(NEDPosition(14.0, -10.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(15.5, -9.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(16.75, -8.9, 0.0), "green_buoy"),
    Obstacle(NEDPosition(18.0, -9.7, 0.0), "green_buoy"),
    Obstacle(NEDPosition(19.25, -9.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(20.5, -8.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(21.0, -6.75, 0.0), "green_buoy"),
    Obstacle(NEDPosition(21.0, -5.25, 0.0), "green_buoy"),
    Obstacle(NEDPosition(21.5, -4.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(22.75, -3.5, 0.0), "green_buoy"),
    Obstacle(NEDPosition(14.0, -13.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(15.5, -12.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(16.75, -12.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(19.0, -11.8, 0.0), "red_buoy"),
    Obstacle(NEDPosition(20.75, -11.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(21.75, -10.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(23.0, -9.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(24.0, -7.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(24.75, -5.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(26.0, -3.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(16.0, -10.0, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(18.25, -11.7, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(21.0, -10.2, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(21.5, -4.8, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(25.3, -3.7, 0.0), "yellow_buoy"),

    #Task 4
    Obstacle(NEDPosition(16.0, -0.5, 0.0), "blue_buoy"),
    Obstacle(NEDPosition(18.0, 0.0, 0.0), "black_buoy"),
    Obstacle(NEDPosition(11.0, -1.0, 0.0), "black_buoy"),
    Obstacle(NEDPosition(12.3, 8.1, 0.0), "black_buoy"),
    Obstacle(NEDPosition(16.0, 5.9, 0.0), "black_buoy"),
    Obstacle(NEDPosition(14.0, 3.2, 0.0), "black_buoy"),
    Obstacle(NEDPosition(17.2, 15.2, 0.0), "green_buoy"),
    Obstacle(NEDPosition(14.8, 15.2, 0.0), "red_buoy"),
    Obstacle(NEDPosition(17.2, 16.4, 0.0), "green_buoy"),
    Obstacle(NEDPosition(14.8, 16.4, 0.0), "red_buoy"),

    #Task 5a
    Obstacle(NEDPosition(16.2, -12.8, 0.0), "black_plus"),
    Obstacle(NEDPosition(12.0, 1.3, 0.0), "black_triangle"),
    Obstacle(NEDPosition(17.2, 12.1, 0.0), "black_triangle"),

    #Task 5b (dock)
    Obstacle(NEDPosition(-4.03, 12.8, 0.0), "black_plus"),
    Obstacle(NEDPosition(-1.0, 9.8, 0.0), "black_plus"),
    Obstacle(NEDPosition(-7.06, 12.8, 0.0), "black_triangle"),
    #Task 3
    Obstacle(NEDPosition(-1.0, 11.8, 0.0), "blue_circle"),
    Obstacle(NEDPosition(-4.03, 11.8, 0.0), "green_triangle"),
    Obstacle(NEDPosition(-7.06, 11.8, 0.0), "red_square"),

    Obstacle(NEDPosition(-1.0, 11.3, 0.0), "green_triangle"),
    Obstacle(NEDPosition(-4.03, 11.3, 0.0), "red_square"),
    Obstacle(NEDPosition(-7.06, 11.3, 0.0), "blue_square"),


    #Task 6
    Obstacle(NEDPosition(-15.0, -5.0, 0.0), "black_buoy"),
    Obstacle(NEDPosition(-18.0, -5.0, 0.0), "black_buoy"),
    ]

    obstaclesv2 = [
        # Task 0
        Obstacle(ned=NEDPosition(n=24.97, e=-10.0, d=0.0), id='red_gate_buoy'), 
        Obstacle(ned=NEDPosition(n=20.02, e=-5.05, d=0.0), id='red_gate_buoy'), 
        Obstacle(ned=NEDPosition(n=22.85, e=-12.12, d=0.0),id='green_gate_buoy'), 
        Obstacle(ned=NEDPosition(n=17.90, e=-7.17, d=0.0), id='green_gate_buoy'),

        # Task 1
    
    Obstacle(NEDPosition(15.5, -7.5, 0.0), "green_buoy"),
    Obstacle(NEDPosition(13.5, -8.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(12.0, -9.5, 0.0), "green_buoy"),
    Obstacle(NEDPosition(10.5, -11.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(8.5, -11.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(6.5, -10.5, 0.0), "green_buoy"),
    Obstacle(NEDPosition(4.5, -8.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(4.5, -5.5, 0.0), "green_buoy"),
    Obstacle(NEDPosition(4.0, -3.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(3.0, -0.5, 0.0), "green_buoy"),
    
    Obstacle(NEDPosition(16.0, -4.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(14.0, -4.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(12.0, -6.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(11.0, -8.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(8.5, -8.8, 0.0), "red_buoy"),
    Obstacle(NEDPosition(6.5, -7.5, 0.0), "red_buoy"),
    Obstacle(NEDPosition(6.5, -5.25, 0.0), "red_buoy"),
    Obstacle(NEDPosition(7.5, -3.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(5.5, 1.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(6.0, -1.0, 0.0), "red_buoy"),


    Obstacle(NEDPosition(6.5, -2.7, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(13.0, -5.5, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(14.3, -7.2, 0.0), "yellow_buoy"),
    Obstacle(NEDPosition(7.8, -9.0, 0.0), "yellow_buoy"),

    #Task 4
    Obstacle(NEDPosition(23.5, 13.0, 0.0), "blue_buoy"),

    Obstacle(NEDPosition(20.0, 10.0, 0.0), "black_buoy"),
    Obstacle(NEDPosition(24.0, 12.0, 0.0), "black_buoy"),
    Obstacle(NEDPosition(21.3, 6.5, 0.0), "black_buoy"),
    Obstacle(NEDPosition(26.0, 5.9, 0.0), "black_buoy"),
    Obstacle(NEDPosition(21.2, 12.2, 0.0), "black_buoy"),

    Obstacle(NEDPosition(25.0, 0.0, 0.0), "green_buoy"),
    Obstacle(NEDPosition(22.2, 0.0, 0.0), "red_buoy"),
    Obstacle(NEDPosition(25.0, 0.9, 0.0), "green_buoy"),
    Obstacle(NEDPosition(22.2, 0.9, 0.0), "red_buoy"),

    #Task 5a
    Obstacle(NEDPosition(13.0, -9.0, 0.0), "black_plus"),
    Obstacle(NEDPosition(23.3, 8.0, 0.0), "black_triangle"),
    Obstacle(NEDPosition(18.1, 4.0, 0.0), "black_triangle"),

    #Task 5b (dock)
    Obstacle(ned=NEDPosition(n=12.20, e=5.90, d=0.0), id='black_plus'), 
    Obstacle(ned=NEDPosition(n=12.22, e=1.63, d=0.0), id='black_plus'), 
    Obstacle(ned=NEDPosition(n=10.05, e=8.04, d=0.0), id='black_triangle'), 

    #Task 3
    Obstacle(ned=NEDPosition(n=13.63, e=3.05, d=0.0), id='blue_circle'), 
    Obstacle(ned=NEDPosition(n=11.49, e=5.19, d=0.0), id='green_triangle'), 
    Obstacle(ned=NEDPosition(n=9.35, e=7.33, d=0.0), id='red_square'), 
    Obstacle(ned=NEDPosition(n=13.28, e=2.69, d=0.0), id='green_triangle'), 
    Obstacle(ned=NEDPosition(n=11.14, e=4.83, d=0.0), id='red_square'), 
    Obstacle(ned=NEDPosition(n=8.99, e=6.98, d=0.0), id='blue_square'),

     #Task 6
    Obstacle(NEDPosition(29.0, -10.0, 0.0), "black_buoy"),
    Obstacle(NEDPosition(29.0, -7.0, 0.0), "black_buoy"),

    ]

    # dock = [

        
    # Obstacle(NEDPosition(-4.03, 12.8, 0.0), "black_plus"),
    # Obstacle(NEDPosition(-1.0, 9.8, 0.0), "black_plus"),
    # Obstacle(NEDPosition(-7.06, 12.8, 0.0), "black_triangle"),
    #     #Task 3
    # Obstacle(NEDPosition(-1.0, 11.8, 0.0), "blue_circle"),
    # Obstacle(NEDPosition(-4.03, 11.8, 0.0), "green_triangle"),
    # Obstacle(NEDPosition(-7.06, 11.8, 0.0), "red_square"),

    # Obstacle(NEDPosition(-1.0, 11.3, 0.0), "green_triangle"),
    # Obstacle(NEDPosition(-4.03, 11.3, 0.0), "red_square"),
    # Obstacle(NEDPosition(-7.06, 11.3, 0.0), "blue_square"),
    # ]

    # obstacles_test = [rotate(ob, -np.pi/4, 6, -6) for ob in dock]
    # print(f'{obstacles_test=}')
    # obstaclesv2.extend(obstacles_test)

    planner = ConvexPlanner()
    
    area_predictor = AreaPredictor(obstaclesv2)
    task_2_area = area_predictor.find_task_2_area()
    task_4_area = area_predictor.find_task_4_area()
    task_6_area = area_predictor.find_task_6_area()

    if task_4_area:
        buoys_inside_task4 = [
                obs for obs in obstacles 
                if task_4_area["y_min"] <= obs.ned.e <= task_4_area["y_max"] and 
                task_4_area["x_min"] <= obs.ned.n <= task_4_area["x_max"]
            ]
        path = planner.findPath(start_point, goal_point, buoys_inside_task4)
    else:
        path = planner.findPath(start_point, goal_point, obstacles)
    
    print("Computed Path:", path)

    # rysowanie
    # Przygotowanie danych do wykresu
    obstacle_y = [obstacle.ned.n for obstacle in obstaclesv2]
    obstacle_x = [obstacle.ned.e for obstacle in obstaclesv2]
    obstacle_colors = []
    for obstacle in obstaclesv2:
        if obstacle.id == "red_gate_buoy" or obstacle.id == "red_buoy":
            obstacle_colors.append('red')
        elif obstacle.id == "green_gate_buoy" or obstacle.id == "green_buoy":
            obstacle_colors.append('green')
        elif obstacle.id == "yellow_buoy":
            obstacle_colors.append("yellow")
        elif obstacle.id == "black_buoy":
            obstacle_colors.append("black")
        elif obstacle.id == "blue_buoy":
            obstacle_colors.append("blue")
        elif obstacle.id == "black_triangle":
            obstacle_colors.append("orange")
        elif obstacle.id == "black_plus":
            obstacle_colors.append("gray")
        elif obstacle.id in ["blue_circle", "green_triangle", "red_square", "blue_square"]:
            obstacle_colors.append("purple")

    waypoints_y = [point[0] for point in path[:-1]]
    waypoints_x = [point[1] for point in path[:-1]]

    path_x = [start_point[1], *waypoints_x, goal_point[1]]   
    path_y = [start_point[0], *waypoints_y, goal_point[0]]

    # Tworzenie wykresu
    plt.figure(figsize=(10, 8))

    # Rysowanie przeszkód (czerwone kółka)
    plt.scatter(obstacle_x, obstacle_y, color=obstacle_colors, marker='o', s=25, label='Obstacles')

    # Rysowanie punktu startowego (trójkąt)
    # plt.scatter(start_point[1], start_point[0], color='blue', marker='^', s=35, label='Start')

    # # Rysowanie punktu końcowego (fioletowa gwiazdka)
    # plt.scatter(goal_point[1], goal_point[0], color='purple', marker='*', s=35, label='Goal')

    # # # Rysowanie ścieżki (zielone kwadraty)
    # plt.scatter(waypoints_x, waypoints_y, color='purple', marker='d', s=35, label='Path')

    # # # Dodanie linii łączącej punkty ścieżki
    # plt.plot(path_x, path_y, color='purple', linestyle='--', linewidth=1)

    # if task_2_area:
    #         plt.plot([task_2_area['y_min'], task_2_area['y_max'], task_2_area['y_max'], task_2_area['y_min'], task_2_area['y_min']],
    #                  [task_2_area['x_min'], task_2_area['x_min'], task_2_area['x_max'], task_2_area['x_max'], task_2_area['x_min']],
    #                  color='green', linestyle='--', label='Task 2 Area')
        
    # if task_4_area:
    #     plt.plot([task_4_area['y_min'], task_4_area['y_max'], task_4_area['y_max'], task_4_area['y_min'], task_4_area['y_min']],
    #                 [task_4_area['x_min'], task_4_area['x_min'], task_4_area['x_max'], task_4_area['x_max'], task_4_area['x_min']],
    #                 color='blue', linestyle='--', label='Task 4 Area')

    # if task_6_area:
    #     plt.plot([task_6_area['y_min'], task_6_area['y_max'], task_6_area['y_max'], task_6_area['y_min'], task_6_area['y_min']],
    #                 [task_6_area['x_min'], task_6_area['x_min'], task_6_area['x_max'], task_6_area['x_max'], task_6_area['x_min']],
    #                 color='red', linestyle='--', label='Task 6 Area')


    # Dodanie etykiet i legendy
    plt.ylabel('N (North)')
    plt.xlabel('E (East)')
    plt.title('buoys_v2.csv')
    plt.legend()
    plt.grid(True)
    plt.xlim(-15.0, 15.0)
    plt.ylim(0.0, 30.0)
        


    plt.savefig("./Graph.png", format="PNG")