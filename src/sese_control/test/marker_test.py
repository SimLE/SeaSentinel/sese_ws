import rclpy
from rclpy.node import Node
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import TransformStamped
import sese_control.utils as utils
import tf2_ros
from dataclasses import dataclass

@dataclass
class Detection:
    task_number: int
    object_class: str
    class_id: int
    prediction_confidence: float
    position_longitude: float
    position_latitude: float
    position_n: float
    position_e: float

def create_marker(marker_id, frame_id, position, marker_type, scale, color, transparency):
    marker = Marker()
    marker.header.frame_id = frame_id
    marker.action = Marker.ADD
    marker.id = marker_id
    marker.pose.position.x = position[0]
    marker.pose.position.y = position[1]
    marker.pose.position.z = position[2]
    marker.type = marker_type
    marker.scale.x = scale
    marker.scale.y = scale
    marker.scale.z = scale
    marker.color.r = color[0]
    marker.color.g = color[1]
    marker.color.b = color[2]
    marker.color.a = transparency
    marker.lifetime.sec = 0
    marker.lifetime.nanosec = 0
    return marker

class ObjectDictionary(utils.BaseNode):
    def __init__(self):
        super().__init__('object_dictionary')

        self.marker_publisher = self.create_qos_publisher(MarkerArray, '/visualization_marker_array')
        self.pose_publisher = self.create_qos_publisher(Marker, '/visualization_map_pose')
        self.destination_publisher = self.create_qos_publisher(Marker, '/visualization_map_dest')
        self.tf_broadcaster = tf2_ros.StaticTransformBroadcaster(self)
        self.setup_static_tf()

        self.detections = [
            Detection(1, "red_buoy", 1, 0.92, 120.5, -80.3, 5.0, 1.0),
            Detection(2, "green_buoy", 1, 0.85, 122.0, -81.5, 6.1, 5.1),
            Detection(3, "yellow_buoy", 1, 0.78, 123.4, -83.2, 7.2, 3.2),
        ]

        self.vehicle_local_position = (0.0, 0.0)
        self.local_destination = (10.0, 10.0)

        self.create_timer(1.0, self.publish_markers)

    def setup_static_tf(self):
        static_transform = TransformStamped()
        static_transform.header.stamp = self.get_clock().now().to_msg()
        static_transform.header.frame_id = "world"
        static_transform.child_frame_id = "map"
        static_transform.transform.translation.x = 0.0
        static_transform.transform.translation.y = 0.0
        static_transform.transform.translation.z = 0.0
        static_transform.transform.rotation.w = 1.0
        self.tf_broadcaster.sendTransform(static_transform)

    def publish_markers(self):
        self.publish_position_marker()
        self.publish_destination_marker()
        self.publish_obstacle_markers()

    def publish_obstacle_markers(self):
        marker_array = MarkerArray()
        buoy_scale = 2.0

        marker_types = {
            'red_gate_buoy': (Marker.CUBE, (1.0, 0.0, 0.0)),
            'green_gate_buoy': (Marker.CUBE, (0.0, 1.0, 0.0)),
            'red_buoy': (Marker.SPHERE, (1.0, 0.0, 0.0)),
            'green_buoy': (Marker.SPHERE, (0.0, 1.0, 0.0)),
            'yellow_buoy': (Marker.SPHERE, (1.0, 1.0, 0.0)),
            'black_buoy': (Marker.SPHERE, (0.0, 0.0, 0.0)),
            'blue_buoy': (Marker.SPHERE, (0.0, 0.0, 1.0)),
        }

        for i, detection in enumerate(self.detections):
            if detection.object_class in marker_types:
                marker_type, color = marker_types[detection.object_class]
                position = (detection.position_n, detection.position_e, 0.0)
                marker = create_marker(i, "map", position, marker_type, buoy_scale, color, 0.5)
                marker_array.markers.append(marker)

        self.marker_publisher.publish(marker_array)

    def publish_position_marker(self):
        if self.vehicle_local_position:
            pose_msg = create_marker(
                marker_id=1000,
                frame_id="map",
                position=(self.vehicle_local_position[0], self.vehicle_local_position[1], 0.0),
                marker_type=Marker.CUBE,
                scale=2.0,
                color=(0.0, 0.0, 1.0),
                transparency=1.0
            )
            self.pose_publisher.publish(pose_msg)

    def publish_destination_marker(self):
        if self.local_destination:
            destination_msg = create_marker(
                marker_id=1001,
                frame_id="map",
                position=(self.local_destination[0], self.local_destination[1], 0.0),
                marker_type=Marker.CUBE,
                scale=2.0,
                color=(1.0, 0.0, 1.0),
                transparency=1.0
            )
            self.destination_publisher.publish(destination_msg)

def main(args=None):
    rclpy.init(args=args)
    node = ObjectDictionary()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
