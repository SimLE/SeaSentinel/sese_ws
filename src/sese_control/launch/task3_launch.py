from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        Node(
            package='sese_control',
            executable='offboardControl',
            name='offboardControl_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task3',
            name='task3_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='taskManager',
            name='taskManager_node',
            output='screen',
            parameters=[{'task_list': ["Task_3"]}]
        ),
        Node(
            package='sese_control',
            executable='object_list',
            name='object_list_node',
            output='screen'
        ),
        Node(
            package='sese_vision',
            executable='run_camera',
            name='run_camera_node',
            output='screen'
        )
    ])