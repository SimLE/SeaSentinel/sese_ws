from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    return LaunchDescription([

        Node(
            package='sese_control',
            executable='offboardControl',
            name='offboardControl_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task0',
            name='task0_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='task1',
            name='task1_node',
            output='screen'
        ),
        Node(
            package='sese_control',
            executable='taskManager',
            name='taskManager_node',
            output='screen',
            parameters=[{'task_list': ["Task_0", "Task_1"]}] # Task_0 dla znalezienia bramy
        ),
        Node(
            package='sese_control',
            executable='object_list',
            name='object_list_node',
            output='screen'
        ),
        Node(
            package='sese_vision',
            executable='run_camera',
            name='run_camera_node',
            output='screen'
        )
    ])