from launch import LaunchDescription
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
import os


def generate_launch_description():
    config_path = os.path.join(
        get_package_share_directory('sese_control'),
        'config',
        'waypoints.csv'
    )


    return LaunchDescription([
        
        Node(
            package='sese_control',
            executable='waypoint_reader',
            name='waypoint_reader_node',
            output='screen',
            parameters=[{'csv_path': config_path}]
        ),
        Node(
            package='sese_control',
            executable='differentialOffboardControl',
            name='offboardControl_node',
            output='screen'
        )
    ])