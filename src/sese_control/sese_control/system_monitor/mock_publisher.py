# Legacy code
# Mockowanie danych z PX4 i OakD, losowymi wartościami
# Zastąpione px4Mock

import rclpy
from rclpy.node import Node
from px4_msgs.msg import VehicleLocalPosition
from vision_msgs.msg import ObjectHypothesisWithPose
from sese_control.utils import BaseNode
import random

class MockPublisher(BaseNode):
    def __init__(self, mock_types):
        super().__init__('mock_publisher')
        self.mock_types = mock_types
        self.marker_types = [
            'red_gate_buoy',
            'green_gate_buoy',
            'red_buoy',
            'green_buoy',
            'yellow_buoy',
            'black_buoy',
            'blue_buoy'
        ]

        # Sprawdzenie, czy należy mockować PX4
        if 'PX4' in self.mock_types:
            self.px4_publisher_ = self.create_qos_publisher(VehicleLocalPosition, '/fmu/out/vehicle_local_position')
            self.px4_timer_ = self.create_timer(0.5, self.publish_px4)
            self.get_logger().info('Mocking PX4 data.')

        # Sprawdzenie, czy należy mockować OakD
        if 'OakD' in self.mock_types:
            self.detection_publisher_ = self.create_qos_publisher(ObjectHypothesisWithPose, '/Camera/detections')
            self.detection_timer_ = self.create_timer(0.5, self.publish_detection)
            self.get_logger().info('Mocking OakD image data.')

    def publish_px4(self):
        msg = VehicleLocalPosition()
        msg.x = random.uniform(0, 100)  # Przykładowa wartość x
        msg.y = random.uniform(0, 100)  # Przykładowa wartość y
        msg.z = random.uniform(0, 100)  # Przykładowa wartość z
        msg.heading = random.uniform(0, 360)  # Przykładowy kąt
        self.px4_publisher_.publish(msg)
        self.get_logger().info('Publishing mock px4 data.')

    def publish_detection(self):
        msg = ObjectHypothesisWithPose()
        # Tworzenie mocku hipotezy obiektu
        object_hypothesis = ObjectHypothesisWithPose()

        # Losowanie losowej klasy z marker_types
        random_class = random.choice(self.marker_types)       
        object_hypothesis.hypothesis.class_id = random_class # Przypisanie do class_id
        object_hypothesis.hypothesis.score = random.uniform(0.5, 1.0)  # Losowy wynik detekcji

        # Losowa pozycja obiektu
        object_hypothesis.pose.pose.position.x = random.uniform(-1000.0, 1000.0)
        object_hypothesis.pose.pose.position.y = random.uniform(-1000.0, 1000.0)
        object_hypothesis.pose.pose.position.z = random.uniform(1200.0, 20000.0)

        # Publikacja hipotezy z pozycją
        self.detection_publisher_.publish(object_hypothesis)
        self.get_logger().info('Publishing mock object hypothesis data.')

def main(args=None):
    rclpy.init(args=args)

    # Sprawdzenie wyboru systemów mockowanych
    if len(args) > 1:
        mock_types = args[1:]
    else:
        print('Usage: ros2 run mock_px4_oakd mock_publisher.py PX4|OakD|PX4 OakD')
        return

    mock_publisher = MockPublisher(mock_types)
    rclpy.spin(mock_publisher)
    mock_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    import sys
    main(sys.argv)