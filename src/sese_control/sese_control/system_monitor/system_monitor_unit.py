import rclpy
from rclpy.node import Node
from px4_msgs.msg import VehicleLocalPosition
from sese_interfaces.msg import SeSeObjectHypothesisWithPose
from sese_control.utils import BaseNode
import time

class SystemMonitor(BaseNode):

    def __init__(self, system_name, topic_name, msg_type, node):
        super().__init__(f'{system_name}_monitor')
        self.node = node #Wczytanie głównego node do obsugiwania subskrybcji
        
        print(f'Subcriber Initialisation: {system_name}_monitor')
        # Subskrypcja topicu
        self.subscription = self.node.create_qos_subscription(
            msg_type,
            topic_name,
            self.callback
        )

        # Inicjalizacja zmiennych do monitorowania stanu systemu
        self.system_name = system_name
        self.last_msg_time = None
        self.last_msg = None
        self.freeze_start = None

    def callback(self, msg):
        current_time = time.time()
        self.last_msg_time = current_time

        if self.last_msg is not None and self.last_msg == msg:
            if self.freeze_start is None:
                self.freeze_start = current_time
        else:
            self.freeze_start = None

        self.last_msg = msg

    def check_system_state(self):
        current_time = time.time()
        # Sekwencje ANSI dla kolorów
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        RED = '\033[91m'
        BLUE = '\033[94m'
        RESET = '\033[0m'

        # Sprawdzenie stanu systemu
        if self.last_msg_time is None:
            status = f'{self.system_name} Topic: {BLUE}System Initialisation{RESET}'
        elif current_time - self.last_msg_time > 5:
            status = f'{self.system_name} Topic: {RED}System Error{RESET}'
        elif self.freeze_start is not None and current_time - self.freeze_start > 3:
            status = f'{self.system_name} Topic: {YELLOW}System Freeze{RESET}'
        else:
            status = f'{self.system_name} Topic: {GREEN}System Active{RESET}'

        return status

class MultiSystemMonitor(BaseNode):

    def __init__(self):
        super().__init__('multi_system_monitor')
        self.monitors = []

        # Timer do sprawdzania stanu systemu
        self.create_timer(1.0, self.check_all_system_states)

    def add_system(self, system_name, topic_name, msg_type):
        monitor = SystemMonitor(system_name, topic_name, msg_type, self)
        self.monitors.append(monitor)

    def check_all_system_states(self):
        statuses = [monitor.check_system_state() for monitor in self.monitors]

        # Nadpisywanie komunikatów w konsoli
        print("\033[2J\033[H", end='')  # Czyści konsolę i ustawia kursor na górze
        for status in statuses:
            print(status)

def main(args=None):
    rclpy.init(args=args)
    multi_system_monitor = MultiSystemMonitor()

    # Dodanie systemów do monitorowania
    multi_system_monitor.add_system('PX4', '/fmu/out/vehicle_local_position', VehicleLocalPosition)
    multi_system_monitor.add_system('OakD', '/Camera/detections', SeSeObjectHypothesisWithPose)

    # ROS2 stuff
    rclpy.spin(multi_system_monitor)
    multi_system_monitor.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()