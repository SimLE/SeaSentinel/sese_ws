#!/usr/bin/env python

import rclpy
from px4_msgs.msg import VehicleGlobalPosition, VehicleLocalPosition
from sese_interfaces.msg import DetectionArray, DetectionMSG, SeSeObjectHypothesisWithPose, OffboardControlPosition
import sese_control.utils as utils
import json
from datetime import datetime
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import TransformStamped
import os
import tf2_ros
from typing import List

class ObjectDictionary(utils.BaseNode):
    def __init__(self):
        super().__init__('object_dictionary')

        self.vehicle_global_position_subscriber = self.create_qos_subscription(
            VehicleGlobalPosition, '/fmu/out/vehicle_global_position', self.vehicle_global_position_callback
        )
        self.destination_point_subscriber = self.create_qos_subscription(
            OffboardControlPosition, 'CombinedOffboardControl/goal_pose', self.destination_point_callback
        ) 
        self.detections_subscriber = self.create_qos_subscription(
            SeSeObjectHypothesisWithPose, 'Camera/detections', self.update_positions
        )
        self.vehicle_local_position_subscriber = self.create_qos_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback
        )

        self.marker_publisher = self.create_qos_publisher(MarkerArray, '/visualization_marker_array')
        self.pose_publisher = self.create_qos_publisher(Marker, '/visualization_map_pose')
        self.destination_publisher = self.create_qos_publisher(Marker, '/visualization_map_dest')
        self.object_publisher = self.create_qos_publisher(DetectionArray, 'Object_list/obstacles')

        self.tf_broadcaster = tf2_ros.StaticTransformBroadcaster(self)
        self.setup_static_tf()

        self.vehicle_global_position:utils.GlobalPosition = None
        self.vehicle_global_reference:utils.GlobalPosition = None
        self.vehicle_local_position:utils.HNEDPosition = None
        self.local_destination: utils.NEDPosition = None
        self.detection_list: List[utils.Detection] = []

        self.create_timer(30.0, self.save_to)
        self.create_timer(5.0, self.publish_all_markers)

    def setup_static_tf(self):
        static_transform = TransformStamped()
        static_transform.header.stamp = self.get_clock().now().to_msg()
        static_transform.header.frame_id = "world"
        static_transform.child_frame_id = "map"
        static_transform.transform.translation.x = 0.0
        static_transform.transform.translation.y = 0.0
        static_transform.transform.translation.z = 0.0
        static_transform.transform.rotation.w = 1.0
        self.tf_broadcaster.sendTransform(static_transform)

    def publish_obstacle_markers(self):
        marker_array = MarkerArray()
        buoy_scale = 0.2
        banner_scale = 2.0

        marker_types = {
            'red_gate_buoy': (Marker.CUBE, (1.0, 0.0, 0.0)),
            'green_gate_buoy': (Marker.CUBE, (0.0, 1.0, 0.0)),
            'red_buoy': (Marker.SPHERE, (1.0, 0.0, 0.0)),
            'green_buoy': (Marker.SPHERE, (0.0, 1.0, 0.0)),
            'yellow_buoy': (Marker.SPHERE, (1.0, 1.0, 0.0)),
            'black_buoy': (Marker.SPHERE, (0.0, 0.0, 0.0)),
            'blue_buoy': (Marker.SPHERE, (0.0, 0.0, 1.0)),
            'black_plus': (Marker.CUBE, (0.0, 0.0, 0.0)),
            'black_triangle': (Marker.CUBE, (1.0, 1.0, 0.0)),
        }

        banner_types = {
            'blue_circle': (Marker.CYLINDER, (0.0, 0.0, 1.0)),
            'green_circle': (Marker.CYLINDER, (0.0, 1.0, 0.0)),
            'red_circle': (Marker.CYLINDER, (1.0, 0.0, 0.0)),
            'blue_triangle': (Marker.CYLINDER, (0.0, 0.0, 1.0)),
            'green_triangle': (Marker.CYLINDER, (0.0, 1.0, 0.0)),
            'red_triangle': (Marker.CYLINDER, (1.0, 0.0, 0.0)),
            'blue_square': (Marker.CYLINDER, (0.0, 0.0, 1.0)),
            'green_square': (Marker.CYLINDER, (0.0, 1.0, 0.0)),
            'red_square': (Marker.CYLINDER, (1.0, 0.0, 0.0)),
            'blue_plus': (Marker.CYLINDER, (0.0, 0.0, 1.0)),
            'green_plus': (Marker.CYLINDER, (0.0, 1.0, 0.0)),
            'red_plus': (Marker.CYLINDER, (1.0, 0.0, 0.0)),
            'banner': (Marker.CYLINDER, (1.0, 1.0, 0.0)),
        }

        for i, detection in enumerate(self.detection_list):
            if detection.object_class in banner_types:
                marker_type, color = banner_types[detection.object_class]
                position = (detection.ned.n, detection.ned.e, 0.0)
                marker = utils.create_marker(i, "map", position, marker_type, banner_scale, color, 1.0)
                marker_array.markers.append(marker)
            elif detection.object_class in marker_types:
                marker_type, color = marker_types[detection.object_class]
                position = (detection.ned.n, detection.ned.e, 0.0)
                marker = utils.create_marker(i, "map", position, marker_type, buoy_scale, color, 0.5)
                marker_array.markers.append(marker)
        self.marker_publisher.publish(marker_array)
        
    def publish_position_marker(self):
        if self.vehicle_local_position:
            pose_msg = utils.create_marker(
                marker_id=1000,
                frame_id="map",
                position=(self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e, 0.0),
                marker_type=Marker.CUBE,
                scale=0.5,
                color=(0.0, 0.0, 1.0),
                transparency=1.0
            )
            self.pose_publisher.publish(pose_msg)

    def publish_destination_marker(self):
        if self.local_destination:
            destination_msg = utils.create_marker(
                marker_id=1001,
                frame_id="map",
                position=(self.local_destination.n, self.local_destination.e, 0.0),
                marker_type=Marker.CUBE,
                scale=0.5,
                color=(1.0, 0.0, 1.0),
                transparency=1.0
            )
            self.destination_publisher.publish(destination_msg)
        
    def publish_all_markers(self):
        self.publish_destination_marker()
        self.publish_obstacle_markers()
        self.publish_position_marker()
        self.publish_objects()

    def save_to(self):
        filename = datetime.now().strftime("%Y-%m-%d_%H.json")
        file_path = os.path.join('..', filename)
        with open(file_path, 'w') as json_file:
            json.dump([det.to_dict() for det in self.detection_list], json_file, indent=4)
        print(f'Plik JSON "{filename}" został utworzony lub nadpisany.')

    def publish_objects(self):
        msg2 = DetectionArray()
        for i, detection in enumerate(self.detection_list):
            msg = DetectionMSG()
            msg.object_class = detection.object_class
            msg.class_id = detection.class_id
            msg.ned_n = detection.ned.n
            msg.ned_e = detection.ned.e
            msg.ned_d = detection.ned.d
            msg2.detections.append(msg)
        self.object_publisher.publish(msg2)

    def vehicle_global_position_callback(self, msg):
        self.vehicle_global_position = utils.GlobalPosition(lat=msg.lat, lon=msg.lon, alt=msg.alt)

    def vehicle_local_position_callback(self, msg):
        ned_position = utils.NEDPosition(n=msg.x, e=msg.y, d=msg.z)
        self.vehicle_local_position = utils.HNEDPosition(heading=msg.heading, ned=ned_position)
        self.vehicle_global_reference = utils.GlobalPosition(lat=msg.ref_lat, lon=msg.ref_lon, alt=msg.ref_alt)
        self.publish_position_marker()

    def destination_point_callback(self, msg) -> None:
        if msg.control_mode == utils.ControlMode.GLOBAL_MODE:
            self.local_destination = utils.NEDPosition(n=msg.x, e=msg.y, d=msg.z)
        elif msg.control_mode == utils.ControlMode.DIFFERENTIAL_MODE:
            surge = msg.x
            sway = msg.y
            new_n, new_e = utils.distance_to_ned2(
                sway,
                surge, 
                self.vehicle_local_position.ned.n, 
                self.vehicle_local_position.ned.e, 
                self.vehicle_local_position.heading
                )
            new_d = 0.0
            self.local_destination = utils.NEDPosition(n=new_n, e=new_e, d=new_d)
        self.publish_destination_marker()

    def find_highest_id_for_class(self, object_class: str) -> int:
        highest_id = 0
        for detection in self.detection_list:
            if detection.object_class == object_class and detection.class_id > highest_id:
                highest_id = detection.class_id
        return highest_id

    # Funkcja do aktualizacji ID dla wykryć tej samej klasy
    def update_class_ids(self, object_class: str):
        class_detections = [d for d in self.detection_list if d.object_class == object_class]
        class_detections.sort(key=lambda d: d.class_id)  # Sortowanie dla spójności

        # Przypisanie nowych ID od 1 wzwyż
        for new_id, detection in enumerate(class_detections, start=1):
            detection.class_id = new_id

    def object_hypothesis_callback(self, object_class: str, object_ned: utils.NEDPosition, object_score: float, detection_distance: float):
        vgr = self.vehicle_global_reference
        object_global = utils.GlobalPosition(
            *utils.ned_to_geodetic(
                vgr.lat, vgr.lon, vgr.alt,
                object_ned.n, object_ned.e, object_ned.d
            )
        )

        closest_distance = float('inf')
        closest_detection = None
        detection_to_replace = None

        base_min_distance = 0.5
        base_max_distance = 2.0
        
        if detection_distance > 15:
            max_distance = min(4.0, 2.0 + (detection_distance - 15) / 7.5)
            min_distance = min(2.0, 0.5 + (detection_distance - 15) / 10)
        else:
            max_distance = base_max_distance
            min_distance = base_min_distance

        for detection in self.detection_list:
            distance = utils.local_distance(
                detection.ned.n, detection.ned.e,
                object_ned.n, object_ned.e
            )

            if detection.object_class == object_class: # Szukanie najbliższego wykrycia tej samej klasy
                if distance < closest_distance:
                    closest_distance = distance
                    closest_detection = detection
            else: # Filtrowanie wykryć innych klas w promieniu 0.5 m
                if distance <= min_distance:
                    current_importance = object_score / (detection_distance + 1e-6)
                    existing_importance = detection.prediction_confidence / (detection.detection_distance + 1e-6)

                    if current_importance > existing_importance:
                        detection_to_replace = detection
                        # print(f"Replacing detection of {detection_to_replace.object_class} with {object_class}")
                    else:
                        # print(f"Ignoring detection of {object_class} due to stronger existing detection")
                        return  # Pomijanie dodania nowego wykrycia

        if detection_to_replace:  # Jeśli znaleziono obiekt do zastąpienia (inna klasa)
            removed_class = detection_to_replace.object_class
            self.detection_list.remove(detection_to_replace)
            self.update_class_ids(removed_class)

            new_class_id = self.find_highest_id_for_class(object_class) + 1
            self.detection_list.append(utils.Detection(
                object_class=object_class,
                class_id=new_class_id,
                prediction_confidence=object_score,
                gps=object_global,
                ned=object_ned,
                detection_distance=detection_distance
            ))
            self.publish_obstacle_markers()
            self.publish_objects()
        elif closest_detection:
            # print('Closest distance between objects:', closest_distance)
            if min_distance <= closest_distance <= max_distance:
                # print('Distance <= ', max_distance)
                if object_score > closest_detection.prediction_confidence:
                    # print('Higher accuracy')
                    closest_detection.gps = object_global
                    closest_detection.prediction_confidence = object_score
                    closest_detection.ned = object_ned
                    closest_detection.detection_distance = detection_distance
                else:
                    # print('Lower accuracy')
                    weight = object_score
                    closest_detection.gps.lat = (1 - weight) * closest_detection.gps.lat + weight * object_global.lat
                    closest_detection.gps.lon = (1 - weight) * closest_detection.gps.lon + weight * object_global.lon
                    closest_detection.gps.alt = (1 - weight) * closest_detection.gps.alt + weight * object_global.alt
                    closest_detection.prediction_confidence = object_score
                    closest_detection.ned.n = (1 - weight) * closest_detection.ned.n + weight * object_ned.n
                    closest_detection.ned.e = (1 - weight) * closest_detection.ned.e + weight * object_ned.e
                    closest_detection.ned.d = (1 - weight) * closest_detection.ned.d + weight * object_ned.d
                    closest_detection.detection_distance = detection_distance
                self.publish_obstacle_markers()
                #self.publish_objects()
            elif closest_distance > max_distance:
                # print('New object in class')
                new_class_id = self.find_highest_id_for_class(object_class) + 1
                self.detection_list.append(utils.Detection(
                    object_class=object_class,
                    class_id=new_class_id,
                    prediction_confidence=object_score,
                    gps=object_global,
                    ned=object_ned,
                    detection_distance=detection_distance
                ))
                self.publish_obstacle_markers()
                self.publish_objects()
        else:
            # print('Adding first object of the class')
            self.detection_list.append(utils.Detection(
                object_class=object_class,
                class_id=1,
                prediction_confidence=object_score,
                gps=object_global,
                ned=object_ned,
                detection_distance=detection_distance
            ))
            self.publish_obstacle_markers()
            self.publish_objects()

    def update_positions(self, data):
        if self.vehicle_local_position is None:
            self.get_logger().info(f"{self.vehicle_local_position=}")
            return
        # detected buoy data
        object_class = data.id
        score = data.score
        # dividing by 1000 to get result in meters
        sway = data.pose.pose.position.x / 1000.0
        surge = data.pose.pose.position.z / 1000.0
        distance = utils.get_real_distance(sway, surge)

        if surge != 0.0: 
            detection_coords = utils.distance_to_ned2(
                sway, 
                surge, 
                self.vehicle_local_position.ned.n, 
                self.vehicle_local_position.ned.e, 
                self.vehicle_local_position.heading
            )
            object_ned = utils.NEDPosition(
                n=detection_coords[0], 
                e=detection_coords[1], 
                d=0.0
            )
            self.object_hypothesis_callback(object_class, object_ned, score, distance)
                
def main(args=None):
    rclpy.init(args=args)
    node = ObjectDictionary()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()