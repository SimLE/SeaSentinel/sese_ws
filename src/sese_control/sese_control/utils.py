import math
import geopy
from geopy.distance import geodesic
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, DurabilityPolicy, HistoryPolicy
from dataclasses import dataclass
from visualization_msgs.msg import Marker
from typing import List, Tuple

class ControlMode:
    GLOBAL_MODE = True
    DIFFERENTIAL_MODE = False

@dataclass
class NEDPosition:
    n: float
    e: float
    d: float


@dataclass
class HNEDPosition:
    heading: float
    ned: NEDPosition
    

@dataclass
class GlobalPosition:
    lat: float
    lon: float
    alt: float

@dataclass
class Obstacle:
    ned: NEDPosition
    id: str
    
@dataclass
class GlobalPosition:
    lat: float
    lon: float
    alt: float

@dataclass
class Detection:
    object_class: str
    class_id: int
    prediction_confidence: float
    gps: GlobalPosition
    ned: NEDPosition
    detection_distance: float

    def to_dict(self):
        return {
            'object_class': self.object_class,
            'class_id': self.class_id,
            'prediction_confidence': self.prediction_confidence,
            'gps': vars(self.gps),
            'ned': vars(self.ned),
            'detection_distance': self.detection_distance
        }

def create_marker(marker_id, frame_id, position, marker_type, scale, color, transparency):
    marker = Marker()
    marker.header.frame_id = frame_id
    marker.action = Marker.ADD
    marker.id = marker_id
    marker.pose.position.x = position[0]
    marker.pose.position.y = position[1]
    marker.pose.position.z = position[2]
    marker.type = marker_type
    marker.scale.x = scale
    marker.scale.y = scale
    marker.scale.z = scale
    marker.color.r = color[0]
    marker.color.g = color[1]
    marker.color.b = color[2]
    marker.color.a = transparency
    return marker

def ned_to_geodetic(latitude_reference, longitude_reference, altitude_reference, X_NED, Y_NED, Z_NED):
    ref_coords = (latitude_reference, longitude_reference)

    distance = math.sqrt(X_NED**2 + Y_NED**2)
    azimuth = math.atan2(Y_NED, X_NED)
    azimuth_degrees = math.degrees(azimuth)

    target_coords = geopy.distance.distance(meters=distance).destination(ref_coords, bearing=azimuth_degrees)
    target_altitude = altitude_reference - Z_NED

    return [target_coords[0], target_coords[1], target_altitude]

def calculate_initial_compass_bearing(pointA, pointB):
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])
    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1) * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing

def geodetic_to_ned(latitude, longitude, altitude, reference_lat, reference_lon, reference_alt):

    ref_coords = (reference_lat, reference_lon)
    target_coords = (latitude, longitude)

    azimuth = calculate_initial_compass_bearing(ref_coords, target_coords)
    distance = geodesic(ref_coords, target_coords).meters
    relative_altitude = altitude - reference_alt

    X_NED = distance * math.cos(math.radians(azimuth))
    Y_NED = distance * math.sin(math.radians(azimuth))
    Z_NED = -relative_altitude

    return [X_NED, Y_NED, Z_NED]

def distance_to_ned(displacement, distance, ned_n, ned_e, heading):
    """
    Przekształca lokalne współrzędne obiektu względem pozycji pojazdu na układ NED.

        :param displacement: Odchylenie boczne od pojazdu wzdłuż osi X kamery (m)
        :param distance: Rzeczywista odległość obiektu od kamery (m)
        :param ned_n: Aktualna pozycja pojazdu w układzie NED (m)
        :param ned_e: Aktualna pozycja pojazdu w układzie NED (m)
        :param heading: heading pojazdu w radianach (yaw)
        :return: Nowe współrzędne obiektu w układzie NED (ned_n, ned_e)
    """
    # Oblicz kąt beta
    beta = math.asin(displacement / distance)  # Kąt w radianach

    # Oblicz poprawne surge
    surge = distance * math.cos(beta)

    # Oblicz przesunięcie
    new_n = surge * math.cos(heading) - displacement * math.sin(heading) + ned_n
    new_e = surge * math.sin(heading) + displacement * math.cos(heading) + ned_e
    return [new_n, new_e]

def get_real_distance(sway, surge):
    return math.sqrt(sway**2 + surge**2)

def distance_to_ned2(sway, surge, ned_n, ned_e, heading):
    """
    Przekształca lokalne współrzędne obiektu względem pozycji pojazdu na układ NED.

        :param sway: Odchylenie boczne od pojazdu wzdłuż osi X kamery (m)
        :param surge: Odległość obiektu od pojazdu wzdłuż osi Z kamery (m)
        :param ned_n: Aktualna pozycja pojazdu w układzie NED (m)
        :param ned_e: Aktualna pozycja pojazdu w układzie NED (m)
        :param heading: Kąt orientacji pojazdu w radianach
        :return: Nowe współrzędne obiektu w układzie NED (ned_n, ned_e)
    """
    new_n = surge * math.cos(heading) - sway * math.sin(heading) + ned_n
    new_e = surge * math.sin(heading) + sway * math.cos(heading) + ned_e
    return [new_n, new_e]

def distance_between_points(point1 : Tuple[float, float], point2: Tuple[float, float]) -> float:
    p1_X, p1_Y = point1
    p2_X, p2_Y = point2
    return math.sqrt((p1_X - p2_X) ** 2 + (p1_Y - p2_Y) ** 2)

def distance_to_line(point1: Tuple[float, float], point2: Tuple[float, float], obstacle: Tuple[float, float]) -> float:
    """
    Oblicza odległość punktu (obstacle) od odcinka między point1 a point2.
    """
    x1, y1 = point1
    x2, y2 = point2
    x0, y0 = obstacle

    # Wektor od point1 do point2
    dx = x2 - x1
    dy = y2 - y1

    # Wektor od point1 do obstacle
    dx_obs = x0 - x1
    dy_obs = y0 - y1

    # Iloczyn skalarny wektorów (dx, dy) i (dx_obs, dy_obs)
    dot_product = dx * dx_obs + dy * dy_obs

    # Długość odcinka point1-point2 do kwadratu
    segment_length_squared = dx * dx + dy * dy

    if segment_length_squared == 0:
        # Odcinek ma zerową długość (point1 == point2)
        return None

    # Parametr t określający pozycję rzutu punktu obstacle na linię
    t = dot_product / segment_length_squared

    if t < 0:
        # Rzut znajduje się przed point1
        closest_point = point1
        return None
    elif t > 1:
        # Rzut znajduje się za point2
        closest_point = point2
        return None
    else:
        # Rzut znajduje się na odcinku
        closest_point = (x1 + t * dx, y1 + t * dy)

    # Oblicz odległość między obstacle a najbliższym punktem na odcinku
    distance = math.sqrt((x0 - closest_point[0]) ** 2 + (y0 - closest_point[1]) ** 2)
    return distance

def local_distance(position1_n, position1_e, position2_n, position2_e):
    return math.sqrt((position1_n - position2_n) ** 2 + (position1_e - position2_e) ** 2)


########## ARTIFICIAL POTENTIAL FIELD START ###############
class ArtificialPotentialField:
    def __init__(self, attractive_power: float = 2.0, repulsive_distance: float = 2.0):
        self.attractive_power = attractive_power
        self.repulsive_distance = repulsive_distance

    def get_distance(self, ned1: NEDPosition, ned2: NEDPosition) -> float:
        return math.sqrt((ned2.n - ned1.n) ** 2 + (ned2.e - ned1.e) ** 2)

    def is_near_destination(self, destination: NEDPosition, current_position: NEDPosition, proximity_threshold: float) -> bool:
        distance = self.get_distance(destination, current_position)
        return distance <= proximity_threshold

    def get_vector(self, position: NEDPosition, target: NEDPosition) -> NEDPosition:
        return NEDPosition(
            n=target.n - position.n,
            e=target.e - position.e,
            d=target.d - position.d
        )

    def get_magnitude(self, vector: NEDPosition) -> float:
        return math.sqrt(vector.n ** 2 + vector.e ** 2)

    def get_unit_vector(self, vector: NEDPosition, magnitude: float) -> NEDPosition:
        if magnitude != 0:
            return NEDPosition(n=vector.n / magnitude, e=vector.e / magnitude, d=0.0)
        return NEDPosition(0.0, 0.0, 0.0)

    def get_repulsive_force(self, position: NEDPosition, obstacle: NEDPosition) -> NEDPosition:
        vector = self.get_vector(position, obstacle)
        magnitude = self.get_magnitude(vector)
        unit_vector = self.get_unit_vector(vector, magnitude)

        if magnitude == 0:
            return NEDPosition(0.0, 0.0, 0.0)

        force_strength = (self.repulsive_distance / magnitude)
        return NEDPosition(
            n=force_strength * unit_vector.n,
            e=force_strength * unit_vector.e,
            d=0.0
        )

    def get_attractive_force(self, position: NEDPosition, destination: NEDPosition) -> NEDPosition:
        vector = self.get_vector(position, destination)
        magnitude = self.get_magnitude(vector)
        unit_vector = self.get_unit_vector(vector, magnitude)

        return NEDPosition(
            n=self.attractive_power * unit_vector.n,
            e=self.attractive_power * unit_vector.e,
            d=0.0
        )

    def get_total_force(self, local_position: NEDPosition, global_destination: NEDPosition, obstacles: List[NEDPosition]) -> NEDPosition:
        repulsive_force = NEDPosition(0.0, 0.0, 0.0)

        for obstacle in obstacles:
            force = self.get_repulsive_force(local_position, obstacle)
            repulsive_force = NEDPosition(
                n=repulsive_force.n - force.n,
                e=repulsive_force.e - force.e,
                d=0.0
            )

        attractive_force = self.get_attractive_force(local_position, global_destination)

        total_force = NEDPosition(
            n=repulsive_force.n + attractive_force.n,
            e=repulsive_force.e + attractive_force.e,
            d=0.0
        )

        print(f"Repulsive Force: {repulsive_force}")
        print(f"Attractive Force: {attractive_force}")
        print(f"Total Force: {total_force}")

        return total_force

    def compute_temporary_target(self, vehicle_position: NEDPosition, destination: NEDPosition, detections: List[NEDPosition], proximity_threshold: float) -> NEDPosition:
        if self.is_near_destination(destination, vehicle_position, proximity_threshold):
            print("Destination reached!")
            return destination

        total_force = self.get_total_force(vehicle_position, destination, detections)
        temporary_target = NEDPosition(
            n=vehicle_position.n + total_force.n,
            e=vehicle_position.e + total_force.e,
            d=0.0
        )

        print(f"Temporary Target: {temporary_target}")
        return temporary_target
########## ARTIFICIAL POTENTIAL FIELD STOP ###############

########## BASE NODE START ###############
class BaseNode(Node):
    def __init__(self, node_name: str):
        super().__init__(node_name)
        # QoS Config
        self.qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=10
        )

    def create_qos_subscription(self, msg_type, topic_name, callback):
        # Create QoS Subscription
        return self.create_subscription(
            msg_type,
            topic_name,
            callback,
            self.qos_profile
        )

    def create_qos_publisher(self, msg_type, topic_name):
        # Create QoS Publisher
        return self.create_publisher(
            msg_type,
            topic_name,
            self.qos_profile
        )
########## BASE NODE STOP ###############