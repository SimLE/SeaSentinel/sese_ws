import numpy as np
from sklearn.cluster import DBSCAN

class AreaPredictor:
    OFFSET = 0.5
    def __init__(self, obstacles):
        self.obstacles = obstacles
        self.new_obstacle_2 = False
        self.new_obstacle_4 = False
        self.task_2_area = None
        self.task_4_area = None
        self.task_6_area = None


        self.task_4_gate = None

    @staticmethod
    def _add_offset(area, offset):
        """
        Expands or contracts the area by a given offset.
        Positive offset increases the area, negative offset decreases it.
        """
        if area is None:
            return None
        
        return {
            "x_min": area["x_min"] - offset,
            "x_max": area["x_max"] + offset,
            "y_min": area["y_min"] - offset,
            "y_max": area["y_max"] + offset,
        }


    @staticmethod
    def _merge_area(area1, area2):
        if area1 is None:
            return area2
        if area2 is None:
            return area1
        return {
            "x_min": min(area1["x_min"], area2["x_min"]),
            "x_max": max(area1["x_max"], area2["x_max"]),
            "y_min": min(area1["y_min"], area2["y_min"]),
            "y_max": max(area1["y_max"], area2["y_max"]),
        }

    def update_obstacles(self, obstacles):
        self.obstacles = obstacles
        self.new_obstacle_2 = True
        self.new_obstacle_4 = True

    def find_task_2_area(self):
        if self.new_obstacle_2 or self.task_2_area is None:
            self.new_obstacle_2 = False
            return self._add_offset(self._find_task_2_area(), AreaPredictor.OFFSET)

        return self._add_offset(self.task_2_area, AreaPredictor.OFFSET)

    def find_task_4_area(self):
        if self.new_obstacle_4 or self.task_4_area is None:
            self.new_obstacle_4 = False

            self.new_obstacle_2 = False
            self._find_task_2_area() # Brama w tasku 4 jest znajdowana podczas przeszukiwania taska 2 (self.task_4_gate)
            return self._add_offset(self._find_task_4_area(), AreaPredictor.OFFSET)

        return self._add_offset(self.task_4_area, AreaPredictor.OFFSET)
    
    def find_task_4_gate(self):
        if self.new_obstacle_4 or self.task_4_area is None:
            self.new_obstacle_4 = False

            self.new_obstacle_2 = False
            self._find_task_2_area() # Brama w tasku 4 jest znajdowana podczas przeszukiwania taska 2 (self.task_4_gate)
            return self._add_offset(self.task_4_gate, AreaPredictor.OFFSET)

        return self._add_offset(self.task_4_gate, AreaPredictor.OFFSET)

    def find_task_6_area(self):
        if self.new_obstacle_4 or self.task_4_area is None:
            self.new_obstacle_4 = False

            self.new_obstacle_2 = False
            self._find_task_2_area() # Brama w tasku 4 jest znajdowana podczas przeszukiwania taska 2 (self.task_4_gate)
            return self._add_offset(self._find_task_4_area(), AreaPredictor.OFFSET)

        return self._add_offset(self.task_6_area, AreaPredictor.OFFSET)

    def _find_task_2_area(self):
        """
        Znajduje granice obszaru Task 2 używając algorytmu DBSCAN na czerwonych i zielonych bojach
        :return: dict zawierający min/max wartości dla x i y największego klastra
        """

        task_2_obstacles = [obs.ned for obs in self.obstacles if "red_buoy" in obs.id or "green_buoy" in obs.id]
        
        if not task_2_obstacles:
            return None  # Brak przeszkód dla Task 2
        
        coords = np.array([[pos.n, pos.e] for pos in task_2_obstacles])
        clustering = DBSCAN(eps=8, min_samples=5).fit(coords)
        
        labels, counts = np.unique(clustering.labels_, return_counts=True)
        sorted_clusters = sorted(zip(labels, counts), key=lambda x: x[1], reverse=True)

        if not sorted_clusters or sorted_clusters[0][0] == -1:
            return None  # No valid cluster found

        largest_cluster = sorted_clusters[0][0]
        cluster_points = coords[np.where(clustering.labels_ == largest_cluster)]
        
        self.task_2_area = {
            "x_min": np.min(cluster_points[:, 0]),
            "x_max": np.max(cluster_points[:, 0]),
            "y_min": np.min(cluster_points[:, 1]),
            "y_max": np.max(cluster_points[:, 1])
        }
        
        if len(sorted_clusters) > 1:
            second_cluster = sorted_clusters[1][0]
            cluster_points = coords[np.where(clustering.labels_ == second_cluster)]
            self.task_4_gate = {
                "x_min": np.min(cluster_points[:, 0]),
                "x_max": np.max(cluster_points[:, 0]),
                "y_min": np.min(cluster_points[:, 1]),
                "y_max": np.max(cluster_points[:, 1])
            }
        
        return self.task_2_area

    def _find_task_4_area(self):
        """
        Znajduje granice obszaru Task 4 na podstawie współrzędnych przeszkód
        :return: dict zawierający min/max wartości dla x i y
        """
        task_4_obstacles = [obs.ned for obs in self.obstacles if "blue_buoy" in obs.id or "black_buoy" in obs.id]
        
        if not task_4_obstacles:
            return None  # Brak przeszkód dla Task 4
        
        coords = np.array([[pos.n, pos.e] for pos in task_4_obstacles])
        clustering = DBSCAN(eps=10, min_samples=3).fit(coords)
        
        labels, counts = np.unique(clustering.labels_, return_counts=True)
        sorted_clusters = sorted(zip(labels, counts), key=lambda x: x[1], reverse=True)
        
        if not sorted_clusters or sorted_clusters[0][0] == -1:
            return None  # No valid cluster found

        largest_cluster = sorted_clusters[0][0]
        cluster_points = coords[np.where(clustering.labels_ == largest_cluster)]
        
        task_4_black_buoys = {
            "x_min": np.min(cluster_points[:, 0]),
            "x_max": np.max(cluster_points[:, 0]),
            "y_min": np.min(cluster_points[:, 1]),
            "y_max": np.max(cluster_points[:, 1])
        }
        self.task_4_area = self._merge_area(task_4_black_buoys, self.task_4_gate)

        if len(sorted_clusters) > 1:
            second_cluster = sorted_clusters[1][0]
            cluster_points = coords[np.where(clustering.labels_ == second_cluster)]
            self.task_6_area = {
                "x_min": np.min(cluster_points[:, 0]),
                "x_max": np.max(cluster_points[:, 0]),
                "y_min": np.min(cluster_points[:, 1]),
                "y_max": np.max(cluster_points[:, 1])
            }

        return self.task_4_area