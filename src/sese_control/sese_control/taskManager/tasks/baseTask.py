from abc import ABC, abstractmethod
from sese_control.utils import BaseNode

from std_msgs.msg import Bool, String


class BaseTask(BaseNode, ABC):
    def __init__(self, name):
        super().__init__(name)
        self.name = name

        self.create_qos_subscription(String, 'taskManager/next_task', self.task_callback)
        self.task_publisher = self.create_qos_publisher(String, 'taskManager/task_done')

        self.timer = self.create_timer(0.1, self.timer_callback)
        self.running = False

    def publish_end_of_task(self):
        self.running = False

        self.get_logger().info(f'{self.name} completed.')
        msg = String()
        msg.data = f"{self.name}_completed"
        self.task_publisher.publish(msg)
        
    def task_callback(self, msg):
        if not msg.data:
            return

        # self.get_logger().info(f"{msg.data}= {self.name}")
        if msg.data == self.name:
            self.get_logger().info(f'{self.name} is executing.')
            self.running = True
        else:
            self.running=False

    def timer_callback(self):
        if self.running:
            self.make_decision()

    @abstractmethod
    def make_decision(self):
        """Abstract method to be implemented by derived classes."""
        pass

    



        