import csv

import numpy as np
from ament_index_python.packages import get_package_share_directory
import rclpy
from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle, distance_between_points
from sese_control.areaPredictor.predictor import AreaPredictor

from sese_interfaces.msg import DetectionArray, DetectionMSG, OffboardControlPosition
from sese_control.pathPlanner.deafultPlanner import DeafultPlanner

from std_msgs.msg import Bool
from geometry_msgs.msg import Pose
from px4_msgs.msg import VehicleLocalPosition

from typing import Tuple
import math

Waypoint = Tuple[float, float]

class Task_R(BaseTask):
    def __init__(self):
        super().__init__('Task_R')

        # Inicjalizacja parametrów obrotu
        self.yaw_angle = 0.0   # Aktualny kąt yaw w radianach
        self.angle = 120          
        self.step = math.radians(self.angle) # Stopień obrotu na iterację 
        self.max_angle = math.radians(360)  # Maksymalny kąt (360 stopni)

        # Use ament_index_python to find the path to the CSV file in the share directory
        package_share_directory = get_package_share_directory('sese_control')
        self.csv_path = self.declare_parameter('csv_path', f'{package_share_directory}/config/instructions.csv').get_parameter_value().string_value


        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition, 'CombinedOffboardControl/goal_pose')
        self.gate_publisher = self.create_qos_publisher(Pose, 'Task0/end_gate') # topic, który przechowuje pozycję ASV, która będzie potrzebna do obliczenia pozycji czarnej bramy

        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback)
        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)

        self.obstacles = []
        self.old_obstacles = []
        self.new_buoys = []

        self.path = []
        # self.load_waypoints(self.csv_path)

        self.vehicle_local_position = None
        self.center_position = None # środek spirali
        self.Task4_position = None # pozycja przed bramą

        self.waiting_for_offboardcontroller = False
        self.counter = 0 # ile punktów spirali osiągnięto
        self.buyos_num : int = 0
        self.is_first_iteration = True

        self.rotate = False # rotate 360 degrees
        self.follow = True # follow waypoints
        # self.explore = False # go around the discovered buoys

        self.spiral = False
        self.return_to_center = False
        self.Task3_checked = False
        self.Task4_found = False

        self.area_predictor = AreaPredictor(self.obstacles)

        self.default_planner = DeafultPlanner()

        # self.safe_position = None # position from which ASV starts exploring
        # self.waypoint_calc = False # waypoint for exploration calculated

    def vehicle_local_position_callback(self, msg):
        if not msg:
            return

        self.vehicle_local_position = HNEDPosition(
            heading=msg.heading,
            ned=NEDPosition(n=msg.x, e=msg.y, d=0.0)
        )

    def _goal_reached_callback(self, msg):
        if not msg.data or not self.running:
            return
        self.waiting_for_offboardcontroller = False
            
    def _get_detections_callback(self, msg):
        # nie odpala callbacku kiedy running False!!!
        if self.vehicle_local_position is None or not self.running:
            return


        if len(self.obstacles) == 0:
            self.obstacles = msg.detections
            self.old_obstacles = msg.detections
            return

        #TODO: naprawić stackowanie detekcji
        self.obstacles = []

        for detection in msg.detections:
            object_class = detection.object_class
            obs = Obstacle(
                        ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                        id=object_class
                    )
            self.obstacles.append(obs)
            # sprawdź czy są nowe wykrycia
            if obs not in self.old_obstacles:    
                self.new_buoys.append(obs)
                self.buyos_num += 1

        self.get_logger().info(f"Buoys found: {len(self.new_buoys)}")
        self.old_obstacles = self.obstacles
        self._is_new_obstacle = True

    def find_new_buoys(self):
        '''
        Policz punkt docelowy na podstawie nowych bojek
        '''
        if len(self.new_buoys) == 0:
            self.get_logger().info("Nic nie mamy :((((")
            return 0,0

        avg_n = sum(obs.ned.n for obs in self.new_buoys) / len(self.new_buoys)
        avg_e = sum(obs.ned.e for obs in self.new_buoys) / len(self.new_buoys)
    
        return avg_n, avg_e
    
    def find_banner(self):
        '''
        Znajdź banner z Tasku 3
        '''
        for obs in self.new_buoys:
            # TODO: dodać kolejne klasy z Tasku 3
            if obs.id in ["banner"]:
                    obs_pos = (obs.ned.n, obs.ned.e)
                    return obs_pos
        return []

    def clear_new_buyos(self):
        self.new_buoys = []
        # self.buyos_found = 0

    def _is_full_rotation_completed(self):
        return self.yaw_angle >= self.max_angle

    def _publish_rotation(self):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.DIFFERENTIAL_MODE

        msg.x = 0.0
        msg.y = 0.0
        msg.z = 0.0
        msg.heading = self.step
 
        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)
        self.yaw_angle += self.step

    def _publish_next_point(self):
        if len(self.path) > 0:

            self._publish_differential_position(self.path[0][0], self.path[0][1])
            self.get_logger().info(f'Publish this point: {self.path[0]}')
            self.path.pop(0)

    def _publish_next_global_point(self): 
        if len(self.path) > 0:
            waypoint = (self.path[0][0], self.path[0][1])
            #heading jest pozyskiwany z metody generate_spiral_waypoints
            try:
                heading = self.path[0][2]
            # brak headingu oznacza, że to nie spirala
            except:
                heading = self._update_heading(waypoint)
            self._publish_global_position(waypoint, heading)
            self.get_logger().info(f'Publish this point: {self.path[0]}')
            self.path.pop(0)

    def _publish_differential_position(self, surge, sway):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.DIFFERENTIAL_MODE

        msg.x = surge  # surge
        msg.y = sway   # sway
        msg.z = 0.0    # heave
        msg.heading = self.vehicle_local_position.heading   # radiany

        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)

    def _publish_global_position(self, target_pos: Waypoint, heading: float):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_pos[0]   # N
        msg.y = target_pos[1]   # E
        msg.z = 0.0             # D
        msg.heading = heading   # radiany

        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)

    def _update_heading(self, target_position: Waypoint) -> float:
        # Oblicz heading w zakresie <-pi, pi>
        
        delta_n = target_position[0] - self.vehicle_local_position.ned.n
        delta_e = target_position[1] - self.vehicle_local_position.ned.e

        return math.atan2(delta_e, delta_n)

    def load_waypoints(self, csv_path):

        with open(csv_path, mode='r') as file:
            reader = csv.reader(file)
            next(reader)  # Skip header
            for row in reader:
                try:
                    Surge, Sway = float(row[0]), float(row[1])
                    self.path.append([Surge, Sway])
                    # print(buoys)
                except ValueError:
                    pass
                    # self.get_logger().warning(f"Invalid row in CSV file: {row}")
        self.get_logger().info(f'Our path: {self.path}')
    
    # strange fix which somehow works
    @staticmethod
    def generate_spiral_waypoints(center_n, center_e, start_radius, end_radius, step_size=3, num_turns=3, num_points=100):
        """
        Generuje waypointy w układzie NED dla ruchu spiralnego.

        :param center_n: Środek spirali (współrzędna północna)
        :param center_e: Środek spirali (współrzędna wschodnia)
        :param start_radius: Początkowy promień spirali
        :param end_radius: Końcowy promień spirali
        :param step_size: Odległość między kolejnymi waypointami
        :param num_turns: Liczba pełnych obrotów spirali
        :param num_points: Liczba punktów generowanych w spirali
        :return: Lista waypointów w formacie [(N, E, heading)]
        """
        waypoints = []
        theta = np.linspace(0, num_turns * 2 * np.pi, num_points)  # Kąt obrotu
        radii = np.linspace(start_radius, end_radius, num_points)  # Promień rosnący liniowo

        prev_n, prev_e = center_n + radii[0] * np.cos(theta[0]), center_e + radii[0] * np.sin(theta[0])
        
        for i in range(1, num_points):
            n = center_n + radii[i] * np.cos(theta[i])
            e = center_e + radii[i] * np.sin(theta[i])

            # Heading zgodny z kierunkiem ruchu (względem osi NED)
            heading = np.arctan2(e - prev_e, n - prev_n)  # Kąt w radianach
            
            # Dodaj waypoint tylko jeśli dystans od poprzedniego >= step_size
            if np.linalg.norm([n - prev_n, e - prev_e]) >= step_size:
                waypoints.append((n, e, heading))  # Nie konwertuję headingu na stopnie, bo i tak są radiany w px4
                prev_n, prev_e = n, e  # Aktualizacja ostatniej pozycji

        return waypoints

    def compute_left_point(asv_pos:tuple, obs:tuple, d_C:float):
        """
        Compute the NED coordinates of point C, which is to the left of obs relative to the heading from asv_pos to obs.

        :param asv_pos: (N, E)
        :param obs: (N, E)
        :param d_C: Distance from obs to C (leftward displacement)
        :return: (N_C, E_C)
        """

        N_asv, E_asv = asv_pos
        N_obs, E_obs = obs
        # Compute heading (yaw) from asv_pos to obs
        theta_AB = math.atan2(E_obs - E_asv, N_obs - N_asv)  # Angle from asv_pos to obs in radians

        # Compute C's position (left is -90 degrees from heading)
        theta_C = theta_AB - math.radians(90)

        N_C = N_obs + d_C * math.cos(theta_C)
        E_C = E_obs + d_C * math.sin(theta_C)

        return N_C, E_C

    def make_decision(self):
        if self.vehicle_local_position is None:
            return
        
        if self.is_first_iteration and self.vehicle_local_position is not None:
            self.get_logger().info('self.is_first_iteration')
            self.is_first_iteration = False
            self._is_new_obstacle = False
            self.area_predictor.update_obstacles(self.obstacles)
            self.path = [(5.0,0.0),(0.0,-5.0)] # punkty docelowe do wypłynięcia z tasku 2
            return

        if self.waiting_for_offboardcontroller:
            return
        
        # aktualizuj area predictor
        if self._is_new_obstacle:
            self._is_new_obstacle = False
            self.area_predictor.update_obstacles(self.obstacles)
            return
        
        if self._is_full_rotation_completed():
            self.get_logger().info('Full rotation completed')
            self.yaw_angle = 0
            self.rotate = False
            return

        if self.rotate:
            self.get_logger().info('self.rotate')
            self._publish_rotation()
            return

        # wypłyń z tasku 2   
        if self.follow:
            if len(self.path) == 0:
                self.get_logger().info("Get ready to spin!")
                self.follow = False
                self.spiral = True
                asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                self.get_logger().info(f'{asv_pos}')
                self.center_position = asv_pos # środek spirali
                self.path = Task_R.generate_spiral_waypoints(center_n=asv_pos[0], center_e=asv_pos[1], start_radius=2, end_radius=10, step_size=3, num_turns=3, num_points=20)
            else:
                self.get_logger().info("Blindly following your orders!")
                self._publish_next_point()
            return

        # pływamy wzdłuż spirali
        if self.spiral:
            if len(self.path) == 0:
                self.get_logger().info("Wake up babe, new detections just dropped")
                self.spiral = False
            else:
                # kręć się
                if self.counter == 3:
                    self.rotate = True
                    self.counter = 0
                    return
                self._publish_next_global_point()
                self.counter += 1
                self.get_logger().info(f"Punkt spirali: {self.counter}")
            return

        if self.buyos_num >=5:
            if len(self.path) > 0:
                if len(self.path) == 1 and not self.Task3_checked:
                    self.rotate = True # chcę żeby po dotarciu do tasku 3 asv się obróciło
                    self.Task3_checked = True
                    self.return_to_center = True
                    self.get_logger().info("Zaczynam się kręcić")
                self._publish_next_global_point()
                return

            # sprawdź task 3
            if not self.Task3_checked:        
                obs_pos = self.find_banner()
                if len(obs_pos) == 0:
                    self.Task3_checked = True
                    self.get_logger().info("Task 3 wyparował a wraz z nim nasze nadziei")
                    return
                
                # task_h = self._update_heading(obs_pos) - math.radians(30)
                # zrób punkt po lewej od bannera
                asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                new_dest = self.compute_left_point(asv_pos, obs_pos, 10)
                
                self.path = self.default_planner.findPath(asv_pos, new_dest, self.obstacles)
                self.get_logger().info("Płynę do Tasku 3, pokręcę się i wrócę")
                return
            
            # wróć do środka spirali
            if self.return_to_center:
                self.return_to_center = False
                asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                self.path = self.default_planner.findPath(asv_pos,self.center_position, self.obstacles)
                self.get_logger().info("Wracam do środka spirali")
                return

            # płyń do bramy Tasku 4 o ile to jest możliwe
            if self.Task3_checked:
                gate = self.area_predictor.find_task_4_gate()
                # jeżeli bramy nie ma, to płyń w kierunku nowych wykryć
                if gate is None:
                    self.get_logger().info("Jakie życie taki rap -_-")
                    n,e = self.find_new_buoys()
                    asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                    self.path = self.default_planner.findPath(asv_pos, (n,e), self.obstacles)
                    #TODO: logika na eksplorację z poprzedniej wersji taska_R
                    return
                else:
                    self.get_logger().info("Task 4 is coming, we are so back!")
                    #TODO: obliczyć inny punkt obok bramy
                    center_x = (gate["x_min"] + gate["x_max"]) / 2
                    center_y = (gate["y_min"] + gate["y_max"]) / 2

                    center = (center_x, center_y)
                    self.Task4_position = center
                    asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                    self.path = self.default_planner.findPath(asv_pos, center, self.obstacles)
                    self.Task4_found = True
                    return

                # logika na eksplorację
                #else

                # if not self.waypoint_calc and self.explore:
                #     asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)

                #     n,e = self.find_new_buoys()
                #     self.clear_new_buyos()
                #     self.counter += 1
                #     self.path = self.default_planner.findPath(asv_pos, (n,e), self.obstacles)
                #     self.waypoint_calc = True
                #     self.follow = True
                #     self.get_logger().info("Path calculated")
                #     return
        
        # popłyń do tasku 4 i zakończ obecny Task
        if self.Task4_found:
            asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
            if distance_between_points(asv_pos, self.Task4_position) < 0.2:
                self.publish_end_of_task()       


def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_R()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass