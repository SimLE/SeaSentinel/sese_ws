import math
import rclpy
from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle, distance_between_points
from sese_control.pathPlanner import DeafultPlanner
from sese_interfaces.msg import OffboardControlPosition, DetectionArray, DetectionMSG
from px4_msgs.msg import VehicleLocalPosition
from std_msgs.msg import Bool

from typing import List, Tuple

Waypoint = Tuple[float, float]

class Timer():
    def __init__(self):
        self.i = 50
    def reset(self):
        self.i = 0
    def tick(self):
        if self.i < 51:
            self.i += 1
    def is_5_sek(self):
        return self.i > 50 # tick co 100ms

class Task_5(BaseTask):
    ORANGE_BOAT_LABEL = "black_triangle"
    BLACK_BOAT_LABEL = "black_plus"
    BOATS_LABELS = [ORANGE_BOAT_LABEL, BLACK_BOAT_LABEL]
    def __init__(self):
        super().__init__('Task_5')

        self.pee_pee_publisher = self.create_qos_publisher(Bool, 'som/waterCannon/fire')
        self.poo_poo_publisher = self.create_qos_publisher(Bool, 'som/ballCannon/fire')

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition, 'CombinedOffboardControl/goal_pose')
        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.waiting_for_offboardcontroller: bool = False

        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self._vehicle_local_position_callback)
        self.robot_position: HNEDPosition = None 

        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)
        self.obstacles: List[Obstacle] = []
        self._is_new_obstacle = False

        self.path_planner = DeafultPlanner()
        self.path: List[Waypoint] = []
        self.target_boats: List[Obstacle] = []
        self.actual_target :Obstacle = None

        self.is_first_iteration = True

        self.timer = Timer()

    def _get_detections_callback(self, msg: DetectionArray):
        self.obstacles = []
        self._is_new_obstacle = True
        for detection in msg.detections:
            obstacle = Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=detection.ned_d),
                    id=detection.object_class
                )
            self.obstacles.append(obstacle)

    def _goal_reached_callback(self, msg: Bool):
        if msg.data:
            self.waiting_for_offboardcontroller = False

    def _vehicle_local_position_callback(self, msg: VehicleLocalPosition):
        self.robot_position = HNEDPosition(heading=msg.heading,ned=NEDPosition(n=msg.x,e=msg.y,d=msg.z))

    def _publish_offboard_position(self, target_pos: Waypoint, heading: float):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_pos[0]   # N
        msg.y = target_pos[1]   # E
        msg.z = 0.0             # D
        msg.heading = heading   # radiany

        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)

    def _update_heading(self, target_position: Waypoint) -> float:
        # Oblicz heading w zakresie <-pi, pi>
        start_position: Waypoint = (self.robot_position.ned.n, self.robot_position.ned.e)

        delta_n = target_position[0] - start_position[0]
        delta_e = target_position[1] - start_position[1]

        return math.atan2(delta_e, delta_n)

    def _publish_next_point(self):
        if len(self.path) > 0:
            self._publish_offboard_position(self.path[0], self._update_heading(self.path[0]))
            self.path.pop(0)

    def _update_path(self):
        if self.actual_target is None:
            return []

        start_position: Waypoint = (self.robot_position.ned.n, self.robot_position.ned.e)
        target_position: Waypoint = (self.actual_target.ned.n, self.actual_target.ned.e)
        
        return self.path_planner.findPath(start_position, target_position, self.obstacles)

    def _find_targets(self):
        for obstacle in self.obstacles:
            if obstacle.id in Task_5.BOATS_LABELS:
                self.target_boats.append(obstacle)

    def _new_target(self):
        robot_position = (self.robot_position.ned.e, self.robot_position.ned.n)
        self.actual_target = min(self.target_boats, key=lambda boat: distance_between_points(robot_position, (boat.ned.e, boat.ned.n)))
        self.target_boats.remove(self.actual_target)

    def reset_timer(self):
        self.timer.reset()

    def wait(self):
        self.timer.tick()
        return not self.timer.is_5_sek()

    def make_decision(self):
        if self.robot_position is None:
            self.get_logger().info(f'\n{self.robot_position=}\n{len(self.obstacles)=}\n')
            return

        if self.is_first_iteration:
            self.get_logger().info(f'self.is_first_iteration:')
            self._is_new_obstacle = False
            self.is_first_iteration = False
            self._find_targets()
            return
        
        if self.wait():
            return

        if self._is_new_obstacle:
            self.get_logger().info(f'self._is_new_obstacle')
            self._is_new_obstacle = False

            self.path = self._update_path()
            self._publish_next_point()
            return

        if self.waiting_for_offboardcontroller:
            return

        if len(self.path) > 0:
            self._publish_next_point()

        if len(self.path) == 0 and len(self.target_boats) == 0 and self.actual_target is None:
            self.publish_end_of_task()

        if len(self.path) == 0 and len(self.target_boats) > 0 and self.actual_target is None:
            self._new_target()
            self.path = self._update_path()
            self._publish_next_point()
            return

        if len(self.path) == 0 and self.actual_target is not None: # dopłynął do jakiejś łodzi
            if self.actual_target.id == Task_5.ORANGE_BOAT_LABEL:
                # ASV robi psi psi      https://www.youtube.com/watch?v=aLUOkV1OmVk
                
                msg = Bool()
                msg.data = True
                self.pee_pee_publisher.publish(msg)
                self.reset_timer()
                self.actual_target = None
                self.get_logger().info(f'pee pee mode activated!👉💦')

            elif self.actual_target.id == Task_5.BLACK_BOAT_LABEL:
                # ASV robi poo poo      https://www.youtube.com/watch?v=GaqwebUFme0

                msg = Bool()
                msg.data = True
                self.poo_poo_publisher.publish(msg)
                self.reset_timer()
                self.actual_target = None
                self.get_logger().info(f'🚢💩 Special delivery!')

            else: # jakis błąd -> Mówi się trudno i płynie się dalej   ¯\_(ツ)_/¯
                self.get_logger().info(f'Nie powinieneś mnie zobaczyć ->') # Marcel, mówi się trudno wywala colcona xD
                self.actual_target = None
            
            return



def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_5()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass