import numpy as np
import rclpy
from sese_control.areaPredictor.predictor import AreaPredictor
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle, local_distance, distance_between_points
from sese_control.pathPlanner.convexPlanner import ConvexPlanner
from sese_control.pathPlanner.deafultPlanner import DeafultPlanner

from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.pathPlanner.basePlanner import PlannerInterface, Waypoint

from std_msgs.msg import Bool, Int32
import math
from px4_msgs.msg import VehicleLocalPosition
from sese_interfaces.msg import DetectionArray, DetectionMSG, OffboardControlPosition

class Task_4(BaseTask):
    # goal and obstacles
    BLUE_BUOY_LABEL = "blue_buoy"
    BLACK_BUOY_LABEL = "black_buoy"
    # gate
    GREEN_BUOY_LABEL = "green_buoy"
    RED_BUOY_LABEL = "red_buoy"
    # boats
    ORANGE_BOAT_LABEL = "black_triangle"
    BLACK_BOAT_LABEL = "black_plus"

    def __init__(self):
        super().__init__('Task_4')

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition, 'CombinedOffboardControl/goal_pose')
        self.buoy_counter_publisher = self.create_qos_publisher(Int32, 'Task_4/buoy_counter')
        
        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback)
        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)

        self.vehicle_local_position : HNEDPosition = None

        self.start_position = None # start position of task 4
        self.curr_waypoint = None # current waypoint
        self.goal_point = None # end position near the start position
        
        self.begin = False # start circling from the start position
        self.last_point = False # if it's the last waypoint

        self.updated_detections = False # if updated detections came
        self.doit = False # just plan task please!

        self.obstacle_list : Obstacle = []
        self.red_stack : Obstacle = [] # list containing only red buoys
        self.green_stack : Obstacle = [] # list containing only green buoys
        self.start_gate : Obstacle = []
        self.path = [] # task path

        self.planner = ConvexPlanner() # a planner planning to plan our plans
        self.default_planner = DeafultPlanner()
        self.area_predictor = AreaPredictor(self.obstacle_list)

        self.waiting_for_offboardcontroller = False
        self.is_first_iteration = True
        self.start_round = False
        self.waiting = False
        self.i = 0 # counter

    def _count_black_buoys(self):
        return sum(obstacle.id == self.BLACK_BUOY_LABEL for obstacle in self.obstacle_list)

    def publish_black_buoys(self):
        msg = Int32()
        msg.data = self._count_black_buoys()
        self.buoy_counter_publisher.publish(msg)
        self.get_logger().info(f'Black buoys: {msg.data}')

    def vehicle_local_position_callback(self, msg):
        if not msg:
            return

        self.vehicle_local_position = HNEDPosition(
            heading=msg.heading,
            ned=NEDPosition(n=msg.x, e=msg.y, d=0.0)
        )

    def _get_detections_callback(self, msg):

        if not msg:
            return

        old_number = len(self.obstacle_list)
        
        self.obstacle_list = []

        for detection in msg.detections:
            object_class = detection.object_class

            self.obstacle_list.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))
            
        if old_number != len(self.obstacle_list):
            self._is_new_obstacle = True

    def _goal_reached_callback(self, msg):
        if not msg.data:
            return
    
        self.waiting_for_offboardcontroller = False

    def planTask(self):
        """
        Calculate path for start_position accounting for current position
        """
        asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
        
        task4_area = self.area_predictor.find_task_4_area()
        if task4_area is None:
            return []

        buoys_inside_gate = [
            obs for obs in self.obstacle_list 
            if task4_area["y_min"] <= obs.ned.e <= task4_area["y_max"] and 
            task4_area["x_min"] <= obs.ned.n <= task4_area["x_max"]
        ]
        self.path = []
        self.path = self.planner.findPath(asv_pos, self.goal_point, buoys_inside_gate)
        if self.path is None:
            self.path = []

    def _update_heading(self, target_position: Waypoint) -> float:
        # Calculate heading in the range <-pi, pi>
        
        delta_n = target_position[0] - self.vehicle_local_position.ned.n
        delta_e = target_position[1] - self.vehicle_local_position.ned.e

        return math.atan2(delta_e, delta_n)

    def _publish_offboard_position(self, target_pos: Waypoint, heading: float):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_pos[0]   # N
        msg.y = target_pos[1]   # E
        msg.z = 0.0             # D
        msg.heading = heading   # radiany

        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)

    def _publish_next_point(self):
        self.get_logger().info(f'Path: {self.path}')
        if len(self.path) > 0:

            self._publish_offboard_position(self.path[0], self._update_heading(self.path[0]))
            self.path.pop(0)

    def _find_start_position(self):

        self.get_logger().info(f'Red stack: {self.red_stack}')
        self.get_logger().info(f'Green stack: {self.green_stack}')

        self._find_buoys(self.red_stack)
        self.get_logger().info(f'Start gate first sort: {self.start_gate}')

        self._find_buoys(self.green_stack)
        self.get_logger().info(f'Start gate second sort: {self.start_gate}')

        l_corner = self.start_gate[1] # top left
        r_corner = self.start_gate[2] #bottom right

        mid_n = (l_corner.ned.n + r_corner.ned.n) / 2
        mid_e = (l_corner.ned.e + r_corner.ned.e) / 2

        return (mid_n, mid_e), (mid_n + 0.1, mid_e + 0.1)

    def _find_buoys(self, stack):
        """
        Find gate buoys of a specific color
        """
        temp = []
        for obstacle in stack:
            asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
            obs_pos = (obstacle.ned.n, obstacle.ned.e)

            d = local_distance(asv_pos[0], asv_pos[1], obs_pos[0], obs_pos[1])
            self.get_logger().info(f'Distance to buoy: {d}')

            if d > 10:
                continue

            heading_2_buoy = self._update_heading(obs_pos)
            asv_heading = self.vehicle_local_position.heading

            # check if buoy is within our visibile range
            self.get_logger().info(f'Heading to buoy: {heading_2_buoy}. Heading of ASV: {asv_heading}')
            # if heading_2_buoy > (asv_heading - math.pi/3) and heading_2_buoy < (asv_heading + math.pi/3):
            temp.append(obstacle)

        temp.sort(key= lambda obstacle: np.linalg.norm(np.array(asv_pos) - np.array((obstacle.ned.n, obstacle.ned.e))))

        self.get_logger().info(f'Found buoys of : {temp}')
        self.start_gate.extend(temp)

    def _find_start_position_2(self):
        gate = self.area_predictor.find_task_4_gate()
        if gate is None:
            return None, None
        center_x = (gate["x_min"] + gate["x_max"]) / 2
        center_y = (gate["y_min"] + gate["y_max"]) / 2

        center = (center_x, center_y)
        return (center_x, center_y), (center_x + 0.1, center_y + 0.1)

    def tick_tock(self):
        if self.i <= 10:
            self.i += 1
        else:
            self.start_round = True
            self.waiting = False

    def calculate_gate_path(self):
        self.start_position, self.goal_point = self._find_start_position_2()
        if self.start_position is None or self.goal_point is None:
            self.path =[]
            return
        asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)

        self.path = self.default_planner.findPath(asv_pos, self.start_position, self.obstacle_list)
        
        if self.path is None:
            self.path = []

    def make_decision(self):

        if self.vehicle_local_position is None:
            return 

        if self.is_first_iteration:
            self.is_first_iteration = False
            self._is_new_obstacle = False
            self.area_predictor.update_obstacles(self.obstacle_list)
            self.start_position, self.goal_point = self._find_start_position_2()
            return

        if self.start_position is None or self.goal_point is None: # Gate not found -> Skip task
            self.get_logger().info(f"Nie znaleziono bramy -> Skip task")
            self.publish_end_of_task()
            return
            
        if self._is_new_obstacle:
            self._is_new_obstacle = False

            self.area_predictor.update_obstacles(self.obstacle_list)

            if self.start_round:
                self.planTask()
                self._publish_next_point()
            
            elif self.waiting:
                return
                
            else: # szukamy bramy
                self.calculate_gate_path()
                self._publish_next_point()  
            return

        if self.waiting_for_offboardcontroller:
            return

        if self.start_round: # okrążamy bojki
            if len(self.path) > 0:
                self._publish_next_point()
                return
            else:
                asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                if distance_between_points(asv_pos, self.goal_point) < 0.2:
                    self.publish_black_buoys()
                    self.publish_end_of_task()
                return
                
        elif self.waiting: # czekamy w bramie

            self.tick_tock()
            # tick_tock przestawił się na start_round
            if self.start_round:
                self.planTask()
            return
        
        else: # szukamy bramy

            if len(self.path) > 0:
                self._publish_next_point()
                return
            
            else:
                asv_pos = (self.vehicle_local_position.ned.n, self.vehicle_local_position.ned.e)
                if distance_between_points(asv_pos, self.start_position) < 0.2:
                    self.waiting = True
                    return
                self.calculate_gate_path()
                self._publish_next_point()
                return

def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_4()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass




