import math

import rclpy
from std_msgs.msg import Bool
from px4_msgs.msg import VehicleLocalPosition, VehicleGlobalPosition

from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle, distance_between_points
from sese_interfaces.msg import DetectionArray, DetectionMSG, OffboardControlPosition

from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy


class Task_1(BaseTask):
    GREEN_BUOY_LABEL = "green_gate_buoy"
    RED_BUOY_LABEL = "red_gate_buoy"

    def __init__(self):
        super().__init__('Task_1')

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition,
                                                            'CombinedOffboardControl/goal_pose')
        self.mode_publisher = self.create_qos_publisher(Bool, 'CombinedOffboardControl/control_mode')

        self.green_buoy = []  # Lista współrzędnych (latitude, longitude)
        self.red_buoy = []  # Lista współrzędnych (latitude, longitude)


        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback)
        self.robot_position = None  # Pozycja robota (latitude, longitude)
        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)

        self.waiting_for_offboardcontroller = False
        self.entry_reached = False
        self.entry = None # entry to a fisrt gate
        self.updated_detections = False  # if updated detections came
        self.last_goal_point = False

        self.first_goal_point = False # a fix for goal callback 

    def vehicle_local_position_callback(self, msg):
        if not msg:
            return

        self.robot_position = HNEDPosition(
            heading=msg.heading,
            ned=NEDPosition(n=msg.x, e=msg.y, d=0.0)
        )

    def _goal_reached_callback(self, msg):
        if not msg.data:
            return

        if self.running:
            if self.last_goal_point:
                self.last_goal_point = False
                self.publish_end_of_task()  # zakończ taska
                return
            if self.first_goal_point:
                self.entry_reached = True
                self.get_logger().info(f"GOAL REACHED")
                self.waiting_for_offboardcontroller = False

    def _get_detections_callback(self, msg):
        if self.robot_position is None:
            return

        self._clear_bouy_detections()
        for detection in msg.detections:
            object_class = detection.object_class

            if object_class == self.RED_BUOY_LABEL:
                self.red_buoy.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))

            elif object_class == self.GREEN_BUOY_LABEL:
                self.green_buoy.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))

        if self.running:
            self.updated_detections = True

    def _clear_bouy_detections(self):
        self.green_buoy.clear()
        self.red_buoy.clear()

    def _calc_goal_point(self, red_buoy, green_buoy):
        target_n = (red_buoy.ned.n + green_buoy.ned.n) / 2
        target_e = (red_buoy.ned.e + green_buoy.ned.e) / 2
        return target_n, target_e

    def publish_new_global_position(self, target_n, target_e):
        self.waiting_for_offboardcontroller = True
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_n
        msg.y = target_e
        msg.z = 0.0
        msg.heading = self._update_heading((target_n, target_e))

        self._clear_bouy_detections()
        self.offboard_publisher.publish(msg)

    def _update_heading(self, target_position) -> float:
        # Oblicz heading w zakresie <-pi, pi>

        delta_n = target_position[0] - self.robot_position.ned.n
        delta_e = target_position[1] - self.robot_position.ned.e

        return math.atan2(delta_e, delta_n)

    def make_decision(self):
        if self.robot_position is None:
            return

        if self.waiting_for_offboardcontroller:
            return

        # podpłyń do wejścia (środek pierwszej bramy)
        if not self.entry_reached and self.updated_detections:
            asv_pos = (self.robot_position.ned.n, self.robot_position.ned.e)
            try:
                closest_red = min(self.red_buoy, key=lambda buoy: distance_between_points(asv_pos, (buoy.ned.n, buoy.ned.e)))
                closest_green = min(self.green_buoy, key=lambda buoy: distance_between_points(asv_pos, (buoy.ned.n, buoy.ned.e)))
                self.get_logger().info(f"ENTRANCE FOUND")
            except ValueError:
                self.get_logger().info(f"ENTRANCE NOT FOUND")
                return

            self.entry = self._calc_goal_point(closest_red, closest_green)
            self.publish_new_global_position(*self.entry)
            self.first_goal_point = True
            return

        # podpłyń do wyjścia (środek drugiej bramy)
        if self.entry_reached:
            self.get_logger().info(f"GOING TO SECOND GATE")
            try:
                furthest_red = max(self.red_buoy, key=lambda buoy: distance_between_points(self.entry, (buoy.ned.n, buoy.ned.e)))
                furthest_green = max(self.green_buoy, key=lambda buoy: distance_between_points(self.entry, (buoy.ned.n, buoy.ned.e)))
                self.get_logger().info(f'Red gate: {furthest_red.ned.n}, {furthest_red.ned.e}')
                self.get_logger().info(f'Green gate: {furthest_green.ned.n}, {furthest_green.ned.e}')
            except ValueError:
                return

            goal_point = self._calc_goal_point(furthest_red, furthest_green)
            self.publish_new_global_position(*goal_point)
            self.last_goal_point = True


def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_1()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
