from email import utils
import rclpy
from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.areaPredictor.predictor import AreaPredictor
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle, distance_between_points
from sese_control.pathPlanner import ReturnPlanner
from sese_interfaces.msg import OffboardControlPosition, DetectionArray, DetectionMSG
from px4_msgs.msg import VehicleLocalPosition
from std_msgs.msg import Bool
from geometry_msgs.msg import Pose


import numpy as np
from typing import List, Tuple
import math

Waypoint = Tuple[float, float]

class Task_6(BaseTask):
    def __init__(self):
        super().__init__('Task_6')

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition, 'CombinedOffboardControl/goal_pose')
        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.waiting_for_offboardcontroller: bool = False

        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self._vehicle_local_position_callback)
        self.robot_position: HNEDPosition = None 

        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)
        self.obstacles: List[Obstacle] = []
        self._is_new_obstacle = False

        self.create_qos_subscription(Pose, 'Task0/end_gate', self._save_gate_callback)

        self.start_postion = None

        self.area_predictor = AreaPredictor(self.obstacles)
        self.path_planner = ReturnPlanner()
        self.path: List[Waypoint] = []
        self.gate: List[Waypoint] = [] # punkt przed i za bramą
        
        self.is_first_iteration = True
        self.chill_and_ignore_mode = False

    def _save_gate_callback(self, msg):
        self.start_postion = NEDPosition(n=msg.position.x, e=msg.position.y, d=0)

    def _get_detections_callback(self, msg: DetectionArray):
        self.obstacles = []
        old_number = len(self.obstacles)
        

        for detection in msg.detections:
            object_class = detection.object_class


            self.obstacles.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))
            
        if old_number != len(self.obstacles):
            self._is_new_obstacle = True

    def _goal_reached_callback(self, msg: Bool):
        if msg.data:
            self.waiting_for_offboardcontroller = False

    def _vehicle_local_position_callback(self, msg: VehicleLocalPosition):
        self.robot_position = HNEDPosition(heading=msg.heading,ned=NEDPosition(n=msg.x,e=msg.y,d=msg.z))

    def _publish_offboard_position(self, target_pos: Waypoint, heading: float):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_pos[0]   # N
        msg.y = target_pos[1]   # E
        msg.z = 0.0             # D
        msg.heading = heading   # radiany

        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)

    def _update_gate_points_more_buoys(self, buoys: List[Obstacle]):
        black_buoys = [buoy for buoy in buoys if "black_buoy" in buoy.id]

        if len(black_buoys) == 2:
            self._update_gate_points_2_buoys(black_buoys[0], black_buoys[1])
        else:
            avg_n = sum(buoy.ned.n for buoy in buoys) / len(buoys)
            avg_e = sum(buoy.ned.e for buoy in buoys) / len(buoys)

            self.gate = [(avg_n, avg_e)]


    def _update_gate_points_2_buoys(self, buoy1: Obstacle, buoy2: Obstacle):
        mid_n = (buoy1.ned.n + buoy2.ned.n) / 2
        mid_e = (buoy1.ned.e + buoy2.ned.e) / 2

        # Calculate the vector direction of the gate
        dn = buoy2.ned.n - buoy1.ned.n
        de = buoy2.ned.e - buoy1.ned.e

        # Normalize the direction vector
        length = np.sqrt(dn**2 + de**2)
        if length == 0:
            return None 

        unit_dn = dn / length
        unit_de = de / length

        # Get perpendicular direction (rotated 90 degrees)
        perp_unit_dn = -unit_de  # Rotate counterclockwise
        perp_unit_de = unit_dn  

        # Define entry and exit points (1m before and after the gate)
        entry_point = (mid_n - perp_unit_dn, mid_e - perp_unit_de)  # Move backward
        exit_point =  (mid_n + perp_unit_dn, mid_e + perp_unit_de)  # Move forward

        self.gate = [entry_point, exit_point]

    def _find_target_positions(self):
        black_b = [buoy for buoy in self.obstacles if buoy.id == "black_buoy"]


        # Calculate distances from start position to each black buoy
        distances = [(buoy, distance_between_points((self.start_postion.n, self.start_postion.e), (buoy.ned.n, buoy.ned.e))) for buoy in black_b]

        # Sort buoys by distance
        sorted_buoys = sorted(distances, key=lambda x: x[1])

        # Get the two closest black buoys
        closest_black_buoys = [sorted_buoys[i][0] for i in range(min(2, len(sorted_buoys)))]

   


        # gate_area = self.area_prdictor.find_task_6_area()
        # self.get_logger().info(f'{gate_area=}')
        # if gate_area is None:
        #     return None

        # buoys_inside_gate = [
        #     obs for obs in self.obstacles 
        #     if gate_area["y_min"] <= obs.ned.e <= gate_area["y_max"] and 
        #     gate_area["x_min"] <= obs.ned.n <= gate_area["x_max"]
        # ]

        if len(closest_black_buoys) == 1: # Gdy znaleziono jedną boje, płyniemy w jej kierunku. Brame znajdziemy gdy będziemy bliżej
            self.gate = closest_black_buoys
        elif len(closest_black_buoys) == 2: # Gdy dwie to nawigujemy na brame
            self._update_gate_points_2_buoys(closest_black_buoys[0], closest_black_buoys[1]) 
        elif len(closest_black_buoys) > 2: # Gdy więcej to liczymy średnią i tam się nawigujemy
            self._update_gate_points_more_buoys(closest_black_buoys)

    def _get_target_position(self) -> Waypoint:
        if len(self.gate) > 0:
            return self.gate[0]
        return None
    
    def _update_path(self):
        start_position: Waypoint = (self.robot_position.ned.n, self.robot_position.ned.e)
        target_position: Waypoint = self._get_target_position()
        if target_position is None:
            return []
        
        return self.path_planner.findPath(start_position, target_position, self.obstacles)

    def _update_heading(self, target_position: Waypoint) -> float:
        # Oblicz heading w zakresie <-pi, pi>
        start_position: Waypoint = (self.robot_position.ned.n, self.robot_position.ned.e)

        delta_n = target_position[0] - start_position[0]
        delta_e = target_position[1] - start_position[1]

        return math.atan2(delta_e, delta_n)

    def _publish_next_point(self):
        if len(self.path) > 0:
            self._publish_offboard_position(self.path[0], self._update_heading(self.path[0]))
            self.path.pop(0)

    def target_point_reach(self, target_point: Waypoint):
        return distance_between_points((target_point[0], target_point[1]), (self.robot_position.ned.n, self.robot_position.ned.e)) < 0.2

    def make_decision(self):
        if self.robot_position is None or len(self.obstacles) == 0:
            self.get_logger().info(f'\n{self.robot_position=}\n{len(self.obstacles)=}\n')
            return

        if self.is_first_iteration:
            self.get_logger().info(f'self.is_first_iteration:')
            self._is_new_obstacle = False
            self.is_first_iteration = False

            self.area_predictor.update_obstacles(self.obstacles)

            self._find_target_positions()
            self.path = self._update_path()
            return

        if self._is_new_obstacle and not self.chill_and_ignore_mode:
            self.get_logger().info(f'self._is_new_obstacle')
            self._is_new_obstacle = False

            self.area_predictor.update_obstacles(self.obstacles)
            self._find_target_positions()
            self.path = self._update_path()
            self._publish_next_point()
            return

        if self.waiting_for_offboardcontroller:
            # self.get_logger().info(f'waiting_for_offboardcontroller')
            return

        if len(self.path) > 0:
            self.get_logger().info(f'_publish_next_point')
            self._publish_next_point()
            return

        self.get_logger().info('self.path jest pusty -> \nDopłyneliśmy do konca aktualnej ścieżki -> \n(jesteśmy przed gate || jesteśmy za gate(koniec :-)) || jakiś error )')
        
        if len(self.gate) > 0: 
            self.get_logger().info(f'len(self.gate) > 0')
            next_target_point = self.gate[0]
            if self.target_point_reach(next_target_point): # dopłynelismy w okolice bramy :)
                self.gate.pop(0)
                self.chill_and_ignore_mode = True
                if len(self.gate) > 0:
                    self.path = [self.gate[0]]
                return
               

        if len(self.gate) == 0: 
            self.get_logger().info(f'len(self.gate) == 0:')
            self.publish_end_of_task()

         
def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_6()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
        


