import math
import rclpy
from std_msgs.msg import Bool, Int32
from px4_msgs.msg import VehicleLocalPosition
from sese_interfaces.msg import OffboardControlPosition
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle
from sese_control.utils import distance_between_points, Obstacle, NEDPosition, ControlMode
from sese_control.pathPlanner.corridorPlanner import CorridorPlanner
from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_interfaces.msg import DetectionArray, DetectionMSG, OffboardControlPosition

class Task_2(BaseTask):
    GREEN_BUOY_LABEL = "green_buoy"
    RED_BUOY_LABEL = "red_buoy"
    YELLOW_BUOY_LABEL = "yellow_buoy"
    ORANGE_BOAT_LABEL = "black_triangle"
    BLACK_BOAT_LABEL = "black_plus"

    def __init__(self):
        super().__init__('Task_2')

        self.offboard_publisher = self.create_qos_publisher(
            OffboardControlPosition,
            'CombinedOffboardControl/goal_pose'
        )
        self.mode_publisher = self.create_qos_publisher(Bool, 'CombinedOffboardControl/control_mode')
        self.buoy_counter_publisher = self.create_qos_publisher(Int32, 'Task_2/buoy_counter')

        self.green_buoy = []  # Lista współrzędnych (latitude, longitude)
        self.red_buoy = []  # Lista współrzędnych (latitude, longitude)
        self.obstacle_list = []  # Lista współrzędnych (latitude, longitude)

        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position',
                                 self.vehicle_local_position_callback)
        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)

        self.robot_position = None  # Pozycja robota (latitude, longitude)
        self.waiting_for_offboardcontroller = False
        self.entry_reached = False
        self.updated_detections = False  # if updated detections came
        self.goal_points = []
        self.planner = CorridorPlanner()
        self.first_goal_point = False # a fix for goal callback 
        self.last_goal_point = False # go out of task 2 exit
        self.goal_point = None # final destination of Task 2

    def vehicle_local_position_callback(self, msg):
        if not msg:
            return

        self.robot_position = HNEDPosition(
            heading=msg.heading,
            ned=NEDPosition(n=msg.x, e=msg.y, d=0.0)
        )

    def _count_yellow_buoys(self):
        return sum(obstacle.id == self.YELLOW_BUOY_LABEL for obstacle in self.obstacle_list)

    def _goal_reached_callback(self, msg):
        if not msg.data:
            return

        if self.running:
            if self.last_goal_point:
                self.publish_end_of_task()
            if self.first_goal_point:
                self.entry_reached = True
                self.waiting_for_offboardcontroller = False

    def _get_detections_callback(self, msg):
        if self.robot_position == None:
            return
        
        self._clear_buoy_detections()
        for detection in msg.detections:
            object_class = detection.object_class

            if object_class in {self.YELLOW_BUOY_LABEL, self.ORANGE_BOAT_LABEL, self.BLACK_BOAT_LABEL}:
                self.obstacle_list.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))

            elif object_class == self.RED_BUOY_LABEL:
                self.red_buoy.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))

            elif object_class == self.GREEN_BUOY_LABEL:
                self.green_buoy.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))

        if self.running:
            self.updated_detections = True

    def _clear_buoy_detections(self):
        self.green_buoy.clear()
        self.red_buoy.clear()
        self.obstacle_list.clear()

    def _calc_goal_point(self, red_buoy, green_buoy):
        target_n = (red_buoy.ned.n + green_buoy.ned.n) / 2
        target_e = (red_buoy.ned.e + green_buoy.ned.e) / 2
        return target_n, target_e

    def publish_yellow_buoys(self):
        msg = Int32()
        msg.data = self._count_yellow_buoys()
        self.buoy_counter_publisher.publish(msg)
        self.get_logger().info(f'Yellow buoys: {msg.data}')

    def publish_new_global_position(self, target_n, target_e):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_n
        msg.y = target_e
        msg.z = 0.0
        msg.heading = self._update_heading((target_n, target_e))

        self.waiting_for_offboardcontroller = True
        self._clear_buoy_detections()
        self.offboard_publisher.publish(msg)
        self.get_logger().info(f'Publish pose: ({msg.x}, {msg.y}, {msg.z})')

    def publish_differential_position(self, surge, sway):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.DIFFERENTIAL_MODE

        msg.x = surge
        msg.y = sway
        msg.z = 0.0
        msg.heading = -self.robot_position.heading

        self.waiting_for_offboardcontroller = True
        self._clear_buoy_detections()
        self.get_logger().info(f'Publish differential pose: ({msg.x}, {msg.y}, {msg.z})')
        self.offboard_publisher.publish(msg)
        
    def _update_heading(self, target_position) -> float:
        # Oblicz heading w zakresie <-pi, pi>

        delta_n = target_position[0] - self.robot_position.ned.n
        delta_e = target_position[1] - self.robot_position.ned.e

        return math.atan2(delta_e, delta_n)

    def make_decision(self):
        if self.robot_position is None:
            return

        if self.waiting_for_offboardcontroller:
            return

        asv_pos = (self.robot_position.ned.n, self.robot_position.ned.e)
        # podpłyń do wejścia (środek pierwszej pary)
        if not self.entry_reached and self.updated_detections:
            try:
                closest_red = min(self.red_buoy, key=lambda buoy: distance_between_points(asv_pos, (buoy.ned.n, buoy.ned.e)))
                closest_green = min(self.green_buoy, key=lambda buoy: distance_between_points(asv_pos, (buoy.ned.n, buoy.ned.e)))
            except ValueError:
                return

            self.entry = self._calc_goal_point(closest_red, closest_green)
            self.publish_new_global_position(*self.entry)
            self.first_goal_point = True
            return

        # jeżeli jesteśmy blisko celu końcowego
        if self.entry_reached and self.goal_point != None:
            self.get_logger().info("czy coś tutaj jest")
            if distance_between_points(self.goal_point, asv_pos) < 0.2:
                    # ostatni goal point = koniec taska
                    self.get_logger().info("KONIEC TASKA, LECIMY W KOSMOS!!!!!!")
                    self.publish_yellow_buoys()
                    # self.publish_differential_position(-15.0, 0.0) # uncomment it if run in real environment
                    self.publish_new_global_position(20.0, 0.0)
                    self.last_goal_point = True
                    return

        # jeżeli nie ma trasy, wyznacz
        if len(self.goal_points) == 0 and self.entry_reached:
            try:
                furthest_red = max(self.red_buoy, key=lambda buoy: distance_between_points(self.entry, (buoy.ned.n, buoy.ned.e)))
                furthest_green = max(self.green_buoy, key=lambda buoy: distance_between_points(self.entry, (buoy.ned.n, buoy.ned.e)))
                self.get_logger().info(f'Red buoy: {furthest_red.ned.n}, {furthest_red.ned.e}')
                self.get_logger().info(f'Green buoy: {furthest_green.ned.n}, {furthest_green.ned.e}')
            except ValueError:
                return

            self.goal_point = self._calc_goal_point(furthest_red, furthest_green)

            obstacles = self.obstacle_list + self.red_buoy + self.green_buoy
            self.get_logger().info(f'GOAL POINT: {self.goal_point}')
            # self.get_logger().info(f'Obstacles: {obstacles}')
            
            path = self.planner.findPath(asv_pos, self.goal_point, obstacles)
            self.goal_points = path
            self.get_logger().info(f'Path: {path}')


        if len(self.goal_points) != 0:
            # publikuj kolejny goal_point
            if self.goal_point == self.goal_points[0]:
                self.get_logger().info("OSTATNI PUNKT")
            target_lat, target_lon = self.goal_points.pop(0)
            
            self.publish_new_global_position(target_lat, target_lon)


def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_2()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
