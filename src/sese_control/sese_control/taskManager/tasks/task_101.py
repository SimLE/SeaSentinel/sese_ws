###
# Zadanie utworzone w celu przetestowania trybu GLOBAL_MODE offboard controllera. 
# ASV powinien przemieścić się do 4 punktów na mapie, podążając po trajektorii kwadratu.
###

import rclpy
from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.utils import ControlMode
from sese_interfaces.msg import OffboardControlPosition
from std_msgs.msg import Bool

import math

class Task_101(BaseTask):
    def __init__(self):
        super().__init__('Task_101')

        stepSize = 5.0
        self.steps = [(-stepSize, 0.0) ,
                      (-stepSize, -stepSize),
                      (0.0, -stepSize) ,
                      (0.0, 0.0)   ]

        self.i = 0

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition, 'CombinedOffboardControl/goal_pose')

        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)

        self.waiting_for_offboardcontroller = False

    def _goal_reached_callback(self, msg):
        if not msg.data:
            return
        
        if msg.data:
            self.get_logger().info(f'Task 101: step reached')
            self.waiting_for_offboardcontroller = False


    def _publish_rotation(self):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE
        msg.x = self.steps[self.i][0]
        msg.y = self.steps[self.i][1]
        msg.z = 0.0
        msg.heading = 0.0
    
        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)
        self.get_logger().info(f'Published step to (X: {msg.x}, Y: {msg.y})')
        self.i += 1

    def make_decision(self):
        if self.waiting_for_offboardcontroller:
            # self.get_logger().info('Waiting for offboard controller')
            return

        if self.i == len(self.steps):
            self.get_logger().info('Task 101 completed')
            self.publish_end_of_task()
            return

        self._publish_rotation()


def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_101()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)