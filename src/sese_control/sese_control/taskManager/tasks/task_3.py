import rclpy
import math

from std_msgs.msg import Bool
from px4_msgs.msg import VehicleLocalPosition, VehicleGlobalPosition

from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition, Obstacle, distance_between_points
from sese_interfaces.msg import DetectionArray, DetectionMSG, OffboardControlPosition

from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy

import numpy as np




class DockTransform:
    def __init__(self):

        # PAMIETAJCIE PRZELICZYC NA  METRY!!

        dock_points = [ # fajnie by to było w przyszłości z configu czytać
            {"name": "P1", "ned": {"n": 190, "e": 0}},
            {"name": "P2", "ned": {"n": 190, "e": 70}},
            {"name": "P3", "ned": {"n": 160, "e": 150}},
            {"name": "P4", "ned": {"n": 80, "e": 150}},
            {"name": "P5", "ned": {"n": 0, "e": 150}},
            {"name": "P6", "ned": {"n": -80, "e": 150}},
            {"name": "P7", "ned": {"n": -160, "e": 150}},
            {"name": "P8", "ned": {"n": -190, "e": 70}},
            {"name": "P9", "ned": {"n": -190, "e": 0}},
            {"name": "P10", "ned": {"n": -190, "e": -70}},
            {"name": "P11", "ned": {"n": -160, "e": -150}},
            {"name": "P12", "ned": {"n": -80, "e": -150}},
            {"name": "P13", "ned": {"n": 0, "e": -150}},
            {"name": "P14", "ned": {"n": 80, "e": -150}},
            {"name": "P15", "ned": {"n": 160, "e": -150}},
            {"name": "P16", "ned": {"n": 190, "e": -70}},
            {"name": "S1", "ned": {"n": 150, "e": 70}},
            {"name": "S2", "ned": {"n": -150, "e": 70}},
            {"name": "S3", "ned": {"n": -150, "e": -70}},
            {"name": "S4", "ned": {"n": 150, "e": -70}},
            {"name": "D1", "ned": {"n": 80, "e": 70}},
            {"name": "D2", "ned": {"n": 0, "e": 70}},
            {"name": "D3", "ned": {"n": -80, "e": 70}},
            {"name": "D4", "ned": {"n": -80, "e": -70}},
            {"name": "D5", "ned": {"n": 0, "e": -70}},
            {"name": "D6", "ned": {"n": 80, "e": -70}},
            {"name": "O1", "ned": {"n": 80, "e": 100}},
            {"name": "O2", "ned": {"n": 0, "e": 100}},
            {"name": "O3", "ned": {"n": -80, "e": 100}},
            {"name": "O4", "ned": {"n": -80, "e": -100}},
            {"name": "O5", "ned": {"n": 0, "e": -100}},
            {"name": "O6", "ned": {"n": 80, "e": -100}},
            {"name": "B1", "ned": {"n": 80, "e": 10}},
            {"name": "B2", "ned": {"n": 0, "e": 10}},
            {"name": "B3", "ned": {"n": -80, "e": 10}},
            {"name": "B4", "ned": {"n": -80, "e": -10}},
            {"name": "B5", "ned": {"n": 0, "e": -10}},
            {"name": "B6", "ned": {"n": 80, "e": -10}}
        ]

        self.original_dock = np.array([[p['ned']['n'], p['ned']['e']] for p in dock_points])
        self.names = [p['name'] for p in dock_points]
        self.transformed_dock = self.original_dock.copy()
    
    def apply_transform(self, theta=0, dn=0, de=0, scale=1):
        """
        Przekształca punkty doku według zadanej rotacji i translacji.
        """
        translation = np.array([dn, de])
        rotation_matrix = np.array([
            [np.cos(theta), np.sin(theta)],
            [-np.sin(theta), np.cos(theta)]
        ])
        
        self.transformed_dock = ((self.original_dock * scale) @ rotation_matrix) + translation
    
    def get(self):
        """
        Zwraca listę punktów po transformacji.
        """
        return [{"name": name, "ned": {"n": ned[0], "e": ned[1]}} for name, ned in zip(self.names, self.transformed_dock)]



class Task_3(BaseTask):
    BOAT_LABELS = ["black_plus", "black_triangle"]
    BANNER_LABELS = [
        'blue_circle',
        'green_circle',
        'red_circle',
        'blue_triangle',
        'green_triangle',
        'red_triangle',
        'blue_square',
        'green_square',
        'red_square'
    ]

    def __init__(self):
        super().__init__('Task_3')

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition,
                                                            'CombinedOffboardControl/goal_pose')
        self.mode_publisher = self.create_qos_publisher(Bool, 'CombinedOffboardControl/control_mode')

        qos_profile_px4 = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=10
        )

        self.create_subscription(VehicleLocalPosition, '/fmu/in/gazebo_vehicle_local_position',
                                 self.vehicle_local_position_callback, qos_profile_px4)
        self.robot_position = None  # Pozycja robota (latitude, longitude)
        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.create_qos_subscription(DetectionArray, 'Object_list/obstacles', self._get_detections_callback)

        self.banners = []
        self.boats = []

        self.waiting_for_offboardcontroller = False
        self.updated_detections = False  # if updated detections came
        self.dock_position = None
        self.target_banner = "blue_circle"  # dostosować

        # Aktualny stan taska
        self.is_first_iteration = True
        self.sailing_to_dock_area = False
        self.dock_mapping = False
        self.docking = False
        self.mark_territory = False


    def vehicle_local_position_callback(self, msg):
        if not msg:
            return

        self.robot_position = HNEDPosition(
            heading=msg.heading,
            ned=NEDPosition(n=msg.x, e=msg.y, d=0.0)
        )

    def _goal_reached_callback(self, msg):
        if not msg.data:
            return

        self.waiting_for_offboardcontroller = False

    def _get_detections_callback(self, msg):
        if self.robot_position is None:
            return

        self._clear_detections()
        for detection in msg.detections:
            object_class = detection.object_class

            if object_class in self.BOAT_LABELS:
                self.boats.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))
            elif object_class == self.BANNER_LABELS:
                self.banners.append(Obstacle(
                    ned=NEDPosition(n=detection.ned_n, e=detection.ned_e, d=0),
                    id=object_class
                ))

        # if len(banners) > 1: zmapuj pozycję doku
        self.updated_detections = True

    def _clear_detections(self):
        self.boats.clear()
        self.banners.clear()

    def publish_new_global_position(self, target_n, target_e):
        self.waiting_for_offboardcontroller = True
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.GLOBAL_MODE

        msg.x = target_n
        msg.y = target_e
        msg.z = 0.0
        msg.heading = self._update_heading((target_n, target_e))

        self._clear_bouy_detections()
        self.offboard_publisher.publish(msg)
        self.get_logger().info(f'Publish pose: ({msg.x}, {msg.y}, {msg.z})')

    def _update_heading(self, target_position) -> float:
        # Oblicz heading w zakresie <-pi, pi>
        delta_n = target_position[0] - self.robot_position.ned.n
        delta_e = target_position[1] - self.robot_position.ned.e

        return math.atan2(delta_e, delta_n)

    def dock_exist(self):
        # sprawdź czy dock istniej (jakieś bannery w object_list  ???)
        self.get_logger().error(f'def dock_exist(self) -> funkcja nie zaimplementowana')
        return False

    def sailing_to_dock_behaviour(self):
        # 1. znajdź ścieżkę do konkretnego miejsca w docku (np. B1) i wyznacz ścieżkę(Default planner)
        # 2. Płyń
        # 3. rozważ co zrobić jeżeli są nowe detekcje
        # Return True jeżeli skończyłeś dany stan
        self.get_logger().error(f'sailing_to_dock_behaviour(self): -> funkcja nie zaimplementowana')
        return True

    def dock_mapping_behaviour(self):
        # 1. popłyń trasą prowadzącą przez wyznaczone na sztywno punkty (np. B1 -> C1 -> Z1 -> ... -> B1)
        # 2. w wyznaczonych miejscach obróć się o 90 stopni aby zmapować dock lub płyń bokiem cały czas skierowany do docku
        # Return True jeżeli skończyłeś dany stan
        self.get_logger().error(f'dock_mapping_behaviour(self): -> funkcja nie zaimplementowana')
        return True

    def docking_behaviour(self):
        # 1. znajdź pasujące docki (na podstawie banerów)
        # 2. wybierz ten dock(z pasujących), który ma największy distance_between_points() pomiędzy tym dockiem a najbliższym statkiem
        # 3. płyń
        # Return True jeżeli skończyłeś dany stan
        self.get_logger().error(f'docking_behaviour(self): -> funkcja nie zaimplementowana')
        return True

    def mark_territory_behaviour(self): 
        # 1. W sumie nie wiem xD
        # 2. Znajdź statki w doku (distance_between_points(punkt_doku, statek) < offset)
        # 3. Podpłyń (zdefiniowany punkt)
        # 4. rozpoznaj kolor
        # 5. załatw potrzebę
        # Return True jeżeli skończyłeś dany stan
        self.get_logger().error(f'mark_territory_behaviour(self): -> funkcja nie zaimplementowana')
        return False

    def make_decision(self):
        if self.robot_position is None:
            return

        if not self.dock_exist():
            self.get_logger().info(f'Nie ma docku, nie ma taska ;-(')
            self.publish_end_of_task()
            return

        if self.is_first_iteration:
            self.is_first_iteration = False
            self._is_new_obstacle = False

            self.sailing_to_dock_area = True
            # w sumie dock_exist == True -> to nim poruszaj :)
            return

        if self._is_new_obstacle: 
            self._is_new_obstacle = False
            # jeżeli dock to nim poruszaj :)

        if self.waiting_for_offboardcontroller:
            return

            
        if self.sailing_to_dock_area:   # podpłyń do doku
            if self.sailing_to_dock_behaviour():
                self.get_logger().info(f'Sailing to dock area complete -> start dock mapping')
                self.sailing_to_dock_area = False
                self.dock_mapping = True
            return

        elif self.dock_mapping:         # okrąż dok, spojrzyj na bannery i statki 
            if self.dock_mapping_behaviour():
                self.dock_mapping = False
                self.docking = True
            return

        elif self.docking:              # wyznacz wolny dock i zaparkuj
            if self.docking_behaviour():
                self.docking = False
                self.mark_territory = True
            return

        elif self.mark_territory:       # strzel do statków w dockach
            if self.mark_territory_behaviour():

                self.get_logger().info(f'🎉 Task 3 successfully completed! Even the ocean not gets in the way!!! 🌊 ')                                                                                                                                                                                                                                                                                                                                                                                                                                            ; special_message = bytes.fromhex("0a0a536f6d65776865726520696e204575726f70652c2061206c6f6e656c792070726f6772616d6d657220617761697473e280a6206e6f7420666f7220616e6f74686572206275672c2062757420666f72206120706f7374636172642e20f09f93ac20204b65657020746865206c6f677320636c65616e20616e6420746865206d61696c626f782066756c6c2120f09f9886e29c88efb88f20200a0a").decode("utf-8") ;self.get_logger().info(f'\n\n{special_message}  \n\n')
                self.publish_end_of_task()                                                                                                                                                                                                                                                                                                                                   
            
        else:
            self.get_logger().info(f'Tu nas nie powinno być')
            self.publish_end_of_task()
            return


def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_3()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
