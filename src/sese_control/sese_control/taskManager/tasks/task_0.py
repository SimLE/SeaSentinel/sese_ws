from email import utils
import rclpy
from sese_control.taskManager.tasks.baseTask import BaseTask
from sese_control.utils import ControlMode, HNEDPosition, NEDPosition

from sese_interfaces.msg import OffboardControlPosition
from std_msgs.msg import Bool
from geometry_msgs.msg import Pose
from px4_msgs.msg import VehicleLocalPosition

import math

class Task_0(BaseTask):
    def __init__(self):
        super().__init__('Task_0')

        # Inicjalizacja parametrów obrotu
        self.yaw_angle = 0.0   # Aktualny kąt yaw w radianach
        self.angle = 120          
        self.step = math.radians(self.angle) # Stopień obrotu na iterację 
        self.max_angle = math.radians(360)  # Maksymalny kąt (360 stopni)

        self.offboard_publisher = self.create_qos_publisher(OffboardControlPosition, 'CombinedOffboardControl/goal_pose')
        self.gate_publisher = self.create_qos_publisher(Pose, 'Task0/end_gate') # topic, który przechowuje pozycję ASV, która będzie potrzebna do obliczenia pozycji czarnej bramy

        self.create_qos_subscription(Bool, 'CombinedOffboardControl/goal_reached', self._goal_reached_callback)
        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback)

        self.vehicle_local_position = None

        self.waiting_for_offboardcontroller = False
        self.counter = 0
        self.is_first_iteration = True

    def vehicle_local_position_callback(self, msg):
        if not msg:
            return

        self.vehicle_local_position = HNEDPosition(
            heading=msg.heading,
            ned=NEDPosition(n=msg.x, e=msg.y, d=0.0)
        )

    def _goal_reached_callback(self, msg):
        if not msg.data:
            return
        
        if self.running:
            if msg.data:
                self.waiting_for_offboardcontroller = False

    def _is_full_rotation_completed(self):
        return self.yaw_angle >= self.max_angle

    def _publish_rotation(self):
        msg = OffboardControlPosition()
        msg.control_mode = ControlMode.DIFFERENTIAL_MODE

        msg.x = 0.0
        msg.y = 0.0
        msg.z = 0.0
        msg.heading = self.step
 
        self.waiting_for_offboardcontroller = True
        self.offboard_publisher.publish(msg)
        self.yaw_angle += self.step

    def wait(self):
        if self.counter < 30: # czekamy 3sek zanim rozpoczniemy prace (rozruch px4 i offboardController)
            self.counter += 1
            return True
        return False 

    def make_decision(self):
        if self.wait():
            return

        if self.is_first_iteration and self.vehicle_local_position is not None:
            self.is_first_iteration = False

            msg = Pose()

            msg.position.x = self.vehicle_local_position.ned.n
            msg.position.y = self.vehicle_local_position.ned.e
            msg.position.z = self.vehicle_local_position.ned.d

            self.gate_publisher.publish(msg)
            

        if self.waiting_for_offboardcontroller:
            return

        if self._is_full_rotation_completed():
            self.get_logger().info('Task 0: Full rotation completed')
            self.publish_end_of_task()
            return

        self._publish_rotation()


         
def main(args=None) -> None:
    rclpy.init(args=args)
    task = Task_0()
    rclpy.spin(task)
    task.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass