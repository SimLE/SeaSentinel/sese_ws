import rclpy
from rclpy.parameter import Parameter
from std_msgs.msg import String
from std_msgs.msg import Bool
import time

from sese_control.utils import BaseNode


class TaskManager(BaseNode):
    def __init__(self):
        super().__init__('task_manager')
        self.declare_parameter('task_list', ["Task_0", "Task_1", "Task_2", "Task_R", "Task_3", "Task_4", "Task_5", "Task_6"])
        self.task_list = self.get_parameter('task_list').get_parameter_value().string_array_value
        self.get_logger().info(f'{self.task_list=}')
        self.current_task_index = 0

        self.create_qos_subscription(String, 'taskManager/task_done', self.task_done_callback)
        self.task_publisher = self.create_qos_publisher(String, 'taskManager/next_task')

        time.sleep(3.0)
        self.publish_next_task()

    def publish_next_task(self):
        self.get_logger().info(f'Publishing next task: {self.task_list[self.current_task_index]}')
        msg = String()
        msg.data = self.task_list[self.current_task_index]
        self.task_publisher.publish(msg)

    def task_done_callback(self, msg):
        if msg.data == f"{self.task_list[self.current_task_index]}_completed":

            self.get_logger().info(f'{self.task_list[self.current_task_index]} completed, moving to the next task.')
            self.current_task_index += 1

            if self.current_task_index >= len(self.task_list):
                self.get_logger().info('All tasks completed.')
                return

            self.publish_next_task()


def main(args=None) -> None:
    rclpy.init(args=args)
    taskManager = TaskManager()
    rclpy.spin(taskManager)
    taskManager.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e)