import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Bool
import csv
import time
from sese_control.utils import BaseNode

class WaypointReaderNode(BaseNode):
    def __init__(self):
        super().__init__('waypoint_reader_node')

        csv_path = self.declare_parameter('csv_path', 'config/waypoints.csv').get_parameter_value().string_value
        print(f"{csv_path=}")

        self.waypoint_pub = self.create_qos_publisher(PoseStamped, 'next_waypoint')
        self.waypoints = self._load_waypoints(csv_path)
        self.current_waypoint = 0

        # Subskrybent nasłuchujący potwierdzenia osiągnięcia waypointa
        self.goal_reached_sub = self.create_qos_subscription(
            Bool,
            'goal_reached',
            self.on_goal_reached
        )

        # Publikuj pierwszy waypoint, jeśli dostępny
        time.sleep(1.0)
        if self.waypoints:
            self.publish_next_waypoint()

    def _load_waypoints(self, csv_path):
        waypoints = []
        with open(csv_path, mode='r') as file:
            reader = csv.reader(file)
            for row in reader:
                x, y, z = map(float, row)
                waypoints.append((x, y, z))
        return waypoints

    def publish_next_waypoint(self):
        if self.current_waypoint < len(self.waypoints):
            x, y, z = self.waypoints[self.current_waypoint]
            pose = PoseStamped()
            pose.pose.position.x = x
            pose.pose.position.y = y
            pose.pose.position.z = z
            self.waypoint_pub.publish(pose)
            self.get_logger().info(f'Wysłano waypoint {self.current_waypoint}')
            self.current_waypoint += 1
        else:
            self.get_logger().info('Wszystkie waypointy zostały osiągnięte.')

    def on_goal_reached(self, msg):
        # Publikuj następny waypoint, gdy poprzedni został osiągnięty
        if msg.data:
            self.publish_next_waypoint()

def main(args=None):
    rclpy.init(args=args)
    
    node = WaypointReaderNode()
    
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()