from sese_control.pathPlanner.basePlanner import PlannerInterface, Waypoint
from sese_control.utils import Obstacle, distance_between_points, NEDPosition
import networkx as nx
from typing import List

class DockPlanner(PlannerInterface):

    def __init__(self):
        # Dock jest opisany tu: sese_ws/src/sese_control/config/dock_plan.jpg
        
        # tu tworzymy graf
        self.graph = nx.Graph()
        edges = ["P1", "D2", "S1"] # I tak wszystkie punkty. 
        
        for edge in edges:                  # Trzeba sprawdzić. Może powyższa lista nie jest potrzebna 
            self.graph.add_node(edge)       # i dodada brakujące wierzchołki podczas tworzenia krawędzi
            
        self.graph.add_edge("P1", "D2", weight=1.0) # Tak łączymy wszystkie punkty zgodnie z dock_plan.jpg.
        self.graph.add_edge("D2", "S1", weight=1.0) # Raczej staramy się robić prosty graf (minimalna liczba krawędzi a nie każdy możliwy punkt z każdym)

    def findDockPath(self, start, goal):
        # PlannerInterface wymaga implementacji findPath(self, start: str, goal: str, obstacles: List[Obstacle])
        # my tu nie potrzebujemy obstacles
        return self.findPath(start, goal, [])

    def findPath(self, start: str, goal: str, obstacles: List[Obstacle]) -> List[Waypoint]:
        # tu nie trzeba na przesuniętych punktów docku
        # Chcemy uzyskać ścieżkę np. ['P1', 'D2', 'S1']
        # A nawigując się w make_decision sprawdzimy gdzie dany punkt leży
        try:
            path = nx.shortest_path(self.graph, source=start, target=goal, weight="weight")
            return path
        except nx.NetworkXNoPath: # brak ścieżki między startem a celem
            return []