from sese_control.pathPlanner.basePlanner import PlannerInterface, Waypoint
from sese_control.utils import Obstacle, distance_between_points, NEDPosition
import networkx as nx
from typing import List


class CorridorPlanner(PlannerInterface):
    MAX_DISTANCE_BETWEEN_WAYPOINTS = 3.0    # maksymalny dystans między waypointami [w metrach]
    BUOY_OFFSET = 0.6                       # jak daleko od boi generować wierzchołki grafu [w metrach]

    @staticmethod
    def _different_obstacle(point1: Obstacle, point2: Obstacle) -> bool:
        return point1.id != point2.id

    @staticmethod
    def _path_distance_is_correct(point1: Obstacle, point2: Obstacle) -> bool:
        distance = distance_between_points((point1.ned.n, point1.ned.e), (point2.ned.n, point2.ned.e))
        return  distance < CorridorPlanner.MAX_DISTANCE_BETWEEN_WAYPOINTS

    @staticmethod
    def _generate_surrounding_obstacles(obstacle: Obstacle, buffer_distance: float) -> List[Obstacle]:
        """
        Generuje punkty otaczające przeszkodę, aby pozwolić na jej ominięcie.
        """
        n, e = obstacle.ned.n, obstacle.ned.e
        return [
            Obstacle(NEDPosition(n + buffer_distance, e, 0.0), obstacle.id),
            Obstacle(NEDPosition(n - buffer_distance, e, 0.0), obstacle.id),
            Obstacle(NEDPosition(n, e + buffer_distance, 0.0), obstacle.id),
            Obstacle(NEDPosition(n, e - buffer_distance, 0.0), obstacle.id),
        ]

    @staticmethod
    def path_len(graph, path):
        """zwraca długość ścieżki w grafie"""
        length = 0
        for i in range(1, len(path)):
            length += graph[path[i-1]][path[i]]["weight"]
        return length

    def findPath(self, start: Waypoint, goal: Waypoint, obstacles: List[Obstacle]) -> List[Waypoint]:
        graph = nx.Graph()
        obstacles_to_connect: List[Obstacle] = [Obstacle(NEDPosition(*start, 0.0), "Start")]

        goal_valid = True
        for obs in obstacles:
            surrounding_obstacles = self._generate_surrounding_obstacles(obs, CorridorPlanner.BUOY_OFFSET)
            obstacles_to_connect.extend(surrounding_obstacles)

            # sprawdzić, czy goal point nachodzi na jakąś bojkę
            if distance_between_points(goal, (obs.ned.n, obs.ned.e)) < CorridorPlanner.BUOY_OFFSET \
                    and goal_valid:
                goal_valid = False
                surrounding_obstacles = self._generate_surrounding_obstacles(Obstacle(NEDPosition(*goal, 0.0), "Goal"),
                                                                             CorridorPlanner.BUOY_OFFSET)
                new_goals = surrounding_obstacles

        if goal_valid:
            obstacles_to_connect.append(Obstacle(NEDPosition(*goal, 0.0), "Goal"))
            if self._is_path_clear(start, goal, obstacles):
                return [goal]
        else:
            obstacles_to_connect.extend(new_goals)

        # Twórz krawędzie między punktami, które nie kolidują z przeszkodami
        for i, obstacle_1 in enumerate(obstacles_to_connect):
            graph.add_node((obstacle_1.ned.n, obstacle_1.ned.e))
            for j in range(i + 1, len(obstacles_to_connect)):  # j zaczyna się od i + 1
                obstacle_2 = obstacles_to_connect[j]

                graph.add_node((obstacle_2.ned.n, obstacle_2.ned.e))
                if (
                        self._is_path_clear((obstacle_1.ned.n, obstacle_1.ned.e), (obstacle_2.ned.n, obstacle_2.ned.e),
                                            obstacles)
                        and self._different_obstacle(obstacle_1, obstacle_2)
                        and self._path_distance_is_correct(obstacle_1, obstacle_2)
                ):
                    graph.add_edge(
                        (obstacle_1.ned.n, obstacle_1.ned.e),
                        (obstacle_2.ned.n, obstacle_2.ned.e),
                        weight=distance_between_points((obstacle_1.ned.n, obstacle_1.ned.e),
                                                       (obstacle_2.ned.n, obstacle_2.ned.e))
                    )

        try:
            if goal_valid:
                path = nx.shortest_path(graph, source=start, target=goal, weight="weight")
                return path[1:]
            else:
                paths = [nx.shortest_path(graph, source=start, target=(g.ned.n, g.ned.e), weight="weight") for g in
                         new_goals]
                return min(paths, key=lambda p: self.path_len(graph, p))[1:]
        except nx.NetworkXNoPath:
            print("Brak ścieżki między startem a celem.")
            return []
