from abc import ABC, abstractmethod
from typing import List, Tuple

from sese_control.utils import Obstacle, distance_to_line

Waypoint = Tuple[float, float]

class PlannerInterface(ABC):
    DISTANCE_TO_BUOY = 0.5 # Określa minimalny dystans, z którym musimy ominąć wszystkie boje [w metrach]
    DISTANCE_TO_DOCK = 3.
    DOCK_LABELS = [
        'blue_circle',
        'green_circle',
        'red_circle',
        'blue_triangle',
        'green_triangle',
        'red_triangle',
        'blue_square',
        'green_square',
        'red_square'
    ]

    @staticmethod
    def _is_path_clear(point1: Waypoint, point2: Waypoint, obstacles: List[Obstacle]):
        """
        Sprawdza, czy ścieżka między dwoma punktami nie koliduje z przeszkodami.
        """
        for obstacle in obstacles:
            obstacle_point: Waypoint = (obstacle.ned.n, obstacle.ned.e)
            distance = distance_to_line(point1, point2, obstacle_point)
            min_dist = PlannerInterface.DISTANCE_TO_DOCK if obstacle.id in PlannerInterface.DOCK_LABELS else PlannerInterface.DISTANCE_TO_BUOY
            if distance is not None and distance < min_dist:
                return False
        return True

    @abstractmethod
    def findPath(self, start: Waypoint, stop: Waypoint, obstacles: List[Obstacle]) -> List[Waypoint]:
        pass
