from sese_control.pathPlanner.basePlanner import PlannerInterface, Waypoint
from sese_control.utils import Obstacle, distance_between_points
import networkx as nx
from typing import List

class DeafultPlanner(PlannerInterface):
    BUOY_OFFSET = 1.0                   # jak daleko od boi generować wierzchołki grafu [w metrach]
    DOCK_OFFSET = 5.

    @staticmethod 
    def _generate_surrounding_points(obstacle: Obstacle, buffer_distance: float):
        """
        Generuje punkty otaczające przeszkodę, aby pozwolić na jej ominięcie.
        """
        n, e = obstacle.ned.n, obstacle.ned.e
        return [
            (n + buffer_distance, e),
            (n - buffer_distance, e), 
            (n, e + buffer_distance),  
            (n, e - buffer_distance),
        ]

    def findPath(self, start: Waypoint, goal: Waypoint, obstacles: List[Obstacle]) -> List[Waypoint]:
        if DeafultPlanner._is_path_clear(start, goal, obstacles):
            return [goal]

        graph = nx.Graph()
        points_to_connect: List[Waypoint] = [start, goal]

        for obstacle in obstacles:
            offset = self.DOCK_OFFSET if obstacle.id in self.DOCK_LABELS else self.BUOY_OFFSET
            surrounding_points = self._generate_surrounding_points(obstacle, offset)
            points_to_connect.extend(surrounding_points)

        for i, point1 in enumerate(points_to_connect):
            for j in range(i + 1, len(points_to_connect)):
                point2 = points_to_connect[j]

                graph.add_node(point1)
                graph.add_node(point2)
                if DeafultPlanner._is_path_clear(point1, point2, obstacles):
                    graph.add_edge(point1, point2, weight=distance_between_points(point1, point2))

        try:
            path = nx.shortest_path(graph, source=start, target=goal, weight="weight")
            return path[1:]
        except nx.NetworkXNoPath:
            print("Brak ścieżki między startem a celem.")
            return []
