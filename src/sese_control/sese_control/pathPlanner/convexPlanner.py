from sese_control.pathPlanner.basePlanner import PlannerInterface, Waypoint
from sese_control.utils import Obstacle, distance_between_points, NEDPosition
from collections import deque
from shapely.geometry import Polygon
from scipy.spatial import ConvexHull
import numpy as np
from typing import List
import rclpy

class ConvexPlanner(PlannerInterface):
   
    @staticmethod
    def compute_convex_hull(obstacles: List[Obstacle]) -> np.ndarray:
        """
        Creates a convex hull around obstacles.
        """
        points = np.array([(obstacle.ned.n, obstacle.ned.e) for obstacle in obstacles])
        if len(points) < 3:
            return None  # Convex hull cannot be formed

        hull = ConvexHull(points)
        return points[hull.vertices]

    @staticmethod
    def generate_surrounding_obstacles(obstacle: Obstacle, buffer_distance: float) -> List[Obstacle]:
        """
        Generates surrounding points around an obstacle to facilitate avoidance.
        """
        n, e = obstacle.ned.n, obstacle.ned.e
        return [
            Obstacle(NEDPosition(n + buffer_distance, e, 0.0), obstacle.id),
            Obstacle(NEDPosition(n - buffer_distance, e, 0.0), obstacle.id),
            Obstacle(NEDPosition(n, e + buffer_distance, 0.0), obstacle.id),
            Obstacle(NEDPosition(n, e - buffer_distance, 0.0), obstacle.id),
        ]

    @staticmethod
    def find_closest_point(waypoint: Waypoint, hull_points: np.ndarray) -> Waypoint:
        """
        Finds the closest point on the convex hull to the waypoint.
        """
        return min(hull_points, key=lambda point: np.linalg.norm(np.array(waypoint) - np.array(point)))


    @staticmethod
    def rotate_hull_counterclockwise(hull_points: np.ndarray, start: Waypoint, goal: Waypoint) -> List[Waypoint]:
        """
        Rotates convex hull points so that the closest point to the start is first,
        then orders them in a counterclockwise direction, and stops at the closest point to the goal.
        """
        
        hull_list = [tuple(p) for p in hull_points][::-1]
        closest_to_start = ConvexPlanner.find_closest_point(start, hull_list)
        closest_to_goal = ConvexPlanner.find_closest_point(goal, hull_list)

        closest_index = hull_list.index(tuple(closest_to_start))
        rotated_hull = deque(hull_list)
        rotated_hull.rotate(-(closest_index+1))  # Obrót w lewo (CCW)

        out = [closest_to_start]
        for waypoint in rotated_hull:
            if waypoint == closest_to_goal:
                break
            out.append(waypoint)
            
        return out

    @staticmethod
    def rotate_hull_clockwise(hull_points: np.ndarray, start: Waypoint, goal: Waypoint) -> List[Waypoint]:
        """
        Rotates convex hull points so that the closest point to the start is first,
        then orders them in a clockwise direction, and stops at the closest point to the goal.
        """
        
        hull_list = [tuple(p) for p in hull_points]
        closest_to_start = ConvexPlanner.find_closest_point(start, hull_list)
        closest_to_goal = ConvexPlanner.find_closest_point(goal, hull_list)

        closest_index = hull_list.index(tuple(closest_to_start))
        rotated_hull = deque(hull_list)
        rotated_hull.rotate(-(closest_index+1))

        out = [closest_to_start]
        for waypoint in rotated_hull:
            if waypoint == closest_to_goal:
                break
            out.append(waypoint)
            
        return out

    def findPath(self, start: Waypoint, goal: Waypoint, obstacles: List[Obstacle]) -> List[Waypoint]:
        obstacles_to_connect: List[Obstacle] = []

        for obstacle in obstacles:
            if obstacle.id in {"black_buoy", "blue_buoy"}:
                buffer_distance = 1.0
                obstacles_to_connect.extend(ConvexPlanner.generate_surrounding_obstacles(obstacle, buffer_distance))

        convex_hull = ConvexPlanner.compute_convex_hull(obstacles_to_connect)
        if convex_hull is None:
            return []

        rotated_hull = ConvexPlanner.rotate_hull_clockwise(convex_hull, start, goal)

        if len(rotated_hull) > 1:
            if self._is_path_clear(start, rotated_hull[1], obstacles):
                rotated_hull = rotated_hull[1:]


        return rotated_hull + [goal]
