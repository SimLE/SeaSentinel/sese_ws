from sese_control.pathPlanner.deafultPlanner import DeafultPlanner, Waypoint
import math

class ReturnPlanner(DeafultPlanner):
    def segment_intersects_circle(x1, y1, x2, y2, px, py, r):
        """Sprawdza, czy odcinek (x1, y1) - (x2, y2) przecina się z kołem o środku (cx, cy) i promieniu r."""
        dx, dy = x2 - x1, y2 - y1
        if dx == 0 and dy == 0:
            return math.hypot(px - x1, py - y1) <= r

        t = ((px - x1) * dx + (py - y1) * dy) / (dx * dx + dy * dy)
        t = max(0, min(1, t))

        closest_x, closest_y = x1 + t * dx, y1 + t * dy
        return math.hypot(px - closest_x, py - closest_y) <= r

    @staticmethod
    def _is_path_clear(point1: Waypoint, point2: Waypoint, obstacles):
        """
        Sprawdza, czy ścieżka między dwoma punktami nie koliduje z przeszkodami.
        """
        for obstacle in obstacles:
            obstacle_point: Waypoint = (obstacle.ned.n, obstacle.ned.e)
            min_dist = ReturnPlanner.DISTANCE_TO_DOCK if obstacle.id in ReturnPlanner.DOCK_LABELS else ReturnPlanner.DISTANCE_TO_BUOY
            if ReturnPlanner.segment_intersects_circle(*point1, *point2, *obstacle_point, min_dist):
                return False
        return True
