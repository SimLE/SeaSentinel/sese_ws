import rclpy
from std_msgs.msg import Bool
import math

from sese_interfaces.msg import OffboardControlPosition
from sese_control.offboard_control.BaseOffboardControl import BaseOffboardControl
from sese_control.utils import HNEDPosition, NEDPosition, distance_to_ned2, ControlMode

def normalize_angle_radians(angle, x):
    """
    Normalizuje kąt w radianach do przedziału (-x, x] z przycinaniem wartości przekraczających x.
    
    :param angle: Kąt wejściowy w radianach
    :param x_degrees: Granica przedziału w radianach
    :return: Znormalizowany kąt w radianach
    """

    # Normalizacja do przedziału (-pi, pi]
    normalized = (angle + math.pi) % (2 * math.pi) - math.pi

    # Przycinanie do przedziału (-x, x]
    if normalized > x:
        return x
    elif normalized < -x:
        return -x
    else:
        return normalized


class CombinedOffboardControl(BaseOffboardControl):
    MAX_ANGLE = math.radians(160) # Maksymalny kąt obratu, na który pozwala PID w ASV
    
    def __init__(self) -> None:
        super().__init__('combined_offboard_control')

        self.goal_reached_pub = self.create_qos_publisher(Bool, 'CombinedOffboardControl/goal_reached')

        self.create_qos_subscription(OffboardControlPosition, 'CombinedOffboardControl/goal_pose', self.new_goal_callback)

        self.target_position: HNEDPosition = None

    def _shift_to_ned(self, d_surge, d_sway, d_heave) -> None:
        current_ned_n = self.vehicle_local_position.x
        current_ned_e = self.vehicle_local_position.y
        current_heading = self.vehicle_local_position.heading

        new_n, new_e = distance_to_ned2(d_sway, d_surge, current_ned_n, current_ned_e, current_heading)
        new_d = 0.0
        return new_n, new_e, new_d
    
    def new_goal_callback(self, msg) -> None:
        
        if msg.control_mode == ControlMode.GLOBAL_MODE:
            self.get_logger().info(f"ControlMode.GLOBAL_MOD")
            self.target_position = HNEDPosition(
                heading=normalize_angle_radians(msg.heading, CombinedOffboardControl.MAX_ANGLE),
                ned=NEDPosition(n=msg.x, e=msg.y, d=msg.z)
            )

        elif msg.control_mode == ControlMode.DIFFERENTIAL_MODE:
            self.get_logger().info(f"ControlMode.DIFFERENTIAL_MODE\n\n")
            new_n, new_e, new_d = self._shift_to_ned(msg.x, msg.y, msg.z)
            self.target_position = HNEDPosition(
                heading=normalize_angle_radians(self.vehicle_local_position.heading + msg.heading, CombinedOffboardControl.MAX_ANGLE),
                ned=NEDPosition(new_n, new_e, new_d)
                )
            
    def _goal_has_reached(self):
        # self.get_logger().info("check goal reach")
        return (
            abs(self.vehicle_local_position.x - self.target_position.ned.n) <= self.acceptable_error
            and abs(self.vehicle_local_position.y - self.target_position.ned.e) <= self.acceptable_error
            and abs(self.vehicle_local_position.heading - self.target_position.heading) <= self.acceptable_yaw_error
        )

    def timer_callback(self) -> None:
        """Callback function for the timer."""
        self.publish_offboard_control_heartbeat_signal()

        if self.offboard_setpoint_counter < 5:
            self.get_logger().info(f"{self.offboard_setpoint_counter=}")
            self.offboard_setpoint_counter += 1
            return

        if self.target_position is None:
            self.get_logger().info(f"{self.target_position=}")
            return

        if self._goal_has_reached():
            # self.get_logger().info(f"_goal_has_reached")
            msg = Bool()
            msg.data = True
            self.goal_reached_pub.publish(msg)

        self.engage_offboard_mode()
        self.arm()

        self.publish_position_setpoint(self.target_position)


def main(args=None) -> None:
    print('Starting CombinedOffboardControl node...')
    rclpy.init(args=args)
    offboard_control = CombinedOffboardControl()
    rclpy.spin(offboard_control)
    offboard_control.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass