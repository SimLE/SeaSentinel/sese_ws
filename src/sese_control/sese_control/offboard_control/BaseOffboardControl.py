import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import OffboardControlMode, TrajectorySetpoint, VehicleCommand, VehicleLocalPosition, VehicleStatus, VehicleGlobalPosition
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Bool
from sese_control.utils import (
    ned_to_geodetic,
    geodetic_to_ned,
    distance_to_ned,
    HNEDPosition
     )
from abc import ABC, abstractmethod
from sese_control.utils import BaseNode
import math

class BaseOffboardControl(BaseNode, ABC):
    def __init__(self, name:str) -> None:
        super().__init__(name)


        self.offboard_control_mode_publisher = self.create_qos_publisher(
            OffboardControlMode, '/fmu/in/offboard_control_mode')
        self.trajectory_setpoint_publisher = self.create_qos_publisher(
            TrajectorySetpoint, '/fmu/in/trajectory_setpoint')
        self.vehicle_command_publisher = self.create_qos_publisher(
            VehicleCommand, '/fmu/in/vehicle_command')

    
        self.create_qos_subscription(VehicleGlobalPosition, '/fmu/out/vehicle_global_position', self.vehicle_global_position_callback)
        self.create_qos_subscription(VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback)
        self.create_qos_subscription(VehicleStatus, '/fmu/out/vehicle_status', self.vehicle_status_callback)

        # Initialize variables
        self.offboard_setpoint_counter = 0
        self.vehicle_global_position = VehicleGlobalPosition()
        self.vehicle_local_position = VehicleLocalPosition()
        self.vehicle_status = VehicleStatus()

        self.acceptable_yaw_error = math.radians(5) # Tolerancja +/-5 stopni
        self.acceptable_error = 0.5 # in meters

        # Create a timer to publish control commands
        self.timer = self.create_timer(0.5, self.timer_callback)

    def vehicle_global_position_callback(self, vehicle_global_position):
        self.vehicle_global_position = vehicle_global_position

    def vehicle_local_position_callback(self, vehicle_local_position):
        """Callback function for vehicle_local_position topic subscriber."""
        self.vehicle_local_position = vehicle_local_position

    def vehicle_status_callback(self, vehicle_status):
        """Callback function for vehicle_status topic subscriber."""
        self.vehicle_status = vehicle_status

    def arm(self):
        """Send an arm command to the vehicle."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, param1=1.0)
        # self.get_logger().info('Arm command sent')

    def disarm(self):
        """Send a disarm command to the vehicle."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, param1=0.0)
        self.get_logger().info('Disarm command sent')

    def engage_offboard_mode(self):
        """Switch to offboard mode."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_DO_SET_MODE, param1=1.0, param2=6.0)
        # self.get_logger().info("Switching to offboard mode")

    def land(self):
        """Switch to land mode."""
        self.publish_vehicle_command(VehicleCommand.VEHICLE_CMD_NAV_LAND)
        self.get_logger().info("Switching to land mode")

    def publish_offboard_control_heartbeat_signal(self):
        """Publish the offboard control mode."""
        msg = OffboardControlMode()
        msg.position = True
        msg.velocity = False
        msg.acceleration = False
        msg.attitude = False
        msg.body_rate = False
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.offboard_control_mode_publisher.publish(msg)

    def publish_position_setpoint(self, target_position: HNEDPosition):
        """Publish the trajectory setpoint."""
        msg = TrajectorySetpoint()
        msg.position = [target_position.ned.n, target_position.ned.e, target_position.ned.d]
        msg.yaw = target_position.heading
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.trajectory_setpoint_publisher.publish(msg)
        # sself.get_logger().info(f"Publishing position setpoints NED: {[target_position.ned.n, target_position.ned.e, target_position.ned.d]}\nHeading: {target_position.heading}")

    def publish_vehicle_command(self, command, **params) -> None:
        """Publish a vehicle command."""
        msg = VehicleCommand()
        msg.command = command
        msg.param1 = params.get("param1", 0.0)
        msg.param2 = params.get("param2", 0.0)
        msg.param3 = params.get("param3", 0.0)
        msg.param4 = params.get("param4", 0.0)
        msg.param5 = params.get("param5", 0.0)
        msg.param6 = params.get("param6", 0.0)
        msg.param7 = params.get("param7", 0.0)
        msg.target_system = 1
        msg.target_component = 1
        msg.source_system = 1
        msg.source_component = 1
        msg.from_external = True
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.vehicle_command_publisher.publish(msg)

    def _has_reached_goal(self, actual_x, actual_y, target_yaw):
        # self.get_logger().info("check goal reach")
        return (
            abs(self.vehicle_local_position.x - actual_x) <= self.acceptable_error
            and abs(self.vehicle_local_position.y - actual_y) <= self.acceptable_error
            and abs(self.vehicle_local_position.heading - target_yaw) <= self.acceptable_yaw_error
        )

    @abstractmethod
    def timer_callback(self) -> None:
        """Abstract method to be implemented by derived classes."""
        pass
