import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Point, PoseStamped
from std_msgs.msg import Float32
from px4_msgs.msg import TrajectorySetpoint, VehicleLocalPosition
from sese_control.utils import BaseNode
from sese_control.utils import HNEDPosition, NEDPosition
import math
from typing import List

class PX4Mock(BaseNode):
    def __init__(self):
        super().__init__('px4_mock_node')
        
        # Inicjalizacja pozycji łódki i prędkości
        self.declare_parameter('asv_start_position', [-11.5, -20.0, 0.0, 0.0])
        start_position :List[float, float, float, float] = self.get_parameter('asv_start_position').get_parameter_value().double_array_value 
        n, e, d, heading = start_position[0], start_position[1], start_position[2], start_position[3]
        self.current_position:HNEDPosition = HNEDPosition(heading=heading,ned=NEDPosition(n=n, e=e, d=d))
        self.target_position:HNEDPosition =  HNEDPosition(heading=heading,ned=NEDPosition(n=n, e=e, d=d))
        self.get_logger().info(f'{self.current_position=}')
        self.speed = 1.0  # m/s
        self.rotation_speed = math.radians(30)  # 30 degrees per second in radians
        
        # Subscriber dla trajectory setpoint
        self.create_qos_subscription(TrajectorySetpoint,'/fmu/in/trajectory_setpoint',self.update_trajectory_setpoint)
        self.vehicle_local_position_publisher = self.create_qos_publisher(VehicleLocalPosition, '/fmu/out/vehicle_local_position')
        
        # Timer co 0.05s (20Hz)
        self.dt = 0.05
        self.timer = self.create_timer(self.dt, self.timer_callback)

    def publish_vehicle_local_position(self):
        vlp_message = VehicleLocalPosition()
        vlp_message.x = self.current_position.ned.n
        vlp_message.y = self.current_position.ned.e
        vlp_message.z = self.current_position.ned.d
        vlp_message.heading = self.current_position.heading

        self.vehicle_local_position_publisher.publish(vlp_message)
        
    def update_trajectory_setpoint(self, msg):
        if not any(math.isnan(coord) for coord in msg.position):
            self.target_position =HNEDPosition(
                heading=msg.yaw,
                ned=NEDPosition(n=msg.position[0], e=msg.position[1], d=msg.position[2])
            )
            # self.get_logger().info(f'Trajectory setpoint updated: x={msg.position[0]}, y={msg.position[1]}, z={msg.position[2]}')
        else:
            self.get_logger().info('Received NaN in trajectory setpoint, ignoring update.')
        
    def normalize_angle(self, angle):
        return math.atan2(math.sin(angle), math.cos(angle))


    def timer_callback(self):
        # Oblicz wektor różnicy między aktualną a docelową pozycją
        delta_n = self.target_position.ned.n - self.current_position.ned.n
        delta_e = self.target_position.ned.e - self.current_position.ned.e
        delta_d = self.target_position.ned.d - self.current_position.ned.d

        distance = math.sqrt(delta_n**2 + delta_e**2 + delta_d**2)

        if distance > 0:
            # Oblicz przyrosty na podstawie prędkości
            step = min(self.speed * self.dt, distance)  # Prędkość * czas
            ratio = step / distance

            # Aktualizacja pozycji
            self.current_position.ned.n += delta_n * ratio
            self.current_position.ned.e += delta_e * ratio
            self.current_position.ned.d += delta_d * ratio

        # Update heading towards target
        heading_diff = self.normalize_angle(self.target_position.heading - self.current_position.heading)
        max_rotation = self.rotation_speed * self.dt  # Maximum rotation in this time step
        
        if abs(heading_diff) < max_rotation:
            self.current_position.heading = self.target_position.heading
        else:
            self.current_position.heading += max_rotation * (1 if heading_diff > 0 else -1)
            self.current_position.heading = self.normalize_angle(self.current_position.heading)

        self.publish_vehicle_local_position()



def main(args=None):
    rclpy.init(args=args)
    node = PX4Mock()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        pass
