shell-sitl:
	@xhost +
	@docker compose run --rm sitl /usr/bin/bash

build: tools/cmake-install.sh
	@docker compose build

second:
	@docker exec -it $(docker ps -q | head -n 1) bash

tools/cmake-install.sh:
	wget https://github.com/Kitware/CMake/releases/download/v3.24.1/cmake-3.24.1-Linux-x86_64.sh -O tools/cmake-install.sh
