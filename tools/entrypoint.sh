#!/bin/bash


export GZ_SIM_RESOURCE_PATH=$GZ_SIM_RESOURCE_PATH:/home/user/PX4-Autopilot/Tools/simulation/gz/models/sese_omni_boat:/home/user/PX4-Autopilot/Tools/simulation/gz/worlds/

git config --global --add safe.directory /home/user/PX4-Autopilot
git config --global --add safe.directory /home/user/PX4-Autopilot/src/modules/mavlink/mavlink
git config --global --add safe.directory /home/user/PX4-Autopilot/platforms/nuttx/NuttX/nuttx
git config --global --add safe.directory /home/user/PX4-Autopilot/Tools/simulation/gz
cd /home/user/PX4-Autopilot

source /opt/ros/humble/setup.bash
source /home/user/sese_ws/install/setup.bash

export PATH=/opt/gcc-arm-none-eabi-9-2020-q2-update/bin:$PATH


if [ -n "${LOCAL_USER_ID}" ]; then
	echo "Starting with UID : $LOCAL_USER_ID"
	# modify existing user's id
	usermod -u $LOCAL_USER_ID user
	# run as user
	chown user: /workspaces
	chown user: /ccache
	exec gosu user "$@"
else
	exec "$@"
fi

