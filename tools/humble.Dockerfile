ARG ROS_DISTRO=humble
ARG PREFIX=

FROM osrf/ros:${ROS_DISTRO}-desktop
ENV DEBIAN_FRONTEND=noninteractive

LABEL maintainer="Jakub Wilk <kubawilk63@gmail.com>"

RUN apt update
RUN apt install -y \
git \
sudo \
wget


WORKDIR /home/user/

COPY ./sese_ws/tools/cmake-install.sh /tmp/cmake-install.sh
RUN chmod u+x /tmp/cmake-install.sh \
    && mkdir /opt/cmake \
    && /tmp/cmake-install.sh --skip-license --prefix=/opt/cmake \
    && rm /tmp/cmake-install.sh \
    && ln -s /opt/cmake/bin/* /usr/local/bin

RUN git clone https://github.com/eProsima/Micro-XRCE-DDS-Agent.git
RUN mkdir /home/user/Micro-XRCE-DDS-Agent/build
WORKDIR /home/user/Micro-XRCE-DDS-Agent/build
RUN cmake /home/user/Micro-XRCE-DDS-Agent/
RUN make
RUN make install
RUN ldconfig /usr/local/lib/

ADD ../PX4-Autopilot/Tools/setup/ /home/user/setup/
RUN cd /home/user && ./setup/ubuntu.sh

RUN apt update
RUN apt install -y lsb-release wget gnupg
RUN wget https://packages.osrfoundation.org/gazebo.gpg -O /usr/share/keyrings/pkgs-osrf-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/pkgs-osrf-archive-keyring.gpg] http://packages.osrfoundation.org/gazebo/ubuntu-stable $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/gazebo-stable.list > /dev/null
RUN apt-get update
RUN apt-get install -y gz-garden

RUN apt update
RUN apt install -y \
    ros-${ROS_DISTRO}-ros2-control ros-${ROS_DISTRO}-ros2-controllers \
    ros-${ROS_DISTRO}-nav2-bringup \
    ros-${ROS_DISTRO}-navigation2 \
    ros-${ROS_DISTRO}-vision-msgs \
    ros-${ROS_DISTRO}-xacro \
    ros-${ROS_DISTRO}-joint-state-publisher \
    ros-${ROS_DISTRO}-ros-ign-bridge
RUN apt-get install -y ccache

RUN pip install -U matplotlib

COPY ./sese_ws/src/px4_msgs /home/user/sese_ws/src/px4_msgs

WORKDIR /home/user/sese_ws
RUN . /opt/ros/${ROS_DISTRO}/setup.sh && colcon build

RUN --mount=type=cache,target=/ccache \
    /bin/bash -c "source /home/user/sese_ws/install/setup.bash"

COPY ./sese_ws/tools/entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
