#!/usr/bin/env python

import rclpy
import time
import RPi.GPIO as GPIO

from rclpy.node import Node

from std_msgs.msg import Bool


OUTPUT_PIN = 23
SPLASHING_TIME_S = 10

class STM_Subscriber(Node):    
    def __init__(self):
        super().__init__('minimal_subscriber')
        self.subscription = self.create_subscription(Bool, '/stm_enable', self.subCallback, 10)
        self.ackPublisher = self.create_publisher(Bool, '/stm_ack', 10)
        
    def subCallback(self, msg):
        if msg.data == True:
            ackMsg = Bool()
            ackMsg.data = False
            self.ackPublisher.publish(ackMsg)
            GPIO.output(OUTPUT_PIN, GPIO.HIGH)
            time.sleep(SPLASHING_TIME_S)
            GPIO.output(OUTPUT_PIN, GPIO.LOW)
            ackMsg.data = True
            self.ackPublisher.publish(ackMsg)
        else:
            ackMsg = Bool()
            GPIO.output(OUTPUT_PIN, GPIO.LOW)
              
def main():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(OUTPUT_PIN, GPIO.OUT, initial=GPIO.LOW)
    
    rclpy.init()
    node = STM_Subscriber()
    rclpy.spin(node)
    
if __name__ == '__main__':
    main()
