#!/usr/bin/env python

import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
import time

class WaterCannon(Node):    
    def __init__(self):
        super().__init__('stm_publisher')
        self.publisher = self.create_publisher(Bool, '/stm_enable', 10)
        self.subscription = self.create_subscription(Bool, '/stm_ack', self.ack_callback, 10)
        self.finished = False
        self.create_timer(1.0, self.publish_true)
        
    def ack_callback(self, msg):
        if msg.data == False:
            self.get_logger().info('Start Water Delivery')
        if msg.data == True:
            self.get_logger().info('Stop Water Delivery')
            self.finished = True

    def publish_true(self):
        msg = Bool()
        msg.data = True
        self.publisher.publish(msg)

    def publish_bool(self, value):
        msg = Bool()
        msg.data = value
        self.publisher.publish(msg)

def main(args=None):
    rclpy.init(args=args)
    node = WaterCannon()
    rclpy.spin(node)
    # time.sleep(3)
    # publisher.publish_bool(False)


if __name__ == '__main__':
    main()
