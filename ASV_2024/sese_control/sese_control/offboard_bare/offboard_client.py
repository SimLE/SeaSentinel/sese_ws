import rclpy
from sese_control.action import Offboard
from rclpy.action import ActionClient
from geometry_msgs.msg import Point
from rclpy.node import Node

class DestinationSubscriber(Node):
    def __init__(self):
        super().__init__('destination_subsruber')
        self.destination_subscriber = self.create_subscription(Point, '/dest_point', self.send_destination_point, 10)
        self.action_client = None

    def send_destination_point(self, data):
        if self.action_client is None:
            action_client = OffboardActionClient()
            action_client.send_goal([float(data.x)], [float(data.z)])
        rclpy.spin(action_client)

class OffboardActionClient(Node):
    def __init__(self):
        super().__init__('offboard_client')
        self._action_client = ActionClient(self, Offboard, 'offboard')
        self.result = False
        self.get_logger().info('Executing goal...')

    def send_goal(self, x_positions: list, y_positions: list):
        goal_msg = Offboard.Goal()
        # goal_msg.x = goal_position[0]
        goal_msg.x_positions = x_positions
        goal_msg.y_positions = y_positions

        self._action_client.wait_for_server()

        self._send_goal_future = self._action_client.send_goal_async(
            goal_msg, feedback_callback=self.feedback_callback)
        self._send_goal_future.add_done_callback(self.goal_response_callback)

    def goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected')
            return

        self.get_logger().info('Goal accepted')

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)

    def get_result_callback(self, future):
        result = future.result().result
        self.get_logger().info(f'Success: {result.success}')
        if result.success:
            self.result = True
        rclpy.shutdown()

    def feedback_callback(self, future):
        feedback = future.feedback
        self.get_logger().info(
            f'Current position  x:{feedback.current_x} y:{feedback.current_y}')


def main(args=None):
    # positions = [[1.0, 10.0], [-3.0, 50.0], [-7.0, 100.0]]
    # x_positions = [position[0] for position in positions]
    # y_positions = [position[1] for position in positions]
    # print(x_positions)
    # print(y_positions)

    # for position in positions:
    rclpy.init(args=args)
    offboard_client = OffboardActionClient()
    offboard_client.send_goal()

    rclpy.spin(offboard_client)
    offboard_client.destroy_node()

if __name__ == '__main__':
    main()
