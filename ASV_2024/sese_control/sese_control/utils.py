#!/usr/bin/env python
import math
import geopy
from geopy.distance import geodesic


def ned_to_geodetic(latitude_reference, longitude_reference, altitude_reference, X_NED, Y_NED, Z_NED):
    # Współrzędne referencyjne (dla punktu zerowego NED)
    ref_coords = (latitude_reference, longitude_reference)

    # Obliczenie odległości geodezyjnej pomiędzy punktem referencyjnym a punktem docelowym
    distance = math.sqrt(X_NED**2 + Y_NED**2)

    # Obliczenie azymutu pomiędzy punktem referencyjnym a punktem docelowym w radianach
    azimuth = math.atan2(Y_NED, X_NED)

    # Przekształcenie azymutu do stopni
    azimuth_degrees = math.degrees(azimuth)

    # Obliczenie współrzędnych geodezyjnych punktu docelowego
    target_coords = geopy.distance.distance(meters=distance).destination(ref_coords, bearing=azimuth_degrees)

    # Przeliczenie wysokości relatywnej
    target_altitude = altitude_reference - Z_NED

    # Zwrócenie współrzędnych geodezyjnych
    return [target_coords[0], target_coords[1], target_altitude]



def calculate_initial_compass_bearing(pointA, pointB):
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])
    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1) * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    # Now we have the initial bearing, but math.atan2() returns values from -π to +π,
    # so we need to normalize the result to a compass bearing.
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing

def geodetic_to_ned(latitude, longitude, altitude, reference_lat, reference_lon, reference_alt):
    # Współrzędne referencyjne (dla punktu zerowego NED)
    ref_coords = (reference_lat, reference_lon)

    # Współrzędne geodezyjne punktu docelowego
    target_coords = (latitude, longitude)

    # Obliczenie azymutu pomiędzy punktem docelowym a punktem referencyjnym
    azimuth = calculate_initial_compass_bearing(ref_coords, target_coords)

    # Obliczenie odległości geodezyjnej pomiędzy punktem docelowym a punktem referencyjnym
    distance = geodesic(ref_coords, target_coords).meters

    # Obliczenie wysokości relatywnej
    relative_altitude = altitude - reference_alt

    # Obliczenie współrzędnych NED
    X_NED = distance * math.cos(math.radians(azimuth))
    Y_NED = distance * math.sin(math.radians(azimuth))
    Z_NED = -relative_altitude

    return [X_NED, Y_NED, Z_NED]

# transform distance to camera to distance in ned
def local_to_ned(x, z, ned_x, ned_y, alfa):
    c = math.sqrt(x**2+z**2)
    beta = math.atan(x/z)
    delta = alfa + beta
    new_x = c*math.cos(delta) + ned_x
    new_y = c*math.sin(delta) + ned_y
    return [new_x, new_y]
