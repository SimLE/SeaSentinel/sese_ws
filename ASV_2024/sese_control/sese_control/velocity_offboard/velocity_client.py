import rclpy
from sese_control.action import Velocity
from rclpy.action import ActionClient
from rclpy.node import Node


class OffboardActionClient(Node):
    def __init__(self):
        super().__init__('velocity_client')
        self._action_client = ActionClient(self, Velocity, 'velocity')
        self.result = False
        self.get_logger().info('Executing goal...')

    def send_goal(self, positions: list):
        goal_msg = Velocity.Goal()
        # goal_msg.x = goal_position[0]
        goal_msg.velocity = positions

        self._action_client.wait_for_server()

        self._send_goal_future = self._action_client.send_goal_async(
            goal_msg, feedback_callback=self.feedback_callback)
        self._send_goal_future.add_done_callback(self.goal_response_callback)

    def goal_response_callback(self, future):
        goal_handle = future.result()
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected')
            return

        self.get_logger().info('Goal accepted')

        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)

    def get_result_callback(self, future):
        result = future.result().result
        self.get_logger().info(f'Success: {result.success}')
        if result.success:
            self.result = True
        rclpy.shutdown()

    def feedback_callback(self, future):
        feedback = future.feedback
        self.get_logger().info(
            f'Current position  x:{feedback.current_x} y:{feedback.current_y}')


def main(args=None):
    positions = [1.0, 1.0, 0.0]

    for position in positions:
        rclpy.init(args=args)
        action_client = OffboardActionClient()
        action_client.send_goal(positions)

        rclpy.spin(action_client)
        action_client.destroy_node()
        # rclpy.shutdown()


if __name__ == '__main__':
    main()
