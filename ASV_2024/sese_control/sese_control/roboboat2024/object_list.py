
    #!/usr/bin/env python

import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleGlobalPosition, VehicleLocalPosition
import sese_control.sese_control.utils as utils
from haversine import haversine
import json
from datetime import datetime
from visualization_msgs.msg import Marker, MarkerArray
import math
from geometry_msgs.msg import Point
import os


def oblicz_odleglosc(lon1, lat1, lon2, lat2):
    # Funkcja obliczająca odległość między dwoma punktami na sferze ziemi
    pointA = (lat1,lon1)#(lat,lon)
    pointB = (lat2,lon2)
    distance = haversine(pointA, pointB) 
    return distance # Zwróć odległość w metrach

def oblicz_odleglosc2(position1_n, position1_e, position2_n, position2_e):
    # Funkcja obliczająca odległość między dwoma punktami w NED
    diff_n = position1_n - position2_n
    diff_e = position1_e - position2_e
    distance =  math.sqrt(diff_n**2 + diff_e**2)
    return distance # Zwróć odległość w metrach

class ObjectDictionary(Node):
    def __init__(self):
        super().__init__('minimal')

        # quality oif service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
    
        # create global position subscriber
        self.vehicle_global_position_subscriber = self.create_subscription(
            VehicleGlobalPosition, '/fmu/out/vehicle_global_position', self.vehicle_global_position_callback, qos_profile)
        # Create destination point subscriber
        self.destination_point_subscriber = self.create_subscription(Point, '/dest_point', self.destination_point_callback, 10)
        self.detections_subscriber = self.create_subscription(ObjectHypothesisWithPose, '/Detections', self.update_positions, 10)

        # visualization map subscriber
        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile) # NED
        
        # create publisher to visualize map (NED)
        self.marker_publisher=self.create_publisher(MarkerArray, '/visualization_marker_array', 10)
        self.pose_publisher=self.create_publisher(Marker, '/visualization_map_pose', 10)
        self.destination_publisher=self.create_publisher(Marker, '/visualization_map_dest', 10)
        
        self.positions = {
            'red_gate_buoy': [],
            'green_gate_buoy': [],
            'red_buoy': [],
            'green_buoy': [],
            'yellow_buoy': [],
            'black_buoy': [],
            'blue_buoy': []
        }
        self.detection_list_dict = [
            {
            'task_number': 0, #'task_number' from 1 to 4, gived by the main tree
            'object_class': None,
            'object_id': 0,
            'prediction_confidence': 0.0,
            'position_longitud_dl': 0.0,
            'position_latitude_szer': 0.0,
            'position_n': 0.0,
            'position_e': 0.0,
            }
        ]

        self.vehicle_global_position = None
        self.vehicle_local_position = None
        
        self.create_timer(1.0, self.marker_publish)
        self.create_timer(30.0, self.save_to)
        self.x = None
        self.y = None
        self.heading = None
        self.positions_max_len = 10
    
    def marker_publish(self):
        marker_buoy_array=MarkerArray() 
        gate_bouy_scale=2.0
        buoy_scale=2.0
        marker_scale=2.0
        
        if self.vehicle_local_position is not None:  
            local_ned_x = self.vehicle_local_position[0]
            local_ned_y = self.vehicle_local_position[1]
        else:    
            local_ned_x = 0.0
            local_ned_y = 0.0
            
        #CREATE POSE MESSAGE
        pose_msg=Marker()
        pose_msg.header.frame_id="map"
        pose_msg.action=Marker.ADD
        pose_msg.pose.position.x=local_ned_x
        pose_msg.pose.position.y=local_ned_y
        pose_msg.type=Marker.CUBE
        pose_msg.color.r=0.0
        pose_msg.color.g=0.0
        pose_msg.color.b=1.0
        pose_msg.color.a=1.0
        pose_msg.scale.x=marker_scale
        pose_msg.scale.y=marker_scale
        pose_msg.scale.z=marker_scale


        #CREATE DESTINATION MESSAGE
        destination_msg=Marker()
        destination_msg.header.frame_id="map"
        destination_msg.action=Marker.ADD
        if self.x is not None:
            destination_msg.pose.position.x=self.x
        if self.y is not None:
            destination_msg.pose.position.y=self.y
        destination_msg.type=Marker.CUBE
        destination_msg.color.r=1.0
        destination_msg.color.g=0.0
        destination_msg.color.b=1.0
        destination_msg.color.a=1.0
        destination_msg.scale.x=marker_scale
        destination_msg.scale.y=marker_scale
        destination_msg.scale.z=marker_scale

        
        for i in range(len(self.detection_list_dict)):
            if i!=0:
                marker=Marker()
                marker.header.frame_id="map"
                marker.action=Marker.ADD
                
                marker.pose.position.x=self.detection_list_dict[i]['position_n']
                marker.pose.position.y=-self.detection_list_dict[i]['position_e']
                marker.pose.position.z=0.0
                
                #marker.id=self.detection_list_dict[i]['object_id']
                marker.id=i
                transparency = 0.5
                
                
                if self.detection_list_dict[i]['object_class']=='red_gate_buoy':
                    marker.type=Marker.CUBE
                    
                    marker.scale.x=gate_bouy_scale
                    marker.scale.y=gate_bouy_scale
                    marker.scale.z=gate_bouy_scale
                    
                    marker.color.r=1.0
                    marker.color.g=0.0
                    marker.color.b=0.0
                    marker.color.a=transparency
                
                
                if self.detection_list_dict[i]['object_class']=='green_gate_buoy':
                    marker.type=Marker.CUBE

                    marker.scale.x=gate_bouy_scale
                    marker.scale.y=gate_bouy_scale
                    marker.scale.z=gate_bouy_scale
                   
                    marker.color.r=0.0
                    marker.color.g=1.0
                    marker.color.b=0.0
                    marker.color.a=transparency
                
                
                if self.detection_list_dict[i]['object_class']=='red_buoy':
                    marker.type=Marker.SPHERE
                   
                    marker.scale.x=buoy_scale
                    marker.scale.y=buoy_scale
                    marker.scale.z=buoy_scale
                     
                    marker.color.r=1.0
                    marker.color.g=0.0
                    marker.color.b=0.0
                    marker.color.a=transparency
                    
                if self.detection_list_dict[i]['object_class']=='green_buoy':
                    marker.type=Marker.SPHERE
                   
                    marker.scale.x=buoy_scale
                    marker.scale.y=buoy_scale
                    marker.scale.z=buoy_scale
                     
                    marker.color.r=0.0
                    marker.color.g=1.0
                    marker.color.b=0.0
                    marker.color.a=transparency
                    
                
                if self.detection_list_dict[i]['object_class']=='yellow_buoy':
                    marker.type=Marker.SPHERE
                   
                    marker.scale.x=buoy_scale
                    marker.scale.y=buoy_scale
                    marker.scale.z=buoy_scale
                     
                    marker.color.r=1.0
                    marker.color.g=1.0
                    marker.color.b=0.0
                    marker.color.a=transparency
                    
                    
                if self.detection_list_dict[i]['object_class']=='black_buoy':
                    marker.type=Marker.SPHERE
                   
                    marker.scale.x=buoy_scale
                    marker.scale.y=buoy_scale
                    marker.scale.z=buoy_scale
                     
                    marker.color.r=0.0
                    marker.color.g=0.0
                    marker.color.b=0.0
                    marker.color.a=transparency
                    
                    
                if self.detection_list_dict[i]['object_class']=='blue_buoy':
                    marker.type=Marker.SPHERE
                   
                    marker.scale.x=buoy_scale
                    marker.scale.y=buoy_scale
                    marker.scale.z=buoy_scale
                     
                    marker.color.r=0.0
                    marker.color.g=0.0
                    marker.color.b=1.0
                    marker.color.a=transparency
                    
                    
                marker_buoy_array.markers.append(marker)

             
        self.marker_publisher.publish(marker_buoy_array)
        self.pose_publisher.publish(pose_msg)
        self.destination_publisher.publish(destination_msg)
        
    def save_to(self):
        # Get the current date and time
        current_datetime = datetime.now()
        # Format the date and time in a filename-friendly format
        filename = current_datetime.strftime("%Y-%m-%d_%H") + '.json'
        file_path = os.path.join('..', filename)
        # Complete path with filename
        with open(file_path, 'w') as json_file:
            json.dump(self.detection_list_dict, json_file, indent=4)
        print(f'Plik JSON "{filename}" został utworzony lub nadpisany.')

    def znajdz_najwyzsze_id_dla_klasy(self, object_class):
        # Funkcja znajdująca najwyższe id dla danej klasy w liście obiektów
        najwyzsze_id = 0
        for i in range(len(self.detection_list_dict)):
            if self.detection_list_dict[i]['object_class'] == object_class and int(self.detection_list_dict[i]['object_id']) > najwyzsze_id:
                najwyzsze_id = self.detection_list_dict[i]['object_id']
        return najwyzsze_id
  
    def object_hypotesis_callback(self, data):
        position_in_ned_n = data.pose.pose.position.x #lat
        position_in_ned_e = data.pose.pose.position.y #lon
        object_score = data.score
        object_class_name = data.id

        if self.vehicle_global_position is None:
            self.vehicle_global_position = [0.0, 0.0, 0.0]

        position_latitude_szer, position_longitud_dl, _ = utils.ned_to_geodetic(self.vehicle_global_position[0], self.vehicle_global_position[1], self.vehicle_global_position[2], position_in_ned_n, position_in_ned_e, 0)

        #for loop to iteraty bt detedted objects 'object_class' and compare to 'data.id':
        # Sprawdzenie, czy obiekt danej klasy już istnieje w liście
        znaleziono = False
        indeks_z_najmniejsza_odlegloscia = None
        najmniejsza_odleglosc = float('inf')
        odleglosci = []

        for i in range(len(self.detection_list_dict)):
            if self.detection_list_dict[i]['object_class'] == object_class_name:
                znaleziono = True
                # Sprawdzenie, czy współrzędne mieszczą się w promieniu 
                odleglosc = oblicz_odleglosc2(self.detection_list_dict[i]['position_n'], self.detection_list_dict[i]['position_e'], position_in_ned_n, position_in_ned_e)
                odleglosci.append(odleglosc)
                if odleglosc < najmniejsza_odleglosc:
                    najmniejsza_odleglosc = odleglosc
                    indeks_z_najmniejsza_odlegloscia = i
        print('Najmniejsza odleglosc miedzy obiektami wynosi', najmniejsza_odleglosc)
        if najmniejsza_odleglosc <= 2: # Ustawione 2 metry
            print('Odleglosc <= 2')
            # Współrzędne mieszczą się w promieniu
            # Sprawdzenie, czy accuracy nowego wykrycia jest większe niż accuracy obiektu istniejącego
            if object_score > self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['prediction_confidence']:
                print('Wyzsza dokladnosc')
                # Jeśli tak, zaktualizuj współrzędne obiektu istniejącego
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_longitud_dl']  = position_longitud_dl
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_latitude_szer']  = position_latitude_szer
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['prediction_confidence'] = object_score
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_n'] = position_in_ned_n
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_e'] = position_in_ned_e
            else:
                print('Niższa dokladnosc')
                # Jeśli tak, zaktualizuj współrzędne obiektu istniejącego
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_longitud_dl']  = position_longitud_dl * (1-object_score) + object_score * self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_longitud_dl'] 
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_latitude_szer']  = position_latitude_szer * (1-object_score) + object_score * self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_latitude_szer']
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['prediction_confidence'] = object_score
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_n'] = position_in_ned_n * (1-object_score) + object_score * self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_n']
                self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_e'] = position_in_ned_e * (1-object_score) + object_score * self.detection_list_dict[indeks_z_najmniejsza_odlegloscia]['position_e']
        else:
            # Współrzędne nie mieszczą się w promieniu, dodaj nowy obiekt zwiększając jego id dla danej klasy
            print('Nowy obiekt')
            id_nowego_obiektu = self.znajdz_najwyzsze_id_dla_klasy(object_class_name) + 1
            self.detection_list_dict.append({
            'task_number': 1,
            'object_class': object_class_name,
            'object_id': id_nowego_obiektu,
            'prediction_confidence': object_score,
            'position_longitud_dl': position_longitud_dl, 
            'position_latitude_szer': position_latitude_szer,
            'position_n':position_in_ned_n,
            'position_e':position_in_ned_e,
            })
        # Jeśli obiekt danej klasy nie istnieje, dodaj go do listy
        if not znaleziono:
            print('Dodaje pierwszy obiekt danej klasy')
            self.detection_list_dict.append({
            'task_number': 1,
            'object_class': object_class_name,
            'object_id': 0,
            'prediction_confidence': object_score,
            'position_longitud_dl':position_longitud_dl,
            'position_latitude_szer': position_latitude_szer, 
            'position_n':position_in_ned_n,
            'position_e':position_in_ned_e,
            })
        
    def update_positions(self, data):
        # detected buoy data
        id = data.id
        score = data.score
        # dividing by 1000 to get result in meters
        x = data.pose.pose.position.x / 1000.0
        y = data.pose.pose.position.y / 1000.0
        z = data.pose.pose.position.z / 1000.0

        # updating lists if conditions are met
        if self.vehicle_local_position is not None and  self.heading is not None:
            # set position of vehicle in ned
            local_ned_x = self.vehicle_local_position[0]
            local_ned_y = self.vehicle_local_position[1]
            heading = self.heading
        else:
            # set position of vehicle in ned
            local_ned_x = 1
            local_ned_y = 1
            heading = 1
        self._update_list(self.positions[id], x, z, local_ned_x, local_ned_y, heading, id, score)

    # calculate ned positions of buoy and add to list
    def _update_list(self, positions_list, x, z, local_ned_x, local_ned_y, heading, id, score):

        position_in_ned = utils.local_to_ned(x, z, local_ned_x, local_ned_y, heading)

        if len(positions_list) < self.positions_max_len:
            positions_list.append(position_in_ned)
        else:
            positions_list.pop()
            positions_list.insert(0, position_in_ned)

        object_data = ObjectHypothesisWithPose()
        object_data.id = id
        object_data.pose.pose.position.x = position_in_ned[0]
        object_data.pose.pose.position.y = position_in_ned[1]
        object_data.pose.pose.position.z = 0.0
        object_data.score = score
        self.object_hypotesis_callback(object_data)
    
    # save global position
    def vehicle_global_position_callback(self, vehicle_global_position):
        self.vehicle_global_position = [vehicle_global_position.lat, vehicle_global_position.lon, vehicle_global_position.att]
        
    def vehicle_local_position_callback(self, vehicle_local_position):
        self.vehicle_local_position = [vehicle_local_position.x, vehicle_local_position.y, vehicle_local_position.z]
        self.heading = vehicle_local_position.heading
        
    def destination_point_callback(self, data):
        # read destination point coordinates in NED
        self.x = data.x
        self.y = data.y

def main():
    rclpy.init()
    minimal= ObjectDictionary()
    rclpy.spin(minimal)


if __name__ == '__main__':
    main()
