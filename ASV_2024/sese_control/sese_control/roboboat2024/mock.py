import rclpy
from rclpy.node import Node
from px4_msgs.msg import VehicleLocalPosition
from rclpy.qos import QoSProfile, ReliabilityPolicy, DurabilityPolicy, HistoryPolicy
import random

class ArtificialPublisher(Node):
    def __init__(self):
        super().__init__('artificial_publisher')
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        self.finished = False
        self.publisher = self.create_publisher(VehicleLocalPosition, '/fmu/out/vehicle_local_position', qos_profile)
        self.create_timer(1.0,self.publish_vehicle_local_position)

    def publish_vehicle_local_position(self):
        msg = VehicleLocalPosition()
        msg.x = random.uniform(0, 100)  # Przykładowa wartość x
        msg.y = random.uniform(0, 100)  # Przykładowa wartość y
        msg.z = random.uniform(0, 100)  # Przykładowa wartość z
        msg.heading = random.uniform(0, 360)  # Przykładowy kąt
        self.publisher.publish(msg)
        self.get_logger().info('Wysłano wiadomość VehicleLocalPosition')

def main(args=None):
    rclpy.init(args=args)
    artificial_publisher = ArtificialPublisher()
    rclpy.spin(artificial_publisher)
    artificial_publisher.destroy_node()

if __name__ == '__main__':
    main()
