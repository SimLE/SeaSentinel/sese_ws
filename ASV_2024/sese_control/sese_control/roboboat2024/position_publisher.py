
    #!/usr/bin/env python

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleLocalPosition
from geometry_msgs.msg import Point

class PositionPublisher(Node):
    def __init__(self):
        super().__init__('minimal')

        # quality oif service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
    
        self.destination_point_subscriber = self.create_subscription(Point, '/dest_point', self.destination_point_callback, 10)
        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile) # NED

        # create publisher (NED)
 
        self.create_timer(1.0, self.publish_pose_array)

        self.vehicle_local_position = None
        self.x = None
        self.y = None
        
    def vehicle_local_position_callback(self, vehicle_local_position):
        self.vehicle_local_position = [vehicle_local_position.x, vehicle_local_position.y, vehicle_local_position.z]
        self.heading = vehicle_local_position.heading
        
    def destination_point_callback(self, data):
        # read destination point coordinates in NED
        self.x = data.x
        self.y = data.y

    def publish_pose_array(self):
        if self.vehicle_local_position is not None and self.x is not None and self.y is not None:
            print('------------------------')
            print('Rybitwa:')
            print('N:',self.vehicle_local_position[0])
            print('E:',self.vehicle_local_position[1])
            print('Dest Point:')
            print('N:',self.x)
            print('E:',self.y)
            print('------------------------')

def main():
    rclpy.init()
    minimal= PositionPublisher()
    rclpy.spin(minimal)

if __name__ == '__main__':
    main()
