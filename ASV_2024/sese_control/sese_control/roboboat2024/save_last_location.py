
    #!/usr/bin/env python

import rclpy
from rclpy.node import Node
from px4_msgs.msg import VehicleLocalPosition
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy

class SaveLastLocationNode(Node):
    def __init__(self):
        super().__init__('SaveLastLocationNode')

        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        self.finished = False

        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile) # NED
        
    def vehicle_local_position_callback(self, msg):
        self.location_file = open('BlackBuoys.txt', 'w')  # Otwarcie pliku do zapisu
        self.get_logger().info('Otrzymano pozycję: x={}, y={}, z={}'.format(
            msg.x, msg.y, msg.z))
        # Zapisanie pozycji do pliku
        self.location_file.write('Pozycja: {}, {}, {}\n'.format(
            msg.x, msg.y, msg.z))
        self.location_file.close()
        self.finished = True


def main(args=None):
    rclpy.init(args=args)
    save_location_node = SaveLastLocationNode()
    rclpy.spin(save_location_node) 

if __name__ == '__main__':
    main()
