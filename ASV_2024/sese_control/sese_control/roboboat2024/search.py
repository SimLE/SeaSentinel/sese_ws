#!/usr/bin/env python
import math
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleStatus, VehicleLocalPosition, OffboardControlMode, TrajectorySetpoint, VehicleCommand
from test_msgs.msg import Arrays
from sensor_msgs.msg import Image
import time


class SearchNode(Node):
    def __init__(self, max_heading_search=45):
        super().__init__('velocity_server')
        # quality of service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )

        # Create subcribers
        self.buoys_detected_subscriber = self.create_subscription(Arrays, '/buoys_detected', self.buoys_update, 10)
        self.image_subscriber = self.create_subscription(Image, '/raw_image', self.image_callback, 10)

        self.empty_publisher = self.create_publisher(Arrays, '/empty', 10)
        # Create px4 publishers
        self.offboard_control_mode_publisher = self.create_publisher(
            OffboardControlMode, '/fmu/in/offboard_control_mode', qos_profile)
        self.velocity_setpoint_publisher = self.create_publisher(
            TrajectorySetpoint, '/fmu/in/trajectory_setpoint', qos_profile)
        self.vehicle_command_publisher = self.create_publisher(
            VehicleCommand, '/fmu/in/vehicle_command', qos_profile)

        # Create px4 subscribers
        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile)
        self.vehicle_status_subscriber = self.create_subscription(
            VehicleStatus, '/fmu/out/vehicle_status', self.vehicle_status_callback, qos_profile)

        # Initialize variables
        self.offboard_setpoint_counter = 0
        self.vehicle_local_position = VehicleLocalPosition()
        self.vehicle_status = VehicleStatus()
        self.takeoff_height = 0.0
        self.forward_distance = 5.0

        # set maximum heading and convert from degrees to radians
        self.max_heading_search = max_heading_search * math.pi / 180

        self.first_buoy_detected = False
        self.second_buoy_detected = False

        self.current_x = None
        self.current_y = None

        self.initial_heading = None

        self.finished = False

        self.feedback = 'Idle'

    def buoys_update(self, data):
        self.first_buoy_detected = data.bool_values[0]
        self.second_buoy_detected = data.bool_values[1]
        self.search()

    def image_callback(self, data):
        self.search()

    def search(self):
        if self.first_buoy_detected and self.second_buoy_detected:
            self.finished = True
            self.feedback = 'Buoys detected'
            return 
        elif self.initial_heading is not None:
            if self.first_buoy_detected and not self.second_buoy_detected:
                # turn right
                yaw_setpoint = self.initial_heading + self.max_heading_search
                self.send_velocity_setpoint(yaw_setpoint)
                self.feedback = f'{time.time()}: Turning right'
            elif not self.first_buoy_detected and self.second_buoy_detected:
                # turn left
                yaw_setpoint = self.initial_heading - self.max_heading_search
                self.send_velocity_setpoint(yaw_setpoint)
                self.feedback = f'{time.time()}: Turning left'
            else:
                # if no buoy detected turn right
                yaw_setpoint = self.initial_heading + self.max_heading_search
                self.send_velocity_setpoint(yaw_setpoint)
                self.feedback = f'{time.time()}: No buoys detected. Turning right'

    def send_velocity_setpoint(self, velocity_setpoint):
        self.publish_offboard_control_heartbeat_signal()

        if self.offboard_setpoint_counter == 10:
            self.engage_offboard_mode()
            self.arm()

        if self.vehicle_status.nav_state == VehicleStatus.NAVIGATION_STATE_OFFBOARD:
            self.publish_velocity_setpoint(
                velocity_setpoint)

        if self.offboard_setpoint_counter < 11:
            self.offboard_setpoint_counter += 1

        self.current_x = float(self.vehicle_local_position.x)
        self.current_y = float(self.vehicle_local_position.y)

    # load local position
    def vehicle_local_position_callback(self, vehicle_local_position):
        self.vehicle_local_position = vehicle_local_position
        if self.initial_heading is None:
            self.initial_heading = vehicle_local_position.heading

    def vehicle_status_callback(self, vehicle_status):
        """Callback function for vehicle_status topic subscriber."""
        self.vehicle_status = vehicle_status

    def arm(self):
        """Send an arm command to the vehicle."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, param1=1.0)
        self.get_logger().info('Arm command sent')

    def disarm(self):
        """Send a disarm command to the vehicle."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, param1=0.0)
        self.get_logger().info('Disarm command sent')

    def engage_offboard_mode(self):
        """Switch to offboard mode."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_DO_SET_MODE, param1=1.0, param2=6.0)
        self.get_logger().info("Switching to offboard mode")

    def land(self):
        """Switch to land mode."""
        self.publish_vehicle_command(VehicleCommand.VEHICLE_CMD_NAV_LAND)
        self.get_logger().info("Switching to land mode")

    def publish_offboard_control_heartbeat_signal(self):
        """Publish the offboard control mode."""
        msg = OffboardControlMode()
        msg.position = False
        msg.velocity = True
        msg.acceleration = True
        msg.attitude = False
        msg.body_rate = False
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.offboard_control_mode_publisher.publish(msg)

    def publish_velocity_setpoint(self, yaw_setpoint):
        """Publish the trajectory setpoint."""
        msg = TrajectorySetpoint()
        msg.yaw = yaw_setpoint
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.velocity_setpoint_publisher.publish(msg)

    def publish_vehicle_command(self, command, **params) -> None:
        """Publish a vehicle command."""
        msg = VehicleCommand()
        msg.command = command
        msg.param1 = params.get("param1", 0.0)
        msg.param2 = params.get("param2", 0.0)
        msg.param3 = params.get("param3", 0.0)
        msg.param4 = params.get("param4", 0.0)
        msg.param5 = params.get("param5", 0.0)
        msg.param6 = params.get("param6", 0.0)
        msg.param7 = params.get("param7", 0.0)
        msg.target_system = 1
        msg.target_component = 1
        msg.source_system = 1
        msg.source_component = 1
        msg.from_external = True
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.vehicle_command_publisher.publish(msg)
