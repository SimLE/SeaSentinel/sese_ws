#!/usr/bin/env python

import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose
from geometry_msgs.msg import Point
from std_msgs.msg import Bool
import numpy as np
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleLocalPosition
import os
import sese_control.sese_control.utils as utils
from test_msgs.msg import Arrays
from sensor_msgs.msg import Image
import time

class DockingNode(Node):
    def __init__(self, label, mode, min_distance, max_distance):
        super().__init__('minimal_publisher')

        # quality oif service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        
        # create publishers
        self.destination_point_publisher = self.create_publisher(Point, '/dest_point', 10)
        self.banner_detected_publisher = self.create_publisher(Arrays, '/buoys_detected', 10)

        self.detections_subscriber = self.create_subscription(ObjectHypothesisWithPose, '/Detections', self.update_positions, 10)
        
        # image subscription is made just for sending data about buoys being detected even when no objects are detected
        self.image_subscriber = self.create_subscription(Image, '/raw_image', self.image_callback, 10)

        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile) # NED
        # position of figure and color
        self.positions = []

        # define max and min distances for detected buoys
        self.max_distance = max_distance
        self.min_distance = min_distance

        # define labels for buoys that will be detected
        self.label = label

        # set max len for position lists
        self.position_max_len = 10

        # initialize local and global positions
        self.vehicle_local_position = None
        self.heading = 0
        self.mode = mode

        self.finished = False

        self.feedback = 'Idle'

        self.camera_resolution = 768

    # this function updates lists with positions of first and second buoy
    def update_positions(self, data):
        self.feedback = f'{time.time()}: Executing'
        # detected banner data
        id = data.id
        score = data.score
        # dividing by 1000 to get result in meters
        #x_px = data.pose.pose.position.x 
        x = data.pose.pose.position.x / 1000.0
        y = data.pose.pose.position.y / 1000.0
        z = data.pose.pose.position.z / 1000.0

        if self.mode == 'a':
            z = z - 3
        if self.mode == 'b':
            z = z - 0.3

        # updating lists if conditions are met
        if self.vehicle_local_position is not None and heading is not None:
            # set position of vehicle in ned
            local_ned_x = self.vehicle_local_position[0]
            local_ned_y = self.vehicle_local_position[1]
            heading = self.heading
            if self._verify_distance(z):
                if id == self.label:
                    self._update_list(self.positions, x, z, local_ned_x, local_ned_y, heading, id, score)
  
        # send data about whether buoys were detected or not
        self.publish_banner_detected()        
        self.send_destination() 

    # checks if destination point can be send, calculates it and publishes data
    def send_destination(self):
        # publishing destination point if lists are long enough
        if len(self.positions) == self.position_max_len:
            destination_point = self.calculate_destination()

            # create msg for publishing
            point = Point()
            point.x = destination_point[0]
            point.y = destination_point[1]
            self.destination_point_publisher.publish(point)

    # this is used only to publish data about whether buoys are detected or not
    def image_callback(self, data):
        self.publish_banner_detected()
        self.send_destination()

    # this function publishes data whether first and second buoys were detected at given time period
    def publish_banner_detected(self):
        bool_msg = Arrays()
        if len(self.positions) == self.position_max_len:
            bool_msg.bool_values[0] = True
            bool_msg.bool_values[1] = True
        else:
            bool_msg.bool_values[0] = False
            bool_msg.bool_values[1] = False
        self.banner_detected_publisher.publish(bool_msg)


    # calculate ned positions of buoy and add to list
    def _update_list(self, positions_list, x, z, local_ned_x, local_ned_y, heading, id, score):

        position_in_ned = utils.local_to_ned(x, z, local_ned_x, local_ned_y, heading)

        if len(positions_list) < self.position_max_len:
            positions_list.append(position_in_ned)
        else:
            positions_list.pop()
            positions_list.insert(0, position_in_ned)

    # calculate destination point based on mean values of collected buoys' positions 
    def calculate_destination(self):
        z_mean = sum([position[0] for position in self.positions]) / self.position_max_len
        x_mean = sum([position[1] for position in self.positions]) / self.position_max_len
        dest_z = z_mean
        dest_x = x_mean
        return dest_z, dest_x

    # save local position (NED)
    def vehicle_local_position_callback(self, vehicle_local_position):
        self.vehicle_local_position = [vehicle_local_position.x, vehicle_local_position.y, vehicle_local_position.z]
        self.heading = vehicle_local_position.heading

    # verifying if detected distance is within accepted
    def _verify_distance(self, z):
        if z > self.min_distance and z < self.max_distance:
            return True
        else:
            return False        

def main():
    rclpy.init()
    minimal_publisher = DockingNode('duck','a',1,10)
    rclpy.spin(minimal_publisher)

if __name__ == '__main__':
    main()
