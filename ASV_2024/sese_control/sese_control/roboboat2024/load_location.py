import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Point
import os

class LoadLocationNode(Node):
    def __init__(self):
        super().__init__('LoadLocationNode')

        self.destination_point_publisher = self.create_publisher(Point, '/dest_point', 10)
        
        # Wczytanie pozycji z pliku Docking.txt
        self.finished = False
        self.create_timer(1, self.load_location)    

    def load_location(self):
        if os.path.exists('Docking.txt'):
            with open('Docking.txt', 'r') as file:
                lines = file.readlines()
                for line in lines:
                    if line.startswith('Pozycja:'):
                        position = line.strip().split(':')[1].split(',')
                        destination_point = [float(pos.strip()) for pos in position]
                        self.publish_destination_point(destination_point)
        else:
            self.get_logger().warn('Plik Docking.txt nie istnieje.')

    def publish_destination_point(self, destination_point):
        point = Point()
        point.x = destination_point[0]
        point.y = destination_point[1]
        self.destination_point_publisher.publish(point)
        self.get_logger().info('Wysłano punkt docelowy: x={}, y={}'.format(point.x, point.y))
        self.finished = True
        
def main(args=None):
    rclpy.init(args=args)
    load_location_node = LoadLocationNode()
    rclpy.spin(load_location_node)
    load_location_node.destroy_node()

if __name__ == '__main__':
    main()
