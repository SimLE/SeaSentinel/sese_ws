#!/usr/bin/env python

import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose
from geometry_msgs.msg import Point
from std_msgs.msg import Bool
import numpy as np
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleLocalPosition
import os
import sese_control.sese_control.utils as utils
from test_msgs.msg import Arrays
from sensor_msgs.msg import Image
import time


class DestinationPublisher(Node):
    def __init__(self, first_label, second_label, min_distance, max_distance):
        super().__init__('destination_publisher')

        # quality oif service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        
        # create publishers
        self.destination_point_publisher = self.create_publisher(Point, '/dest_point', 10)
        self.buoys_detected_publisher = self.create_publisher(Arrays, '/buoys_detected', 10)
        
        self.detections_subscriber = self.create_subscription(ObjectHypothesisWithPose, '/Detections', self.update_positions, 10)
        
        # image subscription is made just for sending data about buoys being detected even when no objects are detected
        self.image_subscriber = self.create_subscription(Image, '/raw_image', self.image_callback, 10)

        # create global position subscriber
        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile) # NED
        
        #self.create_timer(1, self.send_destination)

        # lists for positions of two buoys
        self.positions_1 = []
        self.positions_2 = []

        # define max and min distances for detected buoys
        self.max_distance = max_distance
        self.min_distance = min_distance

        # define labels for buoys that will be detected
        self.first_label = first_label
        self.second_label = second_label
        
        # set max len for position lists
        self.positions_max_len = 10

        # initialize local and global positions
        self.vehicle_local_position = None
        self.heading = None

        self.finished = False

        self.feedback = 'Idle'

        self.camera_resolution = 768

    # this function updates lists with positions of first and second buoy
    def update_positions(self, data):
        self.feedback = f'{time.time()}: Executing'
        # detected buoy data
        id = data.id
        score = data.score
        # ### TODO
        # self.vehicle_local_position = [0.0,0.0,0.0]
        # self.heading = 0.0

        # dividing by 1000 to get result in meters
        x = data.pose.pose.position.x / 1000.0
        y = data.pose.pose.position.y / 1000.0
        z = data.pose.pose.position.z / 1000.0

        # updating lists if conditions are met
        if self.vehicle_local_position is not None and  self.heading is not None:
            # set position of vehicle in ned
            local_ned_x = self.vehicle_local_position[0]
            local_ned_y = self.vehicle_local_position[1]
            heading = self.heading
            #print('duck1')
            if self._verify_distance(z):
                #print('duck2')
                if id == self.first_label:
                    self._update_list(self.positions_1, x, z, local_ned_x, local_ned_y, heading, id, score)
                elif id == self.second_label:
                    self._update_list(self.positions_2, x, z, local_ned_x, local_ned_y, heading, id, score)

        # send data about whether buoys were detected or not
        self.publish_buoys_detected()        
        self.send_destination() 

    # checks if destination point can be send, calculates it and publishes data
    def send_destination(self):
        print('calculate destination')
        # publishing destination point if lists are long enough
        if len(self.positions_1) == self.positions_max_len and len(self.positions_2) == self.positions_max_len:
            destination_point = self.calculate_destination()
            # create msg for publishing
            point = Point()
            point.x = destination_point[0]
            point.y = destination_point[1]
            self.destination_point_publisher.publish(point)

    # this is used only to publish data about whether buoys are detected or not
    def image_callback(self, data):
        self.publish_buoys_detected()
        self.send_destination()

    # this function publishes data whether first and second buoys were detected at given time period
    def publish_buoys_detected(self):
        bool_msg = Arrays()
        if len(self.positions_1) == self.positions_max_len:
            bool_msg.bool_values[0] = True
        else:
            bool_msg.bool_values[0] = False
        if len(self.positions_2) == self.positions_max_len:
            bool_msg.bool_values[1] = True
        else:
            bool_msg.bool_values[1] = False
        self.buoys_detected_publisher.publish(bool_msg)


    # calculate ned positions of buoy and add to list
    def _update_list(self, positions_list, x, z, local_ned_x, local_ned_y, heading, id, score):

        position_in_ned = utils.local_to_ned(x, z, local_ned_x, local_ned_y, heading)

        if len(positions_list) < self.positions_max_len:
            positions_list.append(position_in_ned)
        else:
            positions_list.pop()
            positions_list.insert(0, position_in_ned)

    # calculate destination point based on mean values of collected buoys' positions 
    def calculate_destination(self):
        z1_mean = sum([position[0] for position in self.positions_1]) / self.positions_max_len
        x1_mean = sum([position[1] for position in self.positions_1]) / self.positions_max_len
        z2_mean = sum([position[0] for position in self.positions_2]) / self.positions_max_len
        x2_mean = sum([position[1] for position in self.positions_2]) / self.positions_max_len

        dest_z = (z1_mean + z2_mean) / 2
        dest_x = (x1_mean + x2_mean) / 2
        print('destination send')
        # print(dest_z, dest_x)
        
        return dest_z, dest_x

    # save local position (NED)
    def vehicle_local_position_callback(self, vehicle_local_position):
        self.vehicle_local_position = [vehicle_local_position.x, vehicle_local_position.y, vehicle_local_position.z]
        self.heading = vehicle_local_position.heading

    # verifying if detected distance is within accepted
    def _verify_distance(self, z):
        if z > self.min_distance and z < self.max_distance:
            return True
        else:
            return False
        
def main():
    rclpy.init()
    minimal_publisher = DestinationPublisher('red_buoy', 'green_buoy', 1, 10)
    rclpy.spin(minimal_publisher)

if __name__ == '__main__':
    main()
