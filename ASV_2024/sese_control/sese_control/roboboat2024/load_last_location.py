'''
This file is used to calculate less precise destination point when reaching home position. 
Home gate pose is used to calculate more accurate ones when the boat is closer to the last black buoys
'''

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Point
import os

class LoadLastLocationNode(Node):
    def __init__(self):
        super().__init__('LoadLastLocationNode')

        self.destination_point_publisher = self.create_publisher(Point, '/last_dest_point', 10)
        
        # Wczytanie pozycji z pliku BlackBuoys.txt
        self.finished = False
        self.create_timer(1, self.load_location)       

    def load_location(self):
        if os.path.exists('BlackBuoys.txt'):
            with open('BlackBuoys.txt', 'r') as file:
                lines = file.readlines()
                for line in lines:
                    if line.startswith('Pozycja:'):
                        position = line.strip().split(':')[1].split(',')
                        destination_point = [float(pos.strip()) for pos in position]
                        self.publish_destination_point(destination_point)
        else:
            self.get_logger().warn('Plik BlackBuoys.txt nie istnieje.')

    def publish_destination_point(self, destination_point):
        point = Point()
        point.x = destination_point[0]
        point.y = destination_point[1]
        self.destination_point_publisher.publish(point)
        self.get_logger().info('Wysłano punkt docelowy: x={}, y={}'.format(point.x, point.y))
        self.finished = True

def main(args=None):
    rclpy.init(args=args)
    load_location_node = LoadLastLocationNode()
    rclpy.spin(load_location_node)
    load_location_node.destroy_node()

if __name__ == '__main__':
    main()
