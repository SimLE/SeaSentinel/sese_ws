from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import OffboardControlMode, TrajectorySetpoint, VehicleCommand, VehicleLocalPosition, VehicleStatus, VehicleThrustSetpoint
from geometry_msgs.msg import Point
from sensor_msgs.msg import Image
import rclpy
from test_msgs.msg import Arrays
import time


class ThrustSearch(Node):
    def __init__(self):
        super().__init__('thrust_test')

        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )

        # Create destination point subscriber
        self.destination_point_subscriber = self.create_subscription(Image, '/raw_image', self.image_callback, 10)
        self.buoys_detected_subscriber = self.create_subscription(Arrays, '/buoys_detected', self.buoys_update, 10)

        # Create px4 publishers
        self.offboard_control_mode_publisher = self.create_publisher(
            OffboardControlMode, '/fmu/in/offboard_control_mode', qos_profile)
        self.trajectory_setpoint_publisher = self.create_publisher(
            TrajectorySetpoint, '/fmu/in/trajectory_setpoint', qos_profile)
        self.vehicle_command_publisher = self.create_publisher(
            VehicleCommand, '/fmu/in/vehicle_command', qos_profile)
        self.vehicle_command_publisher = self.create_publisher(
            VehicleCommand, '/fmu/in/vehicle_command', qos_profile)
        self.vehicle_thrust_publisher = self.create_publisher(
            VehicleThrustSetpoint, '/fmu/in/vehicle_thrust_setpoint', qos_profile)

        # Create px4 subscribers
        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile)
        self.vehicle_status_subscriber = self.create_subscription(
            VehicleStatus, '/fmu/out/vehicle_status', self.vehicle_status_callback, qos_profile)

        # Initialize variables
        self.offboard_setpoint_counter = 0
        self.vehicle_local_position = VehicleLocalPosition()
        self.vehicle_status = VehicleStatus()
        self.takeoff_height = 0.0
        self.forward_distance = 5.0

        self.i = 0

        self.finished = False

        self.first_buoy_detected = False
        self.second_buoy_detected = False

        self.acceptable_error = 0.5

        self.feedback = 'Idle'

        # timer_period = 0.05 # seconds
        # self.timer = self.create_timer(timer_period, self.execute_goal)
        # self.i = 0

    def buoys_update(self, data):
        self.first_buoy_detected = data.bool_values[0]
        self.second_buoy_detected = data.bool_values[1]
        self.execute_goal()

    def image_callback(self, data):
        self.execute_goal()

    def execute_goal(self):
        # read destination point coordinates in NED
        # x = data.x
        # y = data.y
        # if self.feedback == 'Idle':
        self.feedback = f'{time.time()}: Executing'

        """Callback function for the timer."""
        self.publish_offboard_control_heartbeat_signal()

        if self.offboard_setpoint_counter == 10:
            self.engage_offboard_mode()
            self.arm()

        if self.first_buoy_detected and self.second_buoy_detected:
            self.finished = True
            self.feedback = 'Buoys detected'
            return 
        elif self.vehicle_status.nav_state == VehicleStatus.NAVIGATION_STATE_OFFBOARD:
            self.feedback = 'Spinning'
            self.publish_position_setpoint(
                10., 00.0, 0.0)

        if self.offboard_setpoint_counter < 11:
            self.offboard_setpoint_counter += 1

            
    def vehicle_local_position_callback(self, vehicle_local_position):
        """Callback function for vehicle_local_position topic subscriber."""
        # print('Local position saved')
        self.vehicle_local_position = vehicle_local_position

    def vehicle_status_callback(self, vehicle_status):
        """Callback function for vehicle_status topic subscriber."""
        self.vehicle_status = vehicle_status

    def arm(self):
        """Send an arm command to the vehicle."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, param1=1.0)
        self.get_logger().info('Arm command sent')

    def disarm(self):
        """Send a disarm command to the vehicle."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM, param1=0.0)
        self.get_logger().info('Disarm command sent')

    def engage_offboard_mode(self):
        """Switch to offboard mode."""
        self.publish_vehicle_command(
            VehicleCommand.VEHICLE_CMD_DO_SET_MODE, param1=1.0, param2=6.0)
        self.get_logger().info("Switching to offboard mode")

    def land(self):
        """Switch to land mode."""
        self.publish_vehicle_command(VehicleCommand.VEHICLE_CMD_NAV_LAND)
        self.get_logger().info("Switching to land mode")

    def publish_offboard_control_heartbeat_signal(self):
        """Publish the offboard control mode."""
        msg = OffboardControlMode()
        msg.position = False
        msg.velocity = False
        msg.acceleration = False
        msg.attitude = False
        msg.body_rate = False
        msg.thrust_and_torque = True
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.offboard_control_mode_publisher.publish(msg)

    def publish_position_setpoint(self, x: float, y: float, z: float):
        """Publish the trajectory setpoint."""
        print('Thrust set')
        msg = VehicleThrustSetpoint()
        msg.xyz = [x, y, z]
        # msg.yaw = 1.57079 * 1 # (90 degree)
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.vehicle_thrust_publisher.publish(msg)
        # self.get_logger().info(f"Publishing position setpoints {msg.position}")
        # self.get_logger().info(
        #     f'Local position: {self.vehicle_local_position.x}, {self.vehicle_local_position.y}, {self.vehicle_local_position.z}')

    def publish_vehicle_command(self, command, **params) -> None:
        """Publish a vehicle command."""
        msg = VehicleCommand()
        msg.command = command
        msg.param1 = params.get("param1", 0.0)
        msg.param2 = params.get("param2", 0.0)
        msg.param3 = params.get("param3", 0.0)
        msg.param4 = params.get("param4", 0.0)
        msg.param5 = params.get("param5", 0.0)
        msg.param6 = params.get("param6", 0.0)
        msg.param7 = params.get("param7", 0.0)
        msg.target_system = 1
        msg.target_component = 1
        msg.source_system = 1
        msg.source_component = 1
        msg.from_external = True
        msg.timestamp = int(self.get_clock().now().nanoseconds / 1000)
        self.vehicle_command_publisher.publish(msg)

def main():
    rclpy.init()
    thrust_search = ThrustSearch()
    rclpy.spin(thrust_search)
    rclpy.shutdown()

if __name__ == '__main__':
    main()
