import rclpy
from sese_control.action import Offboard
from rclpy.action import ActionClient
from rclpy.node import Node
import os
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleGlobalPosition, VehicleLocalPosition
from geopy.distance import geodesic
import geopy.distance
import math
from geopy.distance import geodesic
import csv


def ned_to_geodetic(latitude_reference, longitude_reference, altitude_reference, X_NED, Y_NED, Z_NED):
    # Współrzędne referencyjne (dla punktu zerowego NED)
    ref_coords = (latitude_reference, longitude_reference)

    # Obliczenie odległości geodezyjnej pomiędzy punktem referencyjnym a punktem docelowym
    distance = math.sqrt(X_NED**2 + Y_NED**2)

    # Obliczenie azymutu pomiędzy punktem referencyjnym a punktem docelowym w radianach
    azimuth = math.atan2(Y_NED, X_NED)

    # Przekształcenie azymutu do stopni
    azimuth_degrees = math.degrees(azimuth)

    # Obliczenie współrzędnych geodezyjnych punktu docelowego
    target_coords = geopy.distance.distance(meters=distance).destination(ref_coords, bearing=azimuth_degrees)

    # Przeliczenie wysokości relatywnej
    target_altitude = altitude_reference - Z_NED

    # Zwrócenie współrzędnych geodezyjnych
    return [target_coords[0], target_coords[1], target_altitude]



def calculate_initial_compass_bearing(pointA, pointB):
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])

    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1) * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    # Now we have the initial bearing, but math.atan2() returns values from -π to +π,
    # so we need to normalize the result to a compass bearing.
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing

def geodetic_to_ned(latitude, longitude, altitude, reference_lat, reference_lon, reference_alt):
    # Współrzędne referencyjne (dla punktu zerowego NED)
    ref_coords = (reference_lat, reference_lon)

    # Współrzędne geodezyjne punktu docelowego
    target_coords = (latitude, longitude)

    # Obliczenie azymutu pomiędzy punktem docelowym a punktem referencyjnym
    azimuth = calculate_initial_compass_bearing(ref_coords, target_coords)

    # Obliczenie odległości geodezyjnej pomiędzy punktem docelowym a punktem referencyjnym
    distance = geodesic(ref_coords, target_coords).meters

    # Obliczenie wysokości relatywnej
    relative_altitude = altitude - reference_alt

    # Obliczenie współrzędnych NED
    X_NED = distance * math.cos(math.radians(azimuth))
    Y_NED = distance * math.sin(math.radians(azimuth))
    Z_NED = -relative_altitude

    return [X_NED, Y_NED, Z_NED]

class PositionSubscriber(Node):
    def __init__(self):
        super().__init__('position_subscriber')

        # quality of service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        
        # subsribe to global and local position
        self.vehicle_global_position_subscriber = self.create_subscription(
            VehicleGlobalPosition, '/fmu/out/vehicle_global_position', self.vehicle_global_position_callback, qos_profile)
        self.vehicle_local_position_subscriber = self.create_subscription(
            VehicleLocalPosition, '/fmu/out/vehicle_local_position', self.vehicle_local_position_callback, qos_profile)

        # initialize positions to None
        self.vehicle_global_position = None
        self.vehicle_local_position = None
        
    # read global and local positions
    def vehicle_global_position_callback(self, vehicle_global_position):
        self.vehicle_global_position = [vehicle_global_position.lat, vehicle_global_position.lon, vehicle_global_position.alt]

    def vehicle_local_position_callback(self, vehicle_local_position):
        self.vehicle_local_position = [vehicle_local_position.x, vehicle_local_position.y, vehicle_local_position.z]



class OffboardActionClient(Node):
    def __init__(self):
        super().__init__('offboard_client')

        self._action_client = ActionClient(self, Offboard, 'offboard')
        # result not completed
        self.result = False
        self.get_logger().info('Executing goal...')

    def send_goal(self, x_positions: list, y_positions: list):
        # Create goal msg based on action msg
        goal_msg = Offboard.Goal()

        # set goal x and y positions
        goal_msg.x_positions = x_positions
        goal_msg.y_positions = y_positions

        # wait for server that executes it
        self._action_client.wait_for_server()

        # send goal and add callback
        self._send_goal_future = self._action_client.send_goal_async(
            goal_msg, feedback_callback=self.feedback_callback)
        self._send_goal_future.add_done_callback(self.goal_response_callback)

    def goal_response_callback(self, future):
        goal_handle = future.result()

        # callback for accepting and rejecting goal
        if not goal_handle.accepted:
            self.get_logger().info('Goal rejected')
            return
        self.get_logger().info('Goal accepted')

        # get result
        self._get_result_future = goal_handle.get_result_async()
        self._get_result_future.add_done_callback(self.get_result_callback)

    def get_result_callback(self, future):
        result = future.result().result
        # If result is success print log msg and change self.result flag
        self.get_logger().info(f'Success: {result.success}')
        if result.success:
            self.result = True
        rclpy.shutdown()

    def feedback_callback(self, future):
        feedback = future.feedback

        # print current position feedback
        self.get_logger().info(
            f'Current position  x:{feedback.current_x} y:{feedback.current_y}')


def main(args=None):
    rclpy.init(args=args)

    # create subscribers
    position_subscriber = PositionSubscriber()
    offboard_client = OffboardActionClient()

    # file path where csv file is stored
    file_path = os.path.join('../..', 'positions', 'positions.csv')

    # read goal positions
    with open(file_path, "r") as f:
        csv_reader = csv.reader(f, delimiter=',')
        gps_positions = [[float(row[0]), float(row[1]), float(row[2])] for row in csv_reader]

    # wait until global and local positions are read
    while position_subscriber.vehicle_global_position == None or position_subscriber.vehicle_local_position == None:
        rclpy.spin_once(position_subscriber)

    # get current position
    current_lat = position_subscriber.vehicle_global_position[0]
    current_lon = position_subscriber.vehicle_global_position[1]
    current_alt = position_subscriber.vehicle_global_position[2]
    current_ned_x = position_subscriber.vehicle_local_position[0]
    current_ned_y = position_subscriber.vehicle_local_position[1]
    current_ned_z = position_subscriber.vehicle_local_position[2]

    # calculate reference gps points
    reference_lat, reference_lon, refernece_alt = ned_to_geodetic(current_lat, current_lon, current_alt, 
                                                                  -current_ned_x, -current_ned_y, -current_ned_z)
    # calculate destination ned positions
    goal_ned_positions = [geodetic_to_ned(position[0], position[1], position[2], reference_lat, reference_lon, refernece_alt) for position in gps_positions]

    # create x and y positions for offboard_client
    x_positions = [position[0] for position in goal_ned_positions]
    y_positions = [position[1] for position in goal_ned_positions]

    # send goal
    offboard_client.send_goal(x_positions, y_positions)

    rclpy.spin(offboard_client)
    position_subscriber.destroy_node()
    offboard_client.destroy_node()

if __name__ == '__main__':
    main()
