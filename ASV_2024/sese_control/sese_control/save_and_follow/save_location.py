from pynput import keyboard
import os
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, DurabilityPolicy
from px4_msgs.msg import VehicleGlobalPosition
import csv
import argparse



class PositionSubscriber(Node):
    def __init__(self, overwrite):
        super().__init__('position_saver')

        # quality oif service
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            durability=DurabilityPolicy.TRANSIENT_LOCAL,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        
        # create global position subscriber
        self.vehicle_global_position_subscriber = self.create_subscription(
            VehicleGlobalPosition, 'fmu/out/vehicle_global_position', self.vehicle_global_position_callback, qos_profile)
        
        # create keyboard listener
        self.listener = keyboard.Listener(on_release=self.save_position)

        # file path where positions will be stored
        self.file_path = os.path.join('../..', 'positions', 'positions.csv')

        if overwrite:
            if os.path.isfile(self.file_path):
                os.remove(self.file_path)

    # save global position
    def vehicle_global_position_callback(self, vehicle_global_position):
        self.vehicle_global_position = vehicle_global_position

    # save global position to file
    def save_position(self, key):
        # save only when space key is pressed
        if key == keyboard.Key.space:
            try:
                positions = [self.vehicle_global_position.lat, self.vehicle_global_position.lon, self.vehicle_global_position.alt]

                with open(self.file_path, 'a+') as file:
                    writer = csv.writer(file)
                    writer.writerow(positions)
                print('Position saved\n')
            except AttributeError:
                print("Global position can't be read. Not saving\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--overwrite', help="Overwrite existing positions.csv file", type=int)
    args = parser.parse_args()

    if args.overwrite not in [0, 1]:
        raise ValueError('Overwrite value is not boolean')

    rclpy.init(args=None)

    # create position subscriber that reads location and saves it to file
    position_subscriber = PositionSubscriber(overwrite=bool(args.overwrite))
    
    # start keyboard listening 
    position_subscriber.listener.start()
    rclpy.spin(position_subscriber)

if __name__ == '__main__':
    main()