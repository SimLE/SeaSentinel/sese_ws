#!/usr/bin/env python
import time
import py_trees
import atexit
import multiprocessing
import multiprocessing.connection
import py_trees.common
import rclpy
from sese_bt import utils
from sese_control.sese_control.roboboat2024.destination_publisher import DestinationPublisher
from sese_control.sese_control.roboboat2024.thrust_search import ThrustSearch
from sese_control.sese_control.roboboat2024.go_to_goal import GoToGoalPublisher


class WaitAction(py_trees.behaviour.Behaviour):
    def __init__(self, name: str):
        super(WaitAction, self).__init__(name)
        self.logger.debug("%s.__init__()" % (self.__class__.__name__))
        self.counter = 0

    def setup(self, **kwargs: int) -> None:
        """Kickstart the separate process this behaviour will work with.

        Ordinarily this process will be already running. In this case,
        setup is usually just responsible for verifying it exists.
        """
        self.logger.debug(
            "%s.setup()->connections to an external process" % (self.__class__.__name__)
        )
        self.parent_connection, self.child_connection = multiprocessing.Pipe()
        self.planning = multiprocessing.Process(
            target=utils.planning, args=(self.child_connection,)
        )
        atexit.register(self.planning.terminate)
        self.planning.start()

    def initialise(self) -> None:
        """Reset a counter variable."""
        self.logger.debug(
            "%s.initialise()->sending new goal" % (self.__class__.__name__)
        )
        self.parent_connection.send(["new goal"])
        self.percentage_completion = 0

    def update(self) -> py_trees.common.Status:
        """Increment the counter, monitor and decide on a new status."""
        new_status = py_trees.common.Status.RUNNING
        self.counter += 1
        if self.counter == 50:
            new_status = py_trees.common.Status.SUCCESS
        try:
            self.feedback_message = self.node.feedback
        except:
            pass
        if new_status == py_trees.common.Status.SUCCESS:
            self.feedback_message = "Processing finished"
            self.logger.debug(
                "%s.update()[%s->%s][%s]"
                % (
                    self.__class__.__name__,
                    self.status,
                    new_status,
                    self.feedback_message,
                )
            )
        else:
            self.logger.debug(
                "%s.update()[%s][%s]"
                % (self.__class__.__name__, self.status, self.feedback_message)
            )
        return new_status

    def terminate(self, new_status: py_trees.common.Status) -> None:
        """Nothing to clean up in this example."""
        self.logger.debug(
            "%s.terminate()[%s->%s]"
            % (self.__class__.__name__, self.status, new_status)
        )

class Action(py_trees.behaviour.Behaviour):
    def __init__(self, name: str, node, limit_time=False):
        super(Action, self).__init__(name)
        self.logger.debug("%s.__init__()" % (self.__class__.__name__))
        self.node = node

        # limit time is used to stop rclpy.spin_once() from blocking all processes when no topic is read
        self.limit_time = limit_time

    def setup(self, **kwargs: int) -> None:
        """Kickstart the separate process this behaviour will work with.

        Ordinarily this process will be already running. In this case,
        setup is usually just responsible for verifying it exists.
        """
        self.logger.debug(
            "%s.setup()->connections to an external process" % (self.__class__.__name__)
        )
        self.parent_connection, self.child_connection = multiprocessing.Pipe()
        self.planning = multiprocessing.Process(
            target=utils.planning, args=(self.child_connection,)
        )
        atexit.register(self.planning.terminate)
        self.planning.start()

    def initialise(self) -> None:
        """Reset a counter variable."""
        self.logger.debug(
            "%s.initialise()->sending new goal" % (self.__class__.__name__)
        )
        self.parent_connection.send(["new goal"])
        self.percentage_completion = 0

    def update(self) -> py_trees.common.Status:
        # if self.limit_time:
        #     rclpy.spin_once(self.node, timeout_sec=0.1)
        # else:
        rclpy.spin_once(self.node)
        rclpy.spin_once(self.node)
        rclpy.spin_once(self.node)
        rclpy.spin_once(self.node)
        rclpy.spin_once(self.node)


        """Increment the counter, monitor and decide on a new status."""
        new_status = py_trees.common.Status.RUNNING
        # if node is finished set success
        if self.node.finished:
            new_status = py_trees.common.Status.SUCCESS

        try:
            self.feedback_message = self.node.feedback
        except:
            pass
        if new_status == py_trees.common.Status.SUCCESS:
            self.feedback_message = "Processing finished"
            self.logger.debug(
                "%s.update()[%s->%s][%s]"
                % (
                    self.__class__.__name__,
                    self.status,
                    new_status,
                    self.feedback_message,
                )
            )
        else:
            self.logger.debug(
                "%s.update()[%s][%s]"
                % (self.__class__.__name__, self.status, self.feedback_message)
            )
        return new_status

    def terminate(self, new_status: py_trees.common.Status) -> None:
        """Nothing to clean up in this example."""
        self.logger.debug(
            "%s.terminate()[%s->%s]"
            % (self.__class__.__name__, self.status, new_status)
        )


# main
def main() -> None:
    rclpy.init()
    """Entry point for the demo script."""
    # terminal_args = utils.command_line_argument_parser().parse_args()

    py_trees.logging.level = py_trees.logging.Level.DEBUG

    # actions need to be copied because they can't be placed in two different sequences

    # publishes destination points. Names of buoys should be changed (currently only for testing)
    calculate_destination_1 = Action("Calculating destination point 1", DestinationPublisher('red_buoy', 'green_buoy', 1, 20))
    calculate_destination_2 = Action("Calculating destination point 2", DestinationPublisher('red_buoy', 'green_buoy', 3, 10))
    calculate_destination_3 = Action("Calculating destination point 3", DestinationPublisher('red_buoy', 'green_buoy', 3, 10))
    calculate_destination_4 = Action("Calculating destination point 4", DestinationPublisher('red_buoy', 'green_buoy', 3, 10))


    # reach_goal
    reaching_goal_1 = Action("Reaching first goal", GoToGoalPublisher())
    reaching_goal_2 = Action("Reaching second goal", GoToGoalPublisher())
    reaching_goal_3 = Action("Reaching third goal", GoToGoalPublisher())
    reaching_goal_4 = Action("Reaching fourth goal", GoToGoalPublisher())
    search_1 = Action("Searching first gate buoy", ThrustSearch(), limit_time=True)
    search_2 = Action("Searching second gate buoy", ThrustSearch(), limit_time=True)
    search_3 = Action("Searching third gate buoy", ThrustSearch(), limit_time=True)
    search_4 = Action("Searching fourth gate buoy", ThrustSearch(), limit_time=True)

    # wait actions
    wait2 = WaitAction("Wait action")
    wait3 = WaitAction("Wait action")
    wait4 = WaitAction("Wait action")

    
    # create separate sequences for each gate that consists of searching for buoys and reaching destination point
    part_1_seq = utils.create_sequence_child([search_1, reaching_goal_1], "Searching and reaching first gate")
    part_2_seq = utils.create_sequence_child([wait2, search_2, reaching_goal_2], "Searching and reaching second gate")
    part_3_seq = utils.create_sequence_child([wait3, search_3, reaching_goal_3], "Searching and reaching third gate")
    part_4_seq = utils.create_sequence_child([wait4, search_4, reaching_goal_4], "Searching and reaching fourth gate")

    # parallel work of calculating destination points, searching and reaching
    part_1_parallel = utils.create_parallel_child([calculate_destination_1, part_1_seq], "Calculate and reach first gate")
    part_2_parallel = utils.create_parallel_child([calculate_destination_2, part_2_seq], "Calculate and reach second gate")
    part_3_parallel = utils.create_parallel_child([calculate_destination_3, part_4_seq], "Calculate and reach third gate")
    part_4_parallel = utils.create_parallel_child([calculate_destination_4, part_3_seq], "Calculate and reach fourth gate")

    # create tree root
    root = utils.create_sequence_child([part_1_parallel, part_2_parallel, part_3_parallel, part_4_parallel], "Root")

    root.setup_with_descendants()
    while True:
        try:
            root.tick_once()
            print("\n")
            print(py_trees.display.unicode_tree(root=root, show_status=True))
            time.sleep(0.05)
            if root.status == py_trees.common.Status.SUCCESS:
                break
        except KeyboardInterrupt:
            break
    print("\n")

if __name__ == '__main__':
    main()
