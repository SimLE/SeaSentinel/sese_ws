#!/usr/bin/env python
import time
import py_trees
import atexit
import multiprocessing
import multiprocessing.connection
import py_trees.common
import rclpy
from sese_bt import utils
from sese_control.sese_control.roboboat2024.docking_publisher import DockingNode
from sese_control.sese_control.roboboat2024.thrust_search import ThrustSearch
from sese_control.sese_control.roboboat2024.go_to_goal import GoToGoalPublisher
from sese_control.sese_control.roboboat2024.save_location import SaveLocationNode
from sese_control.sese_control.roboboat2024.load_location import LoadLocationNode 


class Action(py_trees.behaviour.Behaviour):
    def __init__(self, name: str, node, limit_time=False):
        super(Action, self).__init__(name)
        self.logger.debug("%s.__init__()" % (self.__class__.__name__))
        self.node = node

        # limit time is used to stop rclpy.spin_once() from blocking all processes when no topic is read
        self.limit_time = limit_time

    def setup(self, **kwargs: int) -> None:
        """Kickstart the separate process this behaviour will work with.

        Ordinarily this process will be already running. In this case,
        setup is usually just responsible for verifying it exists.
        """
        self.logger.debug(
            "%s.setup()->connections to an external process" % (self.__class__.__name__)
        )
        self.parent_connection, self.child_connection = multiprocessing.Pipe()
        self.planning = multiprocessing.Process(
            target=utils.planning, args=(self.child_connection,)
        )
        atexit.register(self.planning.terminate)
        self.planning.start()

    def initialise(self) -> None:
        """Reset a counter variable."""
        self.logger.debug(
            "%s.initialise()->sending new goal" % (self.__class__.__name__)
        )
        self.parent_connection.send(["new goal"])
        self.percentage_completion = 0

    def update(self) -> py_trees.common.Status:
        if self.limit_time:
            rclpy.spin_once(self.node, timeout_sec=0.1)
        else:
            rclpy.spin_once(self.node)
        """Increment the counter, monitor and decide on a new status."""
        new_status = py_trees.common.Status.RUNNING
        # if node is finished set success
        if self.node.finished:
            new_status = py_trees.common.Status.SUCCESS

        try:
            self.feedback_message = self.node.feedback
        except:
            pass
        if new_status == py_trees.common.Status.SUCCESS:
            self.feedback_message = "Processing finished"
            self.logger.debug(
                "%s.update()[%s->%s][%s]"
                % (
                    self.__class__.__name__,
                    self.status,
                    new_status,
                    self.feedback_message,
                )
            )
        else:
            self.logger.debug(
                "%s.update()[%s][%s]"
                % (self.__class__.__name__, self.status, self.feedback_message)
            )
        return new_status

    def terminate(self, new_status: py_trees.common.Status) -> None:
        """Nothing to clean up in this example."""
        self.logger.debug(
            "%s.terminate()[%s->%s]"
            % (self.__class__.__name__, self.status, new_status)
        )


# main
def main() -> None:
    rclpy.init()
    """Entry point for the demo script."""
    # terminal_args = utils.command_line_argument_parser().parse_args()

    py_trees.logging.level = py_trees.logging.Level.DEBUG

    figures_and_color = [
        'blue_circle',
        'green_circle',
        'red_circle',
        'blue_triangle',
        'green_triangle',
        'red_triangle',
        'blue_square',
        'green_square',
        'red_square',
        'blue_plus',
        'green_plus',
        'red_plus',
        'duck',
    ]

    index = 2 #Select the index of the assigned object
    assigned_shape = figures_and_color[index]

    # publishes destination points. Names of buoys should be changed (currently only for testing)
    save_starting_position = Action("Save task starting position", SaveLocationNode())
    load_starting_position = Action("Save task starting position", LoadLocationNode())
    calculate_destination_1a = Action("Calculating destination point", DockingNode(assigned_shape,'a',1,20))
    calculate_destination_1b = Action("Calculating destination point", DockingNode(assigned_shape,'b',1,10))
    # reach_goal
    reaching_goal_1a = Action("Maneuver into an assigned docking bay", GoToGoalPublisher())
    reaching_goal_1b = Action("Maneuver into an assigned docking bay", GoToGoalPublisher())
    reaching_goal_2 = Action("Undocking", GoToGoalPublisher())
    search = Action("Searching an assigned docking bay", ThrustSearch(), limit_time=True)
    #rotate = Action("Zwrot o 180 stopni Celciusza i cala naprzod", RotateNode())

    # create separate sequences for each gate that consists of searching for buoys and reaching destination point
    part_1_seq = utils.create_sequence_child([search, reaching_goal_1a, save_starting_position], "Searching and an assigned docking bay")
    part_2_seq = utils.create_sequence_child([reaching_goal_2], "Back to start position")

    # parallel work of calculating destination points, searching and reaching
    part_1_parallel = utils.create_parallel_child([calculate_destination_1a, part_1_seq], "Calculate destination and docking")
    part_2_parallel = utils.create_parallel_child([calculate_destination_1b, reaching_goal_1b], "Calculate destination and docking")
    part_3_parallel = utils.create_parallel_child([load_starting_position, part_2_seq], "Calculate destination, rotate and undocking")

    # create tree root
    root = utils.create_sequence_child([part_1_parallel, part_2_parallel, part_3_parallel], "Root")
    #root = utils.create_sequence_child([part_1_parallel], "Root")

    root.setup_with_descendants()
    while True:
        try:
            root.tick_once()
            print("\n")
            print(py_trees.display.unicode_tree(root=root, show_status=True))
            time.sleep(0.05)
            if root.status == py_trees.common.Status.SUCCESS:
                break
        except KeyboardInterrupt:
            break
    print("\n")

if __name__ == '__main__':
    main()