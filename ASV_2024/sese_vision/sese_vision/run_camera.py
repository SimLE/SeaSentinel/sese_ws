#!/usr/bin/env python
import cv2
import depthai as dai
import numpy as np
import time
from pathlib import Path
import rclpy
from rclpy.node import Node
from vision_msgs.msg import ObjectHypothesisWithPose
from vision_msgs.msg import BoundingBox2D
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import os

from oak_d.utilities import config, utils

class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('minimal_publisher')
        self.detections_publisher = self.create_publisher(ObjectHypothesisWithPose, '/Detections', 10)
        self.detections_publisher_T5 = self.create_publisher(ObjectHypothesisWithPose, '/Detections_T5', 10)
        self.bbox_publisher = self.create_publisher(BoundingBox2D, '/bboxes', 10)
        self.image_publisher = self.create_publisher(Image, '/raw_image', 10)


def create_pipeline(blob_name, syncNN=True):

    pipeline = dai.Pipeline()

    # chunk size for splitting device-sent XLink packets, in bytes
    pipeline.setXLinkChunkSize(64 * 1024)

    # pipeline.setCameraTuningBlobPath('/home/jakub/Downloads/tuning_color_ov9782_wide_fov.bin')
    # Define sources and outputs
    camRgb = pipeline.create(dai.node.ColorCamera)
    camRgb.initialControl.setManualExposure(700, 100) #(800,100)
    spatialDetectionNetwork = pipeline.create(dai.node.YoloSpatialDetectionNetwork)
    monoLeft = pipeline.create(dai.node.MonoCamera)
    monoRight = pipeline.create(dai.node.MonoCamera)
    stereo = pipeline.create(dai.node.StereoDepth)
    nnNetworkOut = pipeline.create(dai.node.XLinkOut)

    xoutRgb = pipeline.create(dai.node.XLinkOut)
    xoutNN = pipeline.create(dai.node.XLinkOut)
    xoutDepth = pipeline.create(dai.node.XLinkOut)

    xoutRgb.setStreamName("rgb")
    xoutNN.setStreamName("detections")
    xoutDepth.setStreamName("depth")
    nnNetworkOut.setStreamName("nnNetwork")

    # Properties
    camRgb.setPreviewSize(
        768, 768
    )  # Size of images (640, 640) is required by DepthAI model conversion pipeline
    camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_800_P)
    camRgb.setInterleaved(False)
    camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P) #THE_720_P
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P) #THE_400_P
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)

    # setting node configs
    stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)
    # Align depth map to the perspective of RGB camera, on which inference is done
    stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
    stereo.setOutputSize(monoLeft.getResolutionWidth(), monoLeft.getResolutionHeight())

    spatialDetectionNetwork.setBlobPath(blob_name)
    spatialDetectionNetwork.setConfidenceThreshold(0.6)
    spatialDetectionNetwork.input.setBlocking(False)
    spatialDetectionNetwork.setBoundingBoxScaleFactor(0.5)
    spatialDetectionNetwork.setDepthLowerThreshold(100)
    spatialDetectionNetwork.setDepthUpperThreshold(200_000)

    # Yolo specific parameters
    spatialDetectionNetwork.setNumClasses(len(config.LABELS)) #before change was 5
    spatialDetectionNetwork.setCoordinateSize(4)
    spatialDetectionNetwork.setAnchors([10, 14, 23, 27, 37, 58, 81, 82, 135, 169, 344, 319])
    spatialDetectionNetwork.setAnchorMasks(
        {"side80": [1, 2, 3], "side40": [3, 4, 5], "side20": [3, 4, 5]}
    )
    spatialDetectionNetwork.setIouThreshold(0.5)

    # Linking
    monoLeft.out.link(stereo.left)
    monoRight.out.link(stereo.right)

    camRgb.preview.link(spatialDetectionNetwork.input)
    if syncNN:
        spatialDetectionNetwork.passthrough.link(xoutRgb.input)
    else:
        camRgb.preview.link(xoutRgb.input)

    spatialDetectionNetwork.out.link(xoutNN.input)

    stereo.depth.link(spatialDetectionNetwork.inputDepth)
    spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)
    spatialDetectionNetwork.outNetwork.link(nnNetworkOut.input)

    return pipeline


def main():
    rclpy.init()
    minimal_publisher = MinimalPublisher()

    # Create msgs
    msg = ObjectHypothesisWithPose()
    msg2 = ObjectHypothesisWithPose()
    bbox_msg = BoundingBox2D()
    raw_image_msg = Image()
    bridge = CvBridge()


    #nnBlobPath = f'/home/{os.environ["USER"]}/sese_git/src/sese/nn_models/only_boys_6/best_openvino_2022.1_6shave.blob'
    nnBlobPath = f'/home/{os.environ["USER"]}/sese_ws/src/sese/nn_models/only_boys_6/best_openvino_2022.1_6shave.blob'
    
    if not Path(nnBlobPath).exists():
        import sys

        raise FileNotFoundError(
            f'Blob file not found"'
        )

    print(nnBlobPath)

    for device in dai.Device.getAllAvailableDevices():
        print(f"{device.getMxId()} {device.state}")
        camera_id = device.getMxId()

    # Create pipeline
    pipeline = create_pipeline(nnBlobPath, syncNN=True)

    device_info = dai.DeviceInfo(camera_id)

    with dai.Device(pipeline, device_info) as device:

        previewQueue = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        detectionNNQueue = device.getOutputQueue(
            name="detections", maxSize=4, blocking=False
        )
        depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
        networkQueue = device.getOutputQueue(name="nnNetwork", maxSize=4, blocking=False)

        # It uses USB3
        # print(f'USB speed = {device.getUsbSpeed()}')
        startTime = time.monotonic()
        counter = 0
        fps = 0
        color = (255, 255, 255)
        color_text=(0, 255, 255)
        printOutputLayersOnce = True

        while True:
            inPreview = previewQueue.get()
            inDet = detectionNNQueue.get()
            depth = depthQueue.get()
            inNN = networkQueue.get()

            if printOutputLayersOnce:
                toPrint = "Output layer names:"
                for ten in inNN.getAllLayerNames():
                    toPrint = f"{toPrint} {ten},"
                print(toPrint)
                printOutputLayersOnce = False

            frame = inPreview.getCvFrame()
            depthFrame = depth.getFrame()  # depthFrame values are in millimeters

            depthFrameColor = cv2.normalize(
                depthFrame, None, 255, 0, cv2.NORM_INF, cv2.CV_8UC1
            )
            depthFrameColor = cv2.equalizeHist(depthFrameColor)
            depthFrameColor = cv2.applyColorMap(depthFrameColor, cv2.COLORMAP_HOT)

            counter += 1
            current_time = time.monotonic()
            if (current_time - startTime) > 1:
                fps = counter / (current_time - startTime)
                counter = 0
                startTime = current_time

            detections = inDet.detections
            if frame is not None:
                frame = utils.annotateFrame(frame, detections)

            # If the frame is available, draw bounding boxes on it and show the frame
            height = frame.shape[0]
            width = frame.shape[1]
            for detection in detections:
                roiData = detection.boundingBoxMapping
                roi = roiData.roi
                roi = roi.denormalize(depthFrameColor.shape[1], depthFrameColor.shape[0])
                topLeft = roi.topLeft()
                bottomRight = roi.bottomRight()
                xmin = int(topLeft.x)
                ymin = int(topLeft.y)
                xmax = int(bottomRight.x)
                ymax = int(bottomRight.y)
                cv2.rectangle(
                    depthFrameColor,
                    (xmin, ymin),
                    (xmax, ymax),
                    color,
                    cv2.FONT_HERSHEY_SCRIPT_SIMPLEX,
                )

                # Denormalize bounding box
                x1 = int(detection.xmin * width)
                x2 = int(detection.xmax * width)
                y1 = int(detection.ymin * height)
                y2 = int(detection.ymax * height)

                bbox_center = [(x1+x2)/2, (y1+y2)/2]
                bbox_width = x2-x1
                bbox_height = y2- y1

                try:
                    label = config.LABELS[detection.label]
                except:
                    label = detection.label
                
                
                #z-coordinate calculate from rectangle size
                estimation_limit = 6000
                z_calculate_by_size  = detection.spatialCoordinates.z
                z5_calculate_by_size = detection.spatialCoordinates.z
                
                if label=="red_buoy" or label=="green_buoy" or label=="yellow_buoy" or label=="black_buoy":
                    #approximation of z-coordinate by size of rectangle
                    if int(detection.spatialCoordinates.z) < 1 or int(detection.spatialCoordinates.z) > estimation_limit:
                        z_calculate_by_size = 296.5095210664003/bbox_width - 0.47210263844749006
                        z_calculate_by_size = 1000 * z_calculate_by_size
                
                #duże bojki
                if label=="red_buoy" or label=="green_buoy" or label=="yellow_buoy" or label=="blue_buoy":
                    #approximation of z-coordinate by size of rectangle
                    if int(detection.spatialCoordinates.z) < 1 or int(detection.spatialCoordinates.z) > estimation_limit:
                        z5_calculate_by_size = 453.5643295382201/bbox_width + 0.047580767706781324
                        z5_calculate_by_size = 1000 * z5_calculate_by_size
                
                #gate buoys        
                if label=="red_gate_buoy" or label=="green_gate_buoy":
                    #approximation of z-coordinate by size of rectangle
                    if int(detection.spatialCoordinates.z) < 1 or int(detection.spatialCoordinates.z) > estimation_limit:
                        z_calculate_by_size= 657.6100457956861/bbox_width - 0.1512880785204146
                        z_calculate_by_size = 1000 * z_calculate_by_size
                        
                cv2.putText(
                    frame,
                    f"Z_by_size: {int(z_calculate_by_size)} mm",
                    (x1 + 10, y1 + 100),
                    cv2.FONT_HERSHEY_TRIPLEX,
                    0.5,
                    color_text,
                )
                
                cv2.rectangle(frame, (x1, y1), (x2, y2), color, cv2.FONT_HERSHEY_SIMPLEX)
                
                msg.id = config.LABELS[detection.label]
                msg.pose.pose.position.x = detection.spatialCoordinates.x
                msg.pose.pose.position.z = z_calculate_by_size
                msg.pose.pose.position.y = detection.spatialCoordinates.y
                msg.score = detection.confidence

                msg2.id = config.LABELS[detection.label]
                msg2.pose.pose.position.x = detection.spatialCoordinates.x
                msg2.pose.pose.position.z = z5_calculate_by_size
                msg2.pose.pose.position.y = detection.spatialCoordinates.y
                msg2.score = detection.confidence

                bbox_msg.center.x = bbox_center[0]
                bbox_msg.center.y = bbox_center[1]

                # sizes must be float
                bbox_msg.size_x = float(bbox_width)
                bbox_msg.size_y = float(bbox_height)
                # msg.id = detection.label
                minimal_publisher.detections_publisher.publish(msg)
                minimal_publisher.detections_publisher_T5.publish(msg2)
                minimal_publisher.bbox_publisher.publish(bbox_msg)
                
            image_msg = bridge.cv2_to_imgmsg(frame)
            minimal_publisher.image_publisher.publish(image_msg)

            cv2.putText(
                frame,
                "NN fps: {:.2f}".format(fps),
                (2, 15),
                cv2.FONT_HERSHEY_TRIPLEX,
                0.4,
                color,
            )
            # cv2.imshow("depth", depthFrameColor)
            cv2.imshow("rgb", frame)

            if cv2.waitKey(1) == ord("q"):
                break


        rclpy.spin(minimal_publisher)

            # Destroy the node explicitly
            # (optional - otherwise it will be done automatically
            # when the garbage collector destroys the node object)
        minimal_publisher.destroy_node()
        rclpy.shutdown()


        # rospy.spin()

if __name__ == '__main__':
    main()
