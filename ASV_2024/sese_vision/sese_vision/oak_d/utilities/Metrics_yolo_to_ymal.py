from ultralytics import YOLO
import argparse
import os
import yaml


def main():
    parser = argparse.ArgumentParser(description='Takse preformance metrics from YOLOv8.')

    #Initialize two arguments for model and output file

    parser.add_argument('model', type=str,help="Enter model file")

    parser.add_argument('output_yaml',type=str , help="Enter file in YAML format to save metrics")

    args = parser.parse_args()


    #takes absolut model and output file path

    model_path = os.path.abspath(args.model_file)

    output_yaml_path = os.path.abspath(args.output_yaml)



    #defines inserted model and passing it to metrics function

    model = YOLO(model_path)

    metrics = get_yolo_metrics(model)

    
    save_metrics_to_yaml(metrics , output_yaml_path)
   



def get_yolo_classes(model: YOLO):

    #Iterates through moodel names, return empty string if none models
    
    if model.names:
        return model.names
    else:
        return " "
    
def get_yolo_metrics(yolo: YOLO):

    #Saving Precision, recall, mean Average Precision and classes of model into metrcis dictionary

    results = yolo.val()

    metrics = {

        'precision': results.metrics['precision'],
        'recall': results.metrics['recall'],
        'mAP_0.5': results.metrics['mAP50'],
        'mAP_0.5:0.95': results.metrics['mAP50_95'],
        'class_names': get_yolo_classes(yolo)
        
    }
    
    return metrics


def save_metrics_to_yaml(metrics: dict, output_path: str):

    #Outputs metrics into ymal file, 'default_flow_style=False' serializes collcetion in block style

    with open(output_path, 'w') as yaml_file:
        yaml.dump(metrics, yaml_file, default_flow_style=False)


if __name__ == "__main__":
    main()