# import the necessary packages
import os
from dataclasses import dataclass

LABELS = ['red_gate_buoy',
            'green_gate_buoy',
            'red_buoy',
            'green_buoy',
            'yellow_buoy',
            'black_buoy',
            'blue_circle',
            'green_circle',
            'red_circle',
            'blue_triangle',
            'green_triangle',
            'red_triangle',
            'blue_square',
            'green_square',
            'red_square',
            'blue_plus',
            'green_plus',
            'red_plus',
            'duck',
            'other',
            'banner',
            'blue_buoy']

@dataclass
class YoloConfig:
    classes: int = len(LABELS)
    coordinates: int = 4
    anchors = []
    anchorMasks = {}
    iouThreshold: float = 0.5
    confidenceThreshold: float = 0.5
    depth_lower_treshold: int = 500
    depth_upper_treshold: int = 10000

# define path to the model, test data directory and results
YOLOV8N_MODEL = os.path.join("oak_d","nn_models","yolov8n","yolov8n_coco.blob")
YOLOV8S_MODEL = os.path.join("oak_d", "nn_models","yolov8s","yolov8s_boje.blob")

OUTPUT_VIDEO = os.path.join("output_records", "abc4.avi")

# define camera preview dimensions same as YOLOv8 model input size
CAMERA_PREV_DIM = (640, 640)

