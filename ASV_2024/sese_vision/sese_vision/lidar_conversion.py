import rclpy
from rclpy.node import Node
import numpy as np

from sensor_msgs.msg import LaserScan
from vision_msgs.msg import ObjectHypothesisWithPose
import time


class MinimalPublisher(Node):

    def __init__(self):
        super().__init__('blabla')
        self.publisher_ = self.create_publisher(LaserScan, 'scan', 10)
        self.subsciber = self.create_subscription(LaserScan, 'my_scan', self.subscriber_callback, 10)
        self.rosbag_subscriber = self.create_subscription(ObjectHypothesisWithPose, '/Detections', self.position_callback, 10)
        timer_period = 0.5  # seconds
        self.i = 0
        self.resolution = 360
        self.angle_min = 0.000000
        self.angle_max = 6.280000

        self.x = 0.
        self.y = 0.

        self.counter = 0
        self.distance = 5

        self.label = ''

        self.detection_upper_limit = 20000
        self.detection_lower_limit = 50

        self.start_time = 0
        self.start_time_nano = 0

        self.i = 0

    def position_callback(self, msg):
        if msg.pose.pose.position.z > self.detection_lower_limit and msg.pose.pose.position.z < self.detection_upper_limit:
            # if msg.pose.pose.position.x >= 0.000001 or msg.pose.pose.position.x <= -0.000001:
            self.y = float(msg.pose.pose.position.x) / 300
            self.x = float(msg.pose.pose.position.z) / 300

        self.label = msg.id


    def subscriber_callback(self, msg):
        # initialize the result
        result = 0

        # set the radius of circle on which detected point is located
        R = np.sqrt(pow(self.x, 2) + pow(self.y, 2))

        # set the length of arc for everz section of circle
        total_range = (self.angle_max - self.angle_min) * R
        section = total_range / self.resolution

        # set length of arc between starting point of circle's arc and detected point
        if self.y == 0 or self.x == 0:
            R = 0
            arc = 0
        elif self.y >= 0:
            alfa = np.arctan(self.y/self.x)
            # arc = alfa * R + (int(self.resolution/2)) * section
            arc = (self.angle_max - alfa) * R
        else:
            alfa = np.arctan(np.abs(self.y)/self.x)
            # arc = (int(self.resolution/2))*section-alfa * R
            arc = alfa * R

        # get the section in which the detected point is in. Thats the result
        for i in range(self.resolution):
            sekcja = section*(i+1)
            # print(sekcja)
            if  sekcja < arc:
                result += 1
            else:
                break
        print(f'result: {result}')


        # for i in range(result-3,result):
        msg.ranges[result] = float(R)
        # msg.ranges[result] = float(R)
        # self.get_logger().info(f'ranges: {msg.ranges}')
        self.publisher_.publish(msg)
        
        # print('label:', self.label)
        print(f'x: {self.x}')
        print(f'y: {self.y}')
        # print(f'counter: {self.counter}')

def main(args=None):
    rclpy.init(args=args)

    minimal_publisher = MinimalPublisher()

    rclpy.spin(minimal_publisher)

    # Destroz the node explicitlz
    # (optional - otherwise it will be done automaticallz
    # when the garbage collector destrozs the node object)
    minimal_publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()