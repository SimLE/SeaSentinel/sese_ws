from oak_d.utilities import utils
from imutils.video import FPS
import argparse
import time
import cv2
import depthai as dai

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--model", help="Provide model name for inference",
                    default='yolov8n', type=str)
    args = parser.parse_args()

    print("[INFO] Create pipeline")
    model_name = args.model
    pipeline = utils.create_pipeline(model_name)

    camera_video_writer = utils.create_video_writer()

    print("[INFO] Start process")
    with dai.Device(pipeline) as device:

        qRgb = device.getOutputQueue(name="rgb", maxSize=4, blocking=False)
        qDet = device.getOutputQueue(name="nn", maxSize=4, blocking=False)

        frame = None
        startTime = time.monotonic()
        fps = FPS().start()
        counter = 0

        while True:
            
            inRgb = qRgb.get()
            inDet = qDet.get()
            if inRgb is not None:
                # convert inRgb output to a format OpenCV library can work
                frame = inRgb.getCvFrame()
                # print(f"FPS: {counter / (time.monotonic() - startTime)}")
                cv2.putText(frame, "NN fps: {:.2f}".format(counter / (time.monotonic() - startTime)),
                            (2, frame.shape[0] - 4), cv2.FONT_HERSHEY_TRIPLEX, 0.8, (255, 255, 255))

                fps.update()
            if inDet is not None:
                detections = inDet.detections
                print(detections)
                counter += 1
            if frame is not None:
                frame = utils.annotateFrame(frame, detections)
                # cv2.imshow("output", frame)

            camera_video_writer.write(frame)
            
            if cv2.waitKey(1) == ord('q'):
                break

    fps.stop()
    print("[INFO] elapsed time: {:.2f}".format(fps.elapsed()))
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

    camera_video_writer.release()
    cv2.destroyAllWindows()