import os

from ament_index_python.packages import get_package_share_directory

from launch.substitutions import LaunchConfiguration
from launch import LaunchDescription, actions
    
from launch_ros.actions import Node

def generate_launch_description():
    package_name='sese_rviz'

    package_share_directory = get_package_share_directory(package_name)

    default_rviz_config_path = os.path.join(package_share_directory, 'config/rviz_thrusters_config.rviz')

    rviz2_node = Node(
        package='rviz2',
        executable='rviz2',
        name='rviz2',
        output='screen',
        arguments=['-d', LaunchConfiguration('rvizconfig')],
    )

    rviz_thrusters_arrows_node = Node(
        package='sese_rviz',
        executable='rviz_thrusters_arrows',
        name='rviz_thrusters_arrows',
        output='screen',
        arguments=['-d', LaunchConfiguration('rvizconfig')],
    )

    return LaunchDescription([
        actions.DeclareLaunchArgument(name='use_sim_time', default_value='True',
                                            description='Flag to enable use_sim_time'),
        actions.DeclareLaunchArgument(name='rvizconfig', default_value=default_rviz_config_path,
                                            description='Absolute path to rviz config file'),
        rviz2_node,
        rviz_thrusters_arrows_node,
    ])
