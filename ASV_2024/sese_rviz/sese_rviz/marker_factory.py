from visualization_msgs.msg import Marker

NAMED_COLORS = {
    "red": (1.0, 0.0, 0.0),
    "green": (0.0, 1.0, 0.0),
    "blue": (0.0, 0.0, 1.0),
    "yellow": (1.0, 1.0, 0.0),
    "black": (0.3, 0.3, 0.3),
    "white": (1.0, 1.0, 1.0),
}


def _set_marker_color(marker: Marker, color_name, alpha=1.0) -> None:
    r, g, b = NAMED_COLORS[color_name]
    marker.color.r = r
    marker.color.g = g
    marker.color.b = b
    marker.color.a = alpha


class MarkerFactory:
    """Usage
    >>> marker_factory = MarkerFactory()
    >>> new_marker = marker_factory.from_object_class("red_gate_buoy")
    """

    def __init__(self, marker_scale=1.0) -> None:
        self.marker_scale = marker_scale

    def from_object_class(self, object_class: str) -> Marker:
        marker = Marker()
        marker.action = Marker.ADD

        if object_class == "red_gate_buoy":
            marker.type = Marker.CUBE
            _set_marker_color(marker, "red")
        elif object_class == "green_gate_buoy":
            marker.type = Marker.CUBE
            _set_marker_color(marker, "green")
        elif object_class == "red_buoy":
            marker.type = Marker.SPHERE
            _set_marker_color(marker, "red")
        elif object_class == "green_buoy":
            marker.type = Marker.SPHERE
            _set_marker_color(marker, "green")
        elif object_class == "yellow_buoy":
            marker.type = Marker.SPHERE
            _set_marker_color(marker, "yello")
        elif object_class == "black_buoy":
            marker.type = Marker.SPHERE
            _set_marker_color(marker, "black")
        elif object_class == "blue_buoy":
            marker.type = Marker.SPHERE
            _set_marker_color(marker, "blue")
        else:
            marker.type = Marker.CYLINDER
            _set_marker_color(marker, "white")

        return marker
